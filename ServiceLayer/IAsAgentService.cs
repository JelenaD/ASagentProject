﻿
using CommonLayer;
using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace ServiceLayer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAsAgentService" in both code and config file together.
    [ServiceContract]
    public interface IAsAgentService
    {
        [OperationContract]
        bool GetNameOfInputInvoice(string inputinvoice);

        #region ADDRESS BOOK
        [OperationContract]
        List<AddressBookDTO> GetAddressBookList();

        [OperationContract]
        void SaveAddressBook(AddressBookDTO dto);

        [OperationContract]
        AddressBookDTO AddressBookById(int id);

        [OperationContract]
        void SaveAddressChanges(AddressBookDTO dto);

        [OperationContract]
        void DeleteAddressBook(int id);

        [OperationContract]
        List<AddressBookDTO> FilterAddressBookByCompanyName(string nameOfCompany);
        #endregion

        [OperationContract]
        List<ShipperOfferDTO> GetShipperOffersByCOId(int id);

        #region BOOK
        [OperationContract]
        bool DateOfBook(DateTime date);


        [OperationContract]
        List<BookDTO> GetBookCollection();

        [OperationContract]
        List<string> GetDistinctBookingNumbers();

        [OperationContract]
        BookDTO GetBookByBookingNumber(string bookingNumber);

        [OperationContract]
        string GetNameOfClientByBookId(int id);

        [OperationContract]
        List<BookDTO> FindBookByObject(string objectBook);

        [OperationContract]
        void DeleteBook(int id);

        [OperationContract]
        void SaveBook(BookDTO dto);

        [OperationContract]
        BookDTO BookById(int id);

        [OperationContract]
        void SaveBookChanges(BookDTO dto);

        [OperationContract]
        double? GetPriceOfInputInvoice(int bookId);

        [OperationContract]
        double? GetPriceOfForeignInputInovice(int bookId);

        [OperationContract]
        double? GetPriceOfOutgoingInvoice(int bookId);

        [OperationContract]
        double? GetPriceOfForeignOutgoingInvoice(int bookId);
        #endregion

        #region ASA BOOKING NUMBER
        [OperationContract]
        void SaveAsaBookingNumberChanges(ASABookingNumberDTO dto);

        [OperationContract]
        void SaveAsaBookingNumber(ASABookingNumberDTO dto);

        [OperationContract]
        void DeleteAsaBookingNumber(int id);

        [OperationContract]
        List<ASABookingNumberDTO> GetAllAsaNumbers();

        [OperationContract]
        ASABookingNumberDTO AsaBookingNumberById(int id);
        #endregion

        #region STANDARD BOOKING NUMBER
        [OperationContract]
        void SaveStandardBookingNumber(StandardBookingNumberDTO dto);

        [OperationContract]
        List<StandardBookingNumberDTO> GetAllStandardNumbers();

        [OperationContract]
        StandardBookingNumberDTO StandardBookingNumberId(int id);

        [OperationContract]
        void DeleteStandardBookingNumber(int id);

        [OperationContract]
        void SaveStandardBookingNumberChanges(StandardBookingNumberDTO dto);
        #endregion

        #region USER
        [OperationContract]
        bool GetUsername(string username);

        [OperationContract]
        bool CheckIfUserExists(string username, string password);

        [OperationContract]
        User CheckIfUserExistsByUsernameAndPassword(string username, string password);

        [OperationContract]
        List<UserDTO> AllUsers();

        [OperationContract]
        void SaveUser(UserDTO dto);

        [OperationContract]
        void SaveChanges(UserDTO dto);

        [OperationContract]
        void DeleteUser(int id);

        [OperationContract]
        UserDTO FindById(int id);

        #endregion

        #region SHIPPER
        [OperationContract]
        List<ShipperDTO> Shippers();

        [OperationContract]
        void SaveShipper(ShipperDTO dto);

        [OperationContract]
        ShipperDTO FindShipperById(int id);

        [OperationContract]
        void DeleteShipper(int id);

        [OperationContract]
        void SaveShipperChanges(ShipperDTO shipper);
        #endregion

        #region CLIENT
        [OperationContract]
        void SaveClientChanges(ClientDTO dto);

        [OperationContract]
        ClientDTO FindClientById(int id);

        [OperationContract]
        List<ClientDTO> AllClients();

        [OperationContract]
        void SaveClient(ClientDTO client);

        [OperationContract]
        List<ClientDTO> FilterClientByName(string companyName);

        [OperationContract]
        List<ClientDTO> FilterClientByState(string state);

        [OperationContract]
        List<ClientDTO> GetClientsByNameAndCountry(string name, string country);

        [OperationContract]
        void DeleteClient(int id);

        #endregion

        #region COST
        [OperationContract]
        List<CostsDTO> AllCosts();

        [OperationContract]
        void SaveCost(CostsDTO dto);

        [OperationContract]
        List<CostsDTO> FilterCostsByName(string name);

        [OperationContract]
        CostsDTO FindCostById(int id);

        [OperationContract]
        void DeleteCost(int id);

        [OperationContract]
        void SaveCostChanges(CostsDTO costs);

        [OperationContract]
        List<CostsDTO> GetCostByDate(DateTime? date1, DateTime? date2);

        [OperationContract]
        List<CostsDTO> GetCostsByDateAndName(DateTime? date1, DateTime? date2, string account);
        #endregion

        #region SHIPPER OFFER
        [OperationContract]
        List<ShipperOfferDTO> ShipperOffers();

        [OperationContract]
        void SaveShipperOffer(ShipperOfferDTO dto);

        [OperationContract]
        void DeleteOffer(int id);

        [OperationContract]
        ShipperOfferDTO FindShipperOfferById(int id);

        [OperationContract]
        ShipperOfferDTO GetShipperOfferByShipperId(int id);

        [OperationContract]
        void SaveChangesOfShipperOffer(ShipperOfferDTO offer);

        [OperationContract]
        List<ShipperOfferDTO> GetPartnerOffersByName(string name);

        [OperationContract]
        List<ShipperOfferDTO> GetShipperOffersByShipper(string shipper);

        [OperationContract]
        List<ShipperOfferDTO> GetShipperOffersByNameAndShipper(string shipper, string name);
        #endregion

        #region INPUT INVOICE
        [OperationContract]
        List<InputInvoiceDTO> GetInvoiceByBookId(int id);

        [OperationContract]
        List<InputInvoiceDTO> InputInvoice();

        [OperationContract]
        List<InputInvoiceDTO> GetInInvoicesByDate(DateTime? dat1, DateTime? dat2);

        [OperationContract]
        List<InputInvoiceDTO> GetInInvoicesByName(string name);

        [OperationContract]
        List<InputInvoiceDTO> GetInInvoicesBySubject(string subject);

        [OperationContract]
        void DeleteInInvoice(int id);

        [OperationContract]
        InputInvoiceDTO FindInputInvoiceById(int id);

        [OperationContract]
        void SaveInInvoice(InputInvoiceDTO dto);

        [OperationContract]
        void SaveInInvoiceChanges(InputInvoiceDTO dto);
        #endregion

        #region FOREIGN INPUT INVOICE
        [OperationContract]
        bool GetNameOfForeignInputInvoice(string inputinvoice);

        [OperationContract]
        List<ForeignInputInvoiceDTO> GetInoInvoiceByBookId(int id);

        [OperationContract]
        List<ForeignInputInvoiceDTO> GetAllInoInInvoices();

        [OperationContract]
        List<ForeignInputInvoiceDTO> GetInInoInvoicesByDate(DateTime? dat1, DateTime? dat2);

        [OperationContract]
        List<ForeignInputInvoiceDTO> GetInInoInvoicesByName(string name);

        [OperationContract]
        List<ForeignInputInvoiceDTO> GetInInoInvoicesBySubject(string subject);

        [OperationContract]
        void DeleteInoInInvoice(int id);

        [OperationContract]
        ForeignInputInvoiceDTO FindInoInInvoiceById(int id);

        [OperationContract]
        void SaveInoInInvoice(ForeignInputInvoiceDTO dto);

        [OperationContract]
        void SaveInoInInvoiceChanges(ForeignInputInvoiceDTO dto);
        #endregion

        #region OUTGOING INVOICE
        [OperationContract]
        bool GetNameOfOutgoingInvoice(string outgointinvoice);

        [OperationContract]
        List<OutgoingInvoiceDTO> GetOutInvoicesByBookId(int id);

        [OperationContract]
        List<OutgoingInvoiceDTO> OutgoingInvoices();

        [OperationContract]
        void SaveInvoice(OutgoingInvoiceDTO dto);

        [OperationContract]
        List<OutgoingInvoiceDTO> GetOutInvoiceByName(string name);

        [OperationContract]
        List<OutgoingInvoiceDTO> GetOutInvoicesByDate(DateTime? dat1, DateTime? dat2);

        [OperationContract]
        OutgoingInvoiceDTO FindIInvoiceById(int id);

        [OperationContract]
        void DeleteOutInvoice(int id);

        [OperationContract]
        void SaveChangesOfInvoice(OutgoingInvoiceDTO dto);
        #endregion

        #region FOREIGN OUTGOING INVOICE
        [OperationContract]
        bool GetNameOfForeignOutgoingInvoice(string outgointinvoice);

        [OperationContract]
        List<ForeignOutgoingInvoiceDTO> GetOutInoInvoicesByBookId(int id);

        [OperationContract]
        List<ForeignOutgoingInvoiceDTO> AllInoOutInvoices();

        [OperationContract]
        void SaveInoInvoice(ForeignOutgoingInvoiceDTO dto);

        [OperationContract]
        List<ForeignOutgoingInvoiceDTO> GetOutInoInvoiceByName(string name);

        [OperationContract]
        List<ForeignOutgoingInvoiceDTO> GetOutInoInvoicesByDate(DateTime? dat1, DateTime? dat2);

        [OperationContract]
        ForeignOutgoingInvoiceDTO FindInoOutInvoiceById(int id);

        [OperationContract]
        void DeleteOutInoInvoice(int id);

        [OperationContract]
        void SaveChangesOfOutInoInvoice(ForeignOutgoingInvoiceDTO dto);
        #endregion

        #region ADDITIONAL DOCUMENTATION
        [OperationContract]
        List<AdditionalDocumentationDTO> AdditionalDocumentationList();

        [OperationContract]
        List<AdditionalDocumentationDTO> GetDocumentByName(string name);

        [OperationContract]
        List<AdditionalDocumentationDTO> GetDocumentByNameAndSubject(string subject, string name);

        [OperationContract]
        List<AdditionalDocumentationDTO> GetDocumentBySubject(string subject);

        [OperationContract]
        void DeleteAttachments(int id);

        [OperationContract]
        void SaveDocument(AdditionalDocumentationDTO document, HttpPostedFileBase upload);

        [OperationContract]
        void SaveAdditionalDocumentation(AdditionalDocumentationDTO document);

        [OperationContract]
        AdditionalDocumentationDTO GetDocumentById(int id);

        [OperationContract]
        void SaveDocumentChanges(AdditionalDocumentationDTO dto);

        [OperationContract]
        void SaveDetail(AdditionalDocumentationDTO model);
        #endregion

        #region CLIENT OFFER
        [OperationContract]
        List<ClientOffersDTO> ClientOffers();

        [OperationContract]
        void DeleteClientOffer(int id);

        [OperationContract]
        void SaveClientOffer(ClientOffersDTO dto);

        [OperationContract]
        ClientOffersDTO FindClientOfferById(int id);

        [OperationContract]
        void SaveClientOfferChanges(ClientOffersDTO offer);

        [OperationContract]
        List<ClientOffersDTO> GetClientOfferByName(string name);

        [OperationContract]
        List<ClientOffersDTO> GetClientOfferByClient(string client);

        [OperationContract]
        List<ClientOffersDTO> FindClientOfferByNameAndClient(string name, string client);
        #endregion

        #region NOTIFICATIONS
        [OperationContract]
        List<InputInvoiceDTO> GetInvoicesByExpireDate();

        [OperationContract]
        List<ForeignInputInvoiceDTO> GetInoInvoicesByExpireDate();
        #endregion

        [OperationContract]
        List<InputInvoiceDTO> GetEmailInvoicesByExpireDate();

        [OperationContract]
        List<ForeignInputInvoiceDTO> GetEmailINOInvoicesByExpireDate();

        [OperationContract]
        List<ForeignOutgoingInvoiceDTO> GetforeignOutgoingInvoicesByExpireDate();

        [OperationContract]
        List<OutgoingInvoiceDTO> GetOutgoingInvoicesByExpireDate();

        #region DISTANCE TRANSPORT
        [OperationContract]
        List<DistanceTransportDTO> GetDistanceTransportList();

        [OperationContract]
        void DeleteDistanceTransport(int id);

        [OperationContract]
        void SaveDistanceTransport(DistanceTransportDTO dto);

        [OperationContract]
        DistanceTransport FindDistanceTransportById(int id);

        [OperationContract]
        void SaveDistanceTransportChanges(DistanceTransportDTO dto);

        [OperationContract]
        List<DistanceTransportDTO> GetDistanceTransportByShipperId(int id);
        #endregion
    }
}
