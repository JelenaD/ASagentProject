﻿
using CommonLayer;
using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceLayer
{

    public class AsAgentHelper
    {
        private EfDataHelper efDataHelper;

        public AsAgentHelper(EfDataHelper efDataHelper)
        {
            this.efDataHelper = efDataHelper;
        }

        public List<ShipperOfferDTO> GetShipperOffersByCOId(int id)
        {
            return efDataHelper.GetShipperOffersByCOId(id);
        }

        public List<InputInvoiceDTO> GetEmailInvoicesByExpireDate()
        {
            return efDataHelper.GetEmailInvoicesByExpireDate();
        }

        public List<ForeignInputInvoiceDTO> GetEmailINOInvoicesByExpireDate()
        {
            return efDataHelper.GetEmailINOInvoicesByExpireDate();
        }

        public bool GetNameOfInputInvoice(string inputinvoice)
        {
            return efDataHelper.GetNameOfInputInvoice(inputinvoice);
        }

        public List<OutgoingInvoiceDTO> GetOutgoingInvoicesByExpireDate()
        {
            return efDataHelper.GetOutgoingInvoicesByExpireDate();
        }

        public List<ForeignOutgoingInvoiceDTO> GetforeignOutgoingInvoicesByExpireDate()
        {
            return efDataHelper.GetforeignOutgoingInvoicesByExpireDate();
        }

            #region ADDRESS BOOK
            public List<AddressBookDTO> GetAddressBookList()
        {
            return efDataHelper.GetAddressBookList();
        }

        public void SaveAddressBook(AddressBookDTO dto)
        {
            efDataHelper.SaveAddressBook(dto);
        }

        public AddressBookDTO AddressBookById(int id)
        {
            var book = efDataHelper.AddressBookById(id);
            AddressBookDTO dto = new AddressBookDTO();
            dto.AddressBookId = book.AddressBookId;
            dto.NameOfCompany = book.Company;
            dto.Name = book.Name;
            dto.LastName = book.LastName;
            dto.Email = book.Email;
            dto.Telephone = book.Telephone;
            return dto;
        }

        public void SaveAddressChanges(AddressBookDTO dto)
        {
            efDataHelper.SaveAddressChanges(dto);
        }

        public void DeleteAddressBook(int id)
        {
            efDataHelper.DeleteAddressBook(id);
        }

        public List<AddressBookDTO> FilterAddressBookByCompanyName(string nameOfCompany)
        {
            return efDataHelper.FilterAddressBookByCompanyName(nameOfCompany);
        }
        #endregion

        #region BOOK
        public bool DateOfBook(DateTime date)
        {
            return efDataHelper.DateOfBook(date);
        }

            public List<BookDTO> GetBookCollection()
        {
            return efDataHelper.GetBookCollection();
        }

        public List<string> GetDistinctBookingNumbers()
        {
            return efDataHelper.GetDistinctBookingNumbers();
        }

        public BookDTO GetBookByBookingNumber(string bookingNumber)
        {
            return efDataHelper.GetBookByBookingNumber(bookingNumber);
        }

        public string GetNameOfClientByBookId(int id)
        {
            return efDataHelper.GetNameOfClientByBookId(id);
        }

        public List<BookDTO> FindBookByObject(string objectBook)
        {
            return efDataHelper.FindBookByObject(objectBook);
        }

        public void DeleteBook(int id)
        {
            efDataHelper.DeleteBook(id);
        }

        public void SaveBook(BookDTO dto)
        {
            efDataHelper.SaveBook(dto);
        }

        public BookDTO BookById(int id)
        {
            var book = efDataHelper.BookById(id);
            BookDTO dto = new BookDTO();
            dto.BookId = book.BookId;
            dto.ObjectBook = book.BookObject;
            dto.PlaceOfLoading = book.PlaceOfLoading;
            dto.DateOfLoading = book.DateOfLoading;
            dto.PlaceOfUnloading = book.PlaceOfUnloading;
            dto.Goods = book.Goods;
            dto.Customer = book.Customer;
            dto.QuantityOfGoods = book.QuantityOfGoods;
            dto.ContainerTruck = book.ContainerTruck;
            dto.BookingNumber = book.BookingNumber;

            return dto;
        }

        public void SaveBookChanges(BookDTO dto)
        {
            efDataHelper.SaveBookChanges(dto);
        }

        public double? GetPriceOfInputInvoice(int bookId)
        {
            return efDataHelper.GetPriceOfInputInvoice(bookId);
        }

        public double? GetPriceOfForeignInputInovice(int bookId)
        {
            return efDataHelper.GetPriceOfForeignInputInovice(bookId);
        }

        public double? GetPriceOfOutgoingInvoice(int bookId)
        {
            return efDataHelper.GetPriceOfOutgoingInvoice(bookId);
        }

        public double? GetPriceOfForeignOutgoingInvoice(int bookId)
        {
            return efDataHelper.GetPriceOfForeignOutgoingInvoice(bookId);
        }
        #endregion

        #region ASA BOOKING NUMBER
        public void SaveAsaBookingNumberChanges(ASABookingNumberDTO dto)
        {
            efDataHelper.SaveAsaBookingNumberChanges(dto);
        }

        public void SaveAsaBookingNumber(ASABookingNumberDTO dto)
        {
            efDataHelper.SaveAsaBookingNumber(dto);
        }

        public void DeleteAsaBookingNumber(int id)
        {
            efDataHelper.DeleteAsaBookingNumber(id);
        }

        public List<ASABookingNumberDTO> GetAllAsaNumbers()
        {
            return efDataHelper.GetAllAsaNumbers();
        }

        public ASABookingNumberDTO AsaBookingNumberById(int id)
        {
            var book = efDataHelper.AsaBookingNumberById(id);
            ASABookingNumberDTO dto = new ASABookingNumberDTO();
            dto.AsaBookingNumberId = book.AsaBookingNumberId;
            dto.AsaNumber = book.AsaNumber.ToString();
            dto.Distance = book.Distance;
            dto.Container = book.Container;

            return dto;
        }
        #endregion

        #region STANDARD BOOKING NUMBER
        public void SaveStandardBookingNumber(StandardBookingNumberDTO dto)
        {
            efDataHelper.SaveStandardBookingNumber(dto);
        }

        public List<StandardBookingNumberDTO> GetAllStandardNumbers()
        {
            return efDataHelper.GetAllStandardNumbers();
        }

        public StandardBookingNumberDTO StandardBookingNumberId(int id)
        {
            var book = efDataHelper.StandardBookingNumberId(id);
            StandardBookingNumberDTO dto = new StandardBookingNumberDTO();
            dto.StandardBookingNumberId = book.StandardBookingNumberId;
            dto.StandardNumber = book.StandardNumber.ToString();
            dto.Distance = book.Distance;
            dto.Container = book.Container;

            return dto;
        }

        public void SaveStandardBookingNumberChanges(StandardBookingNumberDTO dto)
        {
            efDataHelper.SaveStandardBookingNumberChanges(dto);
        }

        public void DeleteStandardBookingNumber(int id)
        {
            efDataHelper.DeleteStandardBookingNumber(id);
        }
        #endregion

        #region USER
        public bool GetUsername(string username)
        {
            return efDataHelper.GetUsername(username);
        }

        public bool CheckIfUserExists(string username, string password)
        {
            if (efDataHelper.GetUserByUsernameAndPassword(username, password) == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public User CheckIfUserExistsByUsernameAndPassword(string username, string password)
        {
            var korisnik = efDataHelper.GetUserByUsernameAndPassword(username, password);
            return korisnik;
        }

        public List<UserDTO> AllUsers()
        {
            return efDataHelper.AllUsers();
        }

        public void SaveUser(UserDTO dto)
        {
            efDataHelper.SaveUser(dto);
        }

        public void SaveChanges(UserDTO dto)
        {
            efDataHelper.SaveChanges(dto);
        }

        public void DeleteUser(int id)
        {
            efDataHelper.DeleteUser(id);
        }

        public UserDTO FindById(int id)
        {
            var user = efDataHelper.FindById(id);
            UserDTO dto = new UserDTO();
            dto.UserId = user.UserId;
            dto.Username = user.Username;
            dto.Name = user.Name;
            dto.LastName = user.LastName;
            dto.Address = user.Address;
            dto.Email = user.Email;
            dto.Telephone = user.Telephone;
            dto.Password = user.Password;
            dto.UserRole = user.Role;

            return dto;
        }
        #endregion

        #region CLIENT
        public void SaveClientChanges(ClientDTO dto)
        {
            efDataHelper.SaveClientChanges(dto);
        }

        public ClientDTO FindClientById(int id)
        {
            var client = efDataHelper.FindClientById(id);
            ClientDTO dto = new ClientDTO();
            dto.ClientId = client.ClientId;
            dto.PIB = client.PIB;
            dto.NameOfClientCompany = client.NameOfClientCompany;
            dto.Address = client.Address;
            dto.Email = client.Email;
            dto.Telephone = client.Telephone;
            dto.State = client.State;
            dto.ZipCode = client.ZipCode;
            dto.City = client.City;
            return dto;
        }

        public List<ClientDTO> AllClients()
        {
            return efDataHelper.AllClients();
        }

        public void SaveClient(ClientDTO client)
        {
            efDataHelper.AddClient(client);
        }

        public List<ClientDTO> FilterClientByName(string companyName)
        {
            var client = efDataHelper.FilterClientByName(companyName);
            List<ClientDTO> listDTO = new List<ClientDTO>();
            foreach (var cl in client)
            {
                ClientDTO dto = new ClientDTO();
                dto.PIB = cl.PIB;
                dto.NameOfClientCompany = cl.NameOfClientCompany;
                dto.Address = cl.Address;
                dto.Email = cl.Email;
                dto.Telephone = cl.Telephone;
                dto.State = cl.State;
                dto.ZipCode = cl.ZipCode;
                dto.City = cl.City;
                listDTO.Add(dto);
            }
            return listDTO;
        }

        public List<ClientDTO> FilterClientByState(string state)
        {
            var client = efDataHelper.FilterClientByState(state);
            List<ClientDTO> listDTO = new List<ClientDTO>();
            foreach (var cl in client)
            {
                ClientDTO dto = new ClientDTO();
                dto.PIB = cl.PIB;
                dto.NameOfClientCompany = cl.NameOfClientCompany;
                dto.Address = cl.Address;
                dto.Email = cl.Email;
                dto.Telephone = cl.Telephone;
                dto.State = cl.State;
                dto.ZipCode = cl.ZipCode;
                dto.City = cl.City;
                listDTO.Add(dto);
            }
            return listDTO;
        }

        public List<ClientDTO> GetClientsByNameAndCountry(string name, string country)
        {
            return efDataHelper.GetClientsByNameAndCountry(name, country);
        }

        public void DeleteClient(int id)
        {
            efDataHelper.DeleteClient(id);
        }
        #endregion

        #region COST
        public List<CostsDTO> AllCosts()
        {
            return efDataHelper.AllCosts();
        }

        public void SaveCost(CostsDTO dto)
        {
            efDataHelper.SaveCost(dto);
        }

        public List<CostsDTO> FilterCostsByName(string name)
        {
            return efDataHelper.FilterCostsByName(name);
        }

        public CostsDTO FindCostById(int id)
        {
            var cost = efDataHelper.FindCostById(id);
            CostsDTO dto = new CostsDTO();
            dto.CostId = cost.CostId;
            dto.AccountNumber = cost.AccountNumber;
            dto.DateOfReceipt = cost.DateOfReceipt;
            dto.PaymentDate = cost.PaymentDate;
            dto.Amount = cost.Amount;
            dto.Paid = cost.Paid;
            return dto;
        }

        public void DeleteCost(int id)
        {
            efDataHelper.DeleteCost(id);
        }

        public void SaveCostChanges(CostsDTO costs)
        {
            efDataHelper.SaveCostChanges(costs);
        }

        public List<CostsDTO> GetCostByDate(DateTime? date1, DateTime? date2)
        {
            return efDataHelper.GetCostByDate(date1, date2);
        }

        public List<CostsDTO> GetCostsByDateAndName(DateTime? date1, DateTime? date2, string account)
        {
            return efDataHelper.GetCostsByDateAndName(date1, date2, account);
        }
        #endregion

        #region SHIPPER OFFER
        public List<ShipperOfferDTO> ShipperOffers()
        {
            return efDataHelper.ShipperOffers();
        }

        public void SaveShipperOffer(ShipperOfferDTO dto)
        {
            efDataHelper.SaveShipperOffer(dto);
        }

        public void DeleteOffer(int id)
        {
            efDataHelper.DeleteOffer(id);
        }

        public ShipperOfferDTO FindShipperOfferById(int id)
        {
            var offer = efDataHelper.FindShipperOfferById(id);
            ShipperOfferDTO dto = new ShipperOfferDTO();
            dto.ShipperOfferId = offer.ShipperOfferId;
            dto.ShipperOfferName = offer.ShipperOfferName;
            dto.Date = offer.Date;
            dto.Description = offer.Description;
            dto.Price = offer.Price;
            dto.Email = offer.Email;
            dto.Telephone = offer.Telephone;
            dto.Distance = offer.Distance;
            dto.ShipperId = offer.ShipperId;
            dto.ClientOfferId = offer.ClientOffId;

            return dto;
        }

        public ShipperOfferDTO GetShipperOfferByShipperId(int id)
        {
            var offer = efDataHelper.GetShipperOfferByShipperId(id);
            ShipperOfferDTO dto = new ShipperOfferDTO();
            dto.ShipperOfferId = offer.ShipperOfferId;
            dto.ShipperOfferName = offer.ShipperOfferName;
            dto.Date = offer.Date;
            dto.Description = offer.Description;
            dto.Price = offer.Price;
            dto.ShipperId = offer.ShipperId;

            return dto;
        }

        public void SaveChangesOfShipperOffer(ShipperOfferDTO offer)
        {
            efDataHelper.SaveChangesOfShipperOffer(offer);
        }

        public List<ShipperOfferDTO> GetPartnerOffersByName(string name)
        {
            return efDataHelper.GetPartnerOffersByName(name);
        }

        public List<ShipperOfferDTO> GetShipperOffersByShipper(string shipper)
        {
            return efDataHelper.GetShipperOffersByShipper(shipper);
        }

        public List<ShipperOfferDTO> GetShipperOffersByNameAndShipper(string shipper, string name)
        {
            return efDataHelper.GetShipperOffersByNameAndShipper(shipper, name);
        }
        #endregion

        #region SHIPPER
        public List<ShipperDTO> Shippers()
        {
            return efDataHelper.Shippers();
        }

        public void SaveShipper(ShipperDTO dto)
        {
            efDataHelper.SaveShipper(dto);
        }

        public ShipperDTO FindShipperById(int id)
        {
            var shipper = efDataHelper.FindShipperById(id);
            ShipperDTO dto = new ShipperDTO();
            dto.ShipperId = shipper.ShipperId;
            dto.Pib = shipper.PIB;
            dto.CompanyName = shipper.CompanyName;
            dto.Address = shipper.Address;
            dto.Email = shipper.Email;
            dto.Telephone = shipper.Telephone;
            dto.State = shipper.State;
            dto.ZipCode = shipper.ZipCode;
            dto.City = shipper.City;

            return dto;
        }

        public void DeleteShipper(int id)
        {
            efDataHelper.DeleteShipper(id);
        }

        public void SaveShipperChanges(ShipperDTO shipper)
        {
            efDataHelper.SaveShipperChanges(shipper);
        }
        #endregion

        #region INPUT INVOICE
        public List<InputInvoiceDTO> GetInvoiceByBookId(int id)
        {
            return efDataHelper.GetInvoiceByBookId(id);
        }

        public List<InputInvoiceDTO> InputInvoice()
        {
            return efDataHelper.InputInvoice();
        }

        public List<InputInvoiceDTO> GetInInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            return efDataHelper.GetInInvoicesByDate(dat1, dat2);
        }

        public List<InputInvoiceDTO> GetInInvoicesByName(string name)
        {
            return efDataHelper.GetInInvoicesByName(name);
        }

        public List<InputInvoiceDTO> GetInInvoicesBySubject(string subject)
        {
            return efDataHelper.GetInInvoicesBySubject(subject);
        }

        public void DeleteInInvoice(int id)
        {
            efDataHelper.DeleteInInvoice(id);
        }

        public InputInvoiceDTO FindInputInvoiceById(int id)
        {
            var invoice = efDataHelper.FindInputInvoiceById(id);
            InputInvoiceDTO dto = new InputInvoiceDTO();
            dto.SerialNumberOfInputInvoice = invoice.SerialNumberOfInputInvoice;
            dto.DateOfReceiptInvoice = invoice.DateOfReceiptInvoice;
            dto.NumberOfInputInvoice = invoice.NumberOfInputInvoice;
            dto.AmountInDIN = invoice.AmountInDIN;
            dto.AmountInEUR = invoice.AmountInEUR;
            dto.ValueDate = invoice.ValueDate;
            dto.PaymentDate = invoice.PaymentDate;
            dto.Paid = invoice.Paid;
            dto.Comment = invoice.Comment;
            dto.ShipperId = invoice.ShipperId;
            dto.BookId = invoice.BookId;

            return dto;
        }

        public void SaveInInvoice(InputInvoiceDTO dto)
        {
            efDataHelper.SaveInInvoice(dto);
        }

        public void SaveInInvoiceChanges(InputInvoiceDTO dto)
        {
            efDataHelper.SaveInInvoiceChanges(dto);
        }
        #endregion

        #region FOREIGN INPUT INVOICE
        public bool GetNameOfForeignInputInvoice(string inputinvoice)
        {
            return efDataHelper.GetNameOfForeignInputInvoice(inputinvoice);
        }
            public List<ForeignInputInvoiceDTO> GetInoInvoiceByBookId(int id)
        {
            return efDataHelper.GetInoInvoiceByBookId(id);
        }

        public List<ForeignInputInvoiceDTO> GetAllInoInInvoices()
        {
            return efDataHelper.GetAllInoInInvoices();
        }

        public List<ForeignInputInvoiceDTO> GetInInoInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            return efDataHelper.GetInInoInvoicesByDate(dat1, dat2);
        }

        public List<ForeignInputInvoiceDTO> GetInInoInvoicesByName(string name)
        {
            return efDataHelper.GetInInoInvoicesByName(name);
        }

        public List<ForeignInputInvoiceDTO> GetInInoInvoicesBySubject(string subject)
        {
            return efDataHelper.GetInInoInvoicesBySubject(subject);
        }

        public void DeleteInoInInvoice(int id)
        {
            efDataHelper.DeleteInoInInvoice(id);
        }

        public ForeignInputInvoiceDTO FindInoInInvoiceById(int id)
        {
            var invoice = efDataHelper.FindInoInInvoiceById(id);
            ForeignInputInvoiceDTO dto = new ForeignInputInvoiceDTO();
            dto.SerialNumberOfForeignInputInvoice = invoice.SerialNumberOfForeignInputInvoice;
            dto.DateOfReceiptInvoice = invoice.DateOfReceiptInvoice;
            dto.NumberOfForeignInputInvoice = invoice.NumberOfForeignInputInvoice;
            dto.AmountInEUR = invoice.AmountInEUR;
            dto.ValueDate = invoice.ValueDate;
            dto.PaymentDate = invoice.PaymentDate;
            dto.Paid = invoice.Paid;
            dto.Comment = invoice.Comment;
            dto.ShipperId = invoice.ShipperId;
            dto.BookId = invoice.BookId;

            return dto;
        }

        public void SaveInoInInvoice(ForeignInputInvoiceDTO dto)
        {
            efDataHelper.SaveInoInInvoice(dto);
        }

        public void SaveInoInInvoiceChanges(ForeignInputInvoiceDTO dto)
        {
            efDataHelper.SaveInoInInvoiceChanges(dto);
        }
        #endregion

        #region OUTGOING INVOICE
        public bool GetNameOfOutgoingInvoice(string outgointinvoice)
        {
            return efDataHelper.GetNameOfOutgoingInvoice(outgointinvoice);
        }

            public List<OutgoingInvoiceDTO> GetOutInvoicesByBookId(int id)
        {
            return efDataHelper.GetOutInvoicesByBookId(id);
        }

        public List<OutgoingInvoiceDTO> OutgoingInvoices()
        {
            return efDataHelper.OutgoingInvoices();
        }

        public void SaveInvoice(OutgoingInvoiceDTO dto)
        {
            efDataHelper.SaveInvoice(dto);
        }

        public List<OutgoingInvoiceDTO> GetOutInvoiceByName(string name)
        {
            return efDataHelper.GetOutInvoiceByName(name);
        }

        public List<OutgoingInvoiceDTO> GetOutInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            return efDataHelper.GetOutInvoicesByDate(dat1, dat2);
        }

        public OutgoingInvoiceDTO FindIInvoiceById(int id)
        {
            var invoice = efDataHelper.FindIInvoiceById(id);
            OutgoingInvoiceDTO dto = new OutgoingInvoiceDTO();
            dto.DateOfReceipt = invoice.DateOfReceipt;
            dto.NumberOfOutgoingInvoice = invoice.NumberOfOutgoingInvoice;
            dto.AmountDIN = invoice.AmountInDIN;
            dto.AmountInEUR = invoice.AmountInEUR;
            dto.ValueDate = invoice.ValueDate;
            dto.PaymentDate = invoice.PaymentDate;
            dto.Paid = invoice.Paid;
            dto.Comment = invoice.Comment;
            dto.BookId = invoice.BookId;
            dto.ClientId = invoice.ClientId;

            return dto;
        }

        public void DeleteOutInvoice(int id)
        {
            efDataHelper.DeleteOutInvoice(id);
        }

        public void SaveChangesOfInvoice(OutgoingInvoiceDTO dto)
        {
            efDataHelper.SaveChangesOfInvoice(dto);
        }
        #endregion

        #region FOREIGN OUTGOING INVOICE
        public bool GetNameOfForeignOutgoingInvoice(string outgointinvoice)
        {
            return efDataHelper.GetNameOfForeignOutgoingInvoice(outgointinvoice);
        }

            public List<ForeignOutgoingInvoiceDTO> GetOutInoInvoicesByBookId(int id)
        {
            return efDataHelper.GetOutInoInvoicesByBookId(id);
        }

        public List<ForeignOutgoingInvoiceDTO> AllInoOutInvoices()
        {
            return efDataHelper.AllInoOutInvoices();
        }

        public void SaveInoInvoice(ForeignOutgoingInvoiceDTO dto)
        {
            efDataHelper.SaveInoInvoice(dto);
        }

        public List<ForeignOutgoingInvoiceDTO> GetOutInoInvoiceByName(string name)
        {
            return efDataHelper.GetOutInoInvoiceByName(name);
        }

        public List<ForeignOutgoingInvoiceDTO> GetOutInoInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            return efDataHelper.GetOutInoInvoicesByDate(dat1, dat2);
        }

        public ForeignOutgoingInvoiceDTO FindInoOutInvoiceById(int id)
        {
            var invoice = efDataHelper.FindInoOutInvoiceById(id);
            ForeignOutgoingInvoiceDTO dto = new ForeignOutgoingInvoiceDTO();
            dto.DateOfReceipt = invoice.DateOfReceipt;
            dto.NumberOfOutgoingInvoice = invoice.NumberOfForeignOutgoingInvoice;
            dto.AmountInEUR = invoice.AmountInEUR;
            dto.ValueDate = invoice.ValueDate;
            dto.PaymentDate = invoice.PaymentDate;
            dto.Paid = invoice.Paid;
            dto.Comment = invoice.Comment;
            dto.BookId = invoice.BookId;
            dto.ClientId = invoice.ClientId;

            return dto;
        }

        public void DeleteOutInoInvoice(int id)
        {
            efDataHelper.DeleteOutInoInvoice(id);
        }

        public void SaveChangesOfOutInoInvoice(ForeignOutgoingInvoiceDTO dto)
        {
            efDataHelper.SaveChangesOfOutInoInvoice(dto);
        }
        #endregion

        #region ADDITIONAL DOCUMENTATION
        public List<AdditionalDocumentationDTO> AdditionalDocumentationList()
        {
            return efDataHelper.AdditionalDocumentationList();
        }

        public List<AdditionalDocumentationDTO> GetDocumentByName(string name)
        {
            return efDataHelper.GetDocumentByName(name);
        }

        public List<AdditionalDocumentationDTO> GetDocumentByNameAndSubject(string subject, string name)
        {
            return efDataHelper.GetDocumentByNameAndSubject(subject, name);
        }

        public List<AdditionalDocumentationDTO> GetDocumentBySubject(string subject)
        {
            return efDataHelper.GetDocumentBySubject(subject);
        }

        public void DeleteAttachments(int id)
        {
            efDataHelper.DeleteAttachments(id);
        }

        public void SaveDocument(AdditionalDocumentationDTO document, HttpPostedFileBase upload)
        {
            efDataHelper.SaveDocument(document, upload);
        }

        public void SaveAdditionalDocumentation(AdditionalDocumentationDTO document)
        {
            efDataHelper.SaveAdditionalDocumentation(document);
        }

        public AdditionalDocumentationDTO GetDocumentById(int id)
        {
            var doc = efDataHelper.GetDocumentById(id);
            AdditionalDocumentationDTO dto = new AdditionalDocumentationDTO();
            dto.AdditionalDocumentationId = doc.AdditionalDocumentationId;
            dto.Name = doc.Name;
            dto.FileName = doc.FileName;
            dto.BookId = doc.BookId;

            return dto;
        }

        public void SaveDocumentChanges(AdditionalDocumentationDTO dto)
        {
            efDataHelper.SaveDocumentChanges(dto);
        }

        public void SaveDetail(AdditionalDocumentationDTO model)
        {
            efDataHelper.SaveDetail(model);
        }
        #endregion

        #region CLIENT OFFER
        public List<ClientOffersDTO> ClientOffers()
        {
            return efDataHelper.ClientOffers();
        }

        public void DeleteClientOffer(int id)
        {
            efDataHelper.DeleteClientOffer(id);
        }

        public void SaveClientOffer(ClientOffersDTO dto)
        {
            efDataHelper.SaveClientOffer(dto);
        }

        public ClientOffersDTO FindClientOfferById(int id)
        {
            var offer = efDataHelper.FindClientOfferById(id);
            ClientOffersDTO dto = new ClientOffersDTO();
            dto.ClientOfferId = offer.ClientOfferId;
            dto.OffersName = offer.OffersName;
            dto.Description = offer.Descritpion;
            dto.ASA = offer.ASA;
            dto.Accepted = offer.Accepted;
            dto.Date = offer.Date;
            dto.ClientId = offer.ClientId;
            dto.BookId = offer.BookId;
           

            return dto;
        }

        public void SaveClientOfferChanges(ClientOffersDTO offer)
        {
            efDataHelper.SaveClientOfferChanges(offer);
        }

        public List<ClientOffersDTO> GetClientOfferByName(string name)
        {
            return efDataHelper.GetClientOfferByName(name);
        }

        public List<ClientOffersDTO> GetClientOfferByClient(string client)
        {
            return efDataHelper.GetClientOfferByClient(client);
        }

        public List<ClientOffersDTO> FindClientOfferByNameAndClient(string name, string client)
        {
            return efDataHelper.FindClientOfferByNameAndClient(name, client);
        }
        #endregion

        #region NOTIFICATIONS
        public List<InputInvoiceDTO> GetInvoicesByExpireDate()
        {
            return efDataHelper.GetInvoicesByExpireDate();
        }

        public List<ForeignInputInvoiceDTO> GetInoInvoicesByExpireDate()
        {
            return efDataHelper.GetInoInvoicesByExpireDate();
        }
        #endregion

        #region DISTANCE TRANSPORT
        public List<DistanceTransportDTO> GetDistanceTransportList()
        {
            return efDataHelper.GetDistanceTransportList();
        }

        public void DeleteDistanceTransport(int id)
        {
            efDataHelper.DeleteDistanceTransport(id);
        }

        public void SaveDistanceTransport(DistanceTransportDTO dto)
        {
            efDataHelper.SaveDistanceTransport(dto);
        }

        public DistanceTransport FindDistanceTransportById(int id)
        {
            return efDataHelper.FindDistanceTransportById(id);
        }

        public void SaveDistanceTransportChanges(DistanceTransportDTO dto)
        {
            efDataHelper.SaveDistanceTransportChanges(dto);
        }

        public List<DistanceTransportDTO> GetDistanceTransportByShipperId(int id)
        {
            return efDataHelper.GetDistanceTransportByShipperId(id);
        }
        #endregion
    }
}