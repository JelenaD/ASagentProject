﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using CommonLayer;
using DataLayer;

namespace ServiceLayer
{
   
    public class AsAgentService : IAsAgentService
    {

        static ContextClass contextClass = new ContextClass();
        static EfDataHelper efDataHelper = new EfDataHelper(contextClass);
        AsAgentHelper asAgentHelper = new AsAgentHelper(efDataHelper);

        #region ADDRESS BOOK
        public List<AddressBookDTO> GetAddressBookList()
        {
            return asAgentHelper.GetAddressBookList();
        }

        public void SaveAddressBook(AddressBookDTO dto)
        {
            asAgentHelper.SaveAddressBook(dto);
        }

        public AddressBookDTO AddressBookById(int id)
        {
            return asAgentHelper.AddressBookById(id);
        }

        public void SaveAddressChanges(AddressBookDTO dto)
        {
            asAgentHelper.SaveAddressChanges(dto);
        }

        public void DeleteAddressBook(int id)
        {
            asAgentHelper.DeleteAddressBook(id);
        }

        public List<AddressBookDTO> FilterAddressBookByCompanyName(string nameOfCompany)
        {
            return asAgentHelper.FilterAddressBookByCompanyName(nameOfCompany);
        }
        #endregion

        #region BOOK
        public List<BookDTO> GetBookCollection()
        {
            return asAgentHelper.GetBookCollection();
        }

        public List<string> GetDistinctBookingNumbers()
        {
            return asAgentHelper.GetDistinctBookingNumbers();
        }

        public BookDTO GetBookByBookingNumber(string bookingNumber)
        {
            return asAgentHelper.GetBookByBookingNumber(bookingNumber);
        }

        public string GetNameOfClientByBookId(int id)
        {
            return asAgentHelper.GetNameOfClientByBookId(id);
        }

        public List<BookDTO> FindBookByObject(string objectBook)
        {
            return asAgentHelper.FindBookByObject(objectBook);
        }

        public void DeleteBook(int id)
        {
            asAgentHelper.DeleteBook(id);
        }

        public void SaveBook(BookDTO dto)
        {
            asAgentHelper.SaveBook(dto);
        }

        public BookDTO BookById(int id)
        {
            return asAgentHelper.BookById(id);
        }

        public void SaveBookChanges(BookDTO dto)
        {
            asAgentHelper.SaveBookChanges(dto);
        }

        public double? GetPriceOfInputInvoice(int bookId)
        {
            return asAgentHelper.GetPriceOfInputInvoice(bookId);
        }

        public double? GetPriceOfForeignInputInovice(int bookId)
        {
            return asAgentHelper.GetPriceOfForeignInputInovice(bookId);
        }

        public double? GetPriceOfOutgoingInvoice(int bookId)
        {
            return asAgentHelper.GetPriceOfOutgoingInvoice(bookId);
        }

        public double? GetPriceOfForeignOutgoingInvoice(int bookId)
        {
            return asAgentHelper.GetPriceOfForeignOutgoingInvoice(bookId);
        }
        #endregion

        #region ASA BOOKING NUMBER
        public void SaveAsaBookingNumberChanges(ASABookingNumberDTO dto)
        {
            asAgentHelper.SaveAsaBookingNumberChanges(dto);
        }


        public void SaveAsaBookingNumber(ASABookingNumberDTO dto)
        {
            asAgentHelper.SaveAsaBookingNumber(dto);
        }

        public void DeleteAsaBookingNumber(int id)
        {
            asAgentHelper.DeleteAsaBookingNumber(id);
        }

        public List<ASABookingNumberDTO> GetAllAsaNumbers()
        {
            return asAgentHelper.GetAllAsaNumbers();
        }

        public ASABookingNumberDTO AsaBookingNumberById(int id)
        {
            return asAgentHelper.AsaBookingNumberById(id);
        }
        #endregion

        #region STANDARD BOOKING NUMBER
        public void SaveStandardBookingNumber(StandardBookingNumberDTO dto)
        {
            asAgentHelper.SaveStandardBookingNumber(dto);
        }

        public List<StandardBookingNumberDTO> GetAllStandardNumbers()
        {
            return asAgentHelper.GetAllStandardNumbers();
        }

        public StandardBookingNumberDTO StandardBookingNumberId(int id)
        {
            return asAgentHelper.StandardBookingNumberId(id);
        }

        public void DeleteStandardBookingNumber(int id)
        {
            asAgentHelper.DeleteStandardBookingNumber(id);
        }

        public void SaveStandardBookingNumberChanges(StandardBookingNumberDTO dto)
        {
            asAgentHelper.SaveStandardBookingNumberChanges(dto);
        }
        #endregion

        #region USER

        public bool CheckIfUserExists(string username, string password)
        {
            return asAgentHelper.CheckIfUserExists(username, password);
        }

        public User CheckIfUserExistsByUsernameAndPassword(string username, string password)
        {
            return asAgentHelper.CheckIfUserExistsByUsernameAndPassword(username, password);
        }

        public List<UserDTO> AllUsers()
        {
            return asAgentHelper.AllUsers();
        }

        public void SaveUser(UserDTO dto)
        {
            asAgentHelper.SaveUser(dto);
        }

        public void SaveChanges(UserDTO dto)
        {
            asAgentHelper.SaveChanges(dto);
        }

        public void DeleteUser(int id)
        {
            asAgentHelper.DeleteUser(id);
        }

        public UserDTO FindById(int id)
        {
            return asAgentHelper.FindById(id);
        }
        #endregion

        #region CLIENT
        public void SaveClientChanges(ClientDTO dto)
        {
            asAgentHelper.SaveClientChanges(dto);
        }

        public ClientDTO FindClientById(int id)
        {
            return asAgentHelper.FindClientById(id);
        }

        public List<ClientDTO> AllClients()
        {
            return asAgentHelper.AllClients();
        }

        public void SaveClient(ClientDTO client)
        {
            asAgentHelper.SaveClient(client);
        }

        public List<ClientDTO> FilterClientByName(string companyName)
        {
            return asAgentHelper.FilterClientByName(companyName);
        }

        public List<ClientDTO> FilterClientByState(string state)
        {
            return asAgentHelper.FilterClientByState(state);
        }

        public List<ClientDTO> GetClientsByNameAndCountry(string name, string country)
        {
            return asAgentHelper.GetClientsByNameAndCountry(name, country);
        }

        public void DeleteClient(int id)
        {
            asAgentHelper.DeleteClient(id);
        }
        #endregion

        #region COST
        public List<CostsDTO> AllCosts()
        {
            return asAgentHelper.AllCosts();
        }

        public void SaveCost(CostsDTO dto)
        {
            asAgentHelper.SaveCost(dto);
        }

        public List<CostsDTO> FilterCostsByName(string name)
        {
            return asAgentHelper.FilterCostsByName(name);
        }

        public CostsDTO FindCostById(int id)
        {
            return asAgentHelper.FindCostById(id);
        }

        public void DeleteCost(int id)
        {
            asAgentHelper.DeleteCost(id);
        }

        public void SaveCostChanges(CostsDTO costs)
        {
            asAgentHelper.SaveCostChanges(costs);
        }

        public List<CostsDTO> GetCostByDate(DateTime? date1, DateTime? date2)
        {
            return asAgentHelper.GetCostByDate(date1, date2);
        }

        public List<CostsDTO> GetCostsByDateAndName(DateTime? date1, DateTime? date2, string account)
        {
            return asAgentHelper.GetCostsByDateAndName(date1, date2, account);
        }
        #endregion

        #region SHIPPER
        public List<ShipperDTO> Shippers()
        {
            return asAgentHelper.Shippers();
        }

        public void SaveShipper(ShipperDTO dto)
        {
            asAgentHelper.SaveShipper(dto);
        }

        public ShipperDTO FindShipperById(int id)
        {
            return asAgentHelper.FindShipperById(id);
        }

        public void DeleteShipper(int id)
        {
            asAgentHelper.DeleteShipper(id);
        }

        public void SaveShipperChanges(ShipperDTO shipper)
        {
            asAgentHelper.SaveShipperChanges(shipper);
        }
        #endregion

        #region SHIPPER OFFER
        public List<ShipperOfferDTO> ShipperOffers()
        {
            return asAgentHelper.ShipperOffers();
        }

        public void SaveShipperOffer(ShipperOfferDTO dto)
        {
            asAgentHelper.SaveShipperOffer(dto);
        }

        public void DeleteOffer(int id)
        {
            asAgentHelper.DeleteOffer(id);
        }

        public ShipperOfferDTO FindShipperOfferById(int id)
        {
            return asAgentHelper.FindShipperOfferById(id);
        }

        public ShipperOfferDTO GetShipperOfferByShipperId(int id)
        {
            return asAgentHelper.GetShipperOfferByShipperId(id);
        }

        public void SaveChangesOfShipperOffer(ShipperOfferDTO offer)
        {
            asAgentHelper.SaveChangesOfShipperOffer(offer);
        }

        public List<ShipperOfferDTO> GetPartnerOffersByName(string name)
        {
            return asAgentHelper.GetPartnerOffersByName(name);
        }

        public List<ShipperOfferDTO> GetShipperOffersByShipper(string shipper)
        {
            return asAgentHelper.GetShipperOffersByShipper(shipper);
        }

        public List<ShipperOfferDTO> GetShipperOffersByNameAndShipper(string shipper, string name)
        {
            return asAgentHelper.GetShipperOffersByNameAndShipper(shipper, name);
        }
        #endregion

        #region INPUT INVOICE
        public List<InputInvoiceDTO> GetInvoiceByBookId(int id)
        {
            return asAgentHelper.GetInvoiceByBookId(id);
        }

        public List<InputInvoiceDTO> InputInvoice()
        {
            return asAgentHelper.InputInvoice();
        }

        public List<InputInvoiceDTO> GetInInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            return asAgentHelper.GetInInvoicesByDate(dat1, dat2);
        }

        public List<InputInvoiceDTO> GetInInvoicesByName(string name)
        {
            return asAgentHelper.GetInInvoicesByName(name);
        }

        public List<InputInvoiceDTO> GetInInvoicesBySubject(string subject)
        {
            return asAgentHelper.GetInInvoicesBySubject(subject);
        }

        public void DeleteInInvoice(int id)
        {
            asAgentHelper.DeleteInInvoice(id);
        }

        public InputInvoiceDTO FindInputInvoiceById(int id)
        {
            return asAgentHelper.FindInputInvoiceById(id);
        }

        public void SaveInInvoice(InputInvoiceDTO dto)
        {
            asAgentHelper.SaveInInvoice(dto);
        }

        public void SaveInInvoiceChanges(InputInvoiceDTO dto)
        {
            asAgentHelper.SaveInInvoiceChanges(dto);
        }
        #endregion

        #region FOREIGN INPUT INVOICE

        public List<ForeignInputInvoiceDTO> GetInoInvoiceByBookId(int id)
        {
            return asAgentHelper.GetInoInvoiceByBookId(id);
        }

        public List<ForeignInputInvoiceDTO> GetAllInoInInvoices()
        {
            return asAgentHelper.GetAllInoInInvoices();
        }

        public List<ForeignInputInvoiceDTO> GetInInoInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            return asAgentHelper.GetInInoInvoicesByDate(dat1, dat2);
        }

        public List<ForeignInputInvoiceDTO> GetInInoInvoicesByName(string name)
        {
            return asAgentHelper.GetInInoInvoicesByName(name);
        }

        public List<ForeignInputInvoiceDTO> GetInInoInvoicesBySubject(string subject)
        {
            return asAgentHelper.GetInInoInvoicesBySubject(subject);
        }

        public void DeleteInoInInvoice(int id)
        {
            asAgentHelper.DeleteInoInInvoice(id);
        }

        public ForeignInputInvoiceDTO FindInoInInvoiceById(int id)
        {
            return asAgentHelper.FindInoInInvoiceById(id);
        }

        public void SaveInoInInvoice(ForeignInputInvoiceDTO dto)
        {
            asAgentHelper.SaveInoInInvoice(dto);
        }

        public void SaveInoInInvoiceChanges(ForeignInputInvoiceDTO dto)
        {
            asAgentHelper.SaveInoInInvoiceChanges(dto);
        }
        #endregion  

        #region OUTGOING INVOICE
        public List<OutgoingInvoiceDTO> GetOutInvoicesByBookId(int id)
        {
            return asAgentHelper.GetOutInvoicesByBookId(id);
        }

        public List<OutgoingInvoiceDTO> OutgoingInvoices()
        {
            return asAgentHelper.OutgoingInvoices();
        }

        public void SaveInvoice(OutgoingInvoiceDTO dto)
        {
            asAgentHelper.SaveInvoice(dto);
        }

        public List<OutgoingInvoiceDTO> GetOutInvoiceByName(string name)
        {
            return asAgentHelper.GetOutInvoiceByName(name);
        }

        public List<OutgoingInvoiceDTO> GetOutInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            return asAgentHelper.GetOutInvoicesByDate(dat1, dat2);
        }

        public OutgoingInvoiceDTO FindIInvoiceById(int id)
        {
            return asAgentHelper.FindIInvoiceById(id);
        }

        public void DeleteOutInvoice(int id)
        {
            asAgentHelper.DeleteOutInvoice(id);
        }

        public void SaveChangesOfInvoice(OutgoingInvoiceDTO dto)
        {
            asAgentHelper.SaveChangesOfInvoice(dto);
        }
        #endregion

        #region FOREIGN OUTGOING INVOICE
        public List<ForeignOutgoingInvoiceDTO> GetOutInoInvoicesByBookId(int id)
        {
            return asAgentHelper.GetOutInoInvoicesByBookId(id);
        }

        public List<ForeignOutgoingInvoiceDTO> AllInoOutInvoices()
        {
            return asAgentHelper.AllInoOutInvoices();
        }

        public void SaveInoInvoice(ForeignOutgoingInvoiceDTO dto)
        {
            asAgentHelper.SaveInoInvoice(dto);
        }

        public List<ForeignOutgoingInvoiceDTO> GetOutInoInvoiceByName(string name)
        {
            return asAgentHelper.GetOutInoInvoiceByName(name);
        }

        public List<ForeignOutgoingInvoiceDTO> GetOutInoInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            return asAgentHelper.GetOutInoInvoicesByDate(dat1, dat2);
        }

        public ForeignOutgoingInvoiceDTO FindInoOutInvoiceById(int id)
        {
            return asAgentHelper.FindInoOutInvoiceById(id);
        }

        public void DeleteOutInoInvoice(int id)
        {
            asAgentHelper.DeleteOutInoInvoice(id);
        }

        public void SaveChangesOfOutInoInvoice(ForeignOutgoingInvoiceDTO dto)
        {
            asAgentHelper.SaveChangesOfOutInoInvoice(dto);
        }
        #endregion

        #region ADDITIONAL DOCUMENTATION
        public List<AdditionalDocumentationDTO> AdditionalDocumentationList()
        {
            return asAgentHelper.AdditionalDocumentationList();
        }

        public List<AdditionalDocumentationDTO> GetDocumentByName(string name)
        {
            return asAgentHelper.GetDocumentByName(name);
        }

        public List<AdditionalDocumentationDTO> GetDocumentByNameAndSubject(string subject, string name)
        {
            return asAgentHelper.GetDocumentByNameAndSubject(subject, name);
        }

        public List<AdditionalDocumentationDTO> GetDocumentBySubject(string subject)
        {
            return asAgentHelper.GetDocumentBySubject(subject);
        }

        public void DeleteAttachments(int id)
        {
            asAgentHelper.DeleteAttachments(id);
        }

        public void SaveDocument(AdditionalDocumentationDTO document, HttpPostedFileBase upload)
        {
            asAgentHelper.SaveDocument(document, upload);
        }

        public void SaveAdditionalDocumentation(AdditionalDocumentationDTO document)
        {
            asAgentHelper.SaveAdditionalDocumentation(document);
        }

        public AdditionalDocumentationDTO GetDocumentById(int id)
        {
            return asAgentHelper.GetDocumentById(id);
        }

        public void SaveDocumentChanges(AdditionalDocumentationDTO dto)
        {
            asAgentHelper.SaveDocumentChanges(dto);
        }

        public void SaveDetail(AdditionalDocumentationDTO model)
        {
            asAgentHelper.SaveDetail(model);
        }
        #endregion

        #region CLIENT OFFER
        public List<ClientOffersDTO> ClientOffers()
        {
            return asAgentHelper.ClientOffers();
        }

        public void DeleteClientOffer(int id)
        {
            asAgentHelper.DeleteClientOffer(id);
        }

        public void SaveClientOffer(ClientOffersDTO dto)
        {
            asAgentHelper.SaveClientOffer(dto);
        }

        public ClientOffersDTO FindClientOfferById(int id)
        {
            return asAgentHelper.FindClientOfferById(id);
        }

        public void SaveClientOfferChanges(ClientOffersDTO offer)
        {
            asAgentHelper.SaveClientOfferChanges(offer);
        }

        public List<ClientOffersDTO> GetClientOfferByName(string name)
        {
            return asAgentHelper.GetClientOfferByName(name);
        }

        public List<ClientOffersDTO> GetClientOfferByClient(string client)
        {
            return asAgentHelper.GetClientOfferByClient(client);
        }

        public List<ClientOffersDTO> FindClientOfferByNameAndClient(string name, string client)
        {
            return asAgentHelper.FindClientOfferByNameAndClient(name, client);
        }
        #endregion

        #region NOTIFICATIONS
        public List<ForeignInputInvoiceDTO> GetInoInvoicesByExpireDate()
        {
            return asAgentHelper.GetInoInvoicesByExpireDate();
        }

        public List<InputInvoiceDTO> GetInvoicesByExpireDate()
        {
            return asAgentHelper.GetInvoicesByExpireDate();
        }

        public List<InputInvoiceDTO> GetEmailInvoicesByExpireDate()
        {
            return asAgentHelper.GetEmailInvoicesByExpireDate(); ;
        }

        public List<ForeignInputInvoiceDTO> GetEmailINOInvoicesByExpireDate()
        {
            return asAgentHelper.GetEmailINOInvoicesByExpireDate();
        }

        public bool GetUsername(string username)
        {
            return asAgentHelper.GetUsername(username);
        }

        public bool DateOfBook(DateTime date)
        {
            return asAgentHelper.DateOfBook(date);
        }

        public bool GetNameOfInputInvoice(string inputinvoice)
        {
            return asAgentHelper.GetNameOfInputInvoice(inputinvoice);
        }

        public bool GetNameOfForeignInputInvoice(string inputinvoice)
        {
            return asAgentHelper.GetNameOfForeignInputInvoice(inputinvoice);
        }

        public bool GetNameOfOutgoingInvoice(string outgointinvoice)
        {
            return asAgentHelper.GetNameOfOutgoingInvoice(outgointinvoice);
        }

        public bool GetNameOfForeignOutgoingInvoice(string outgointinvoice)
        {
            return asAgentHelper.GetNameOfForeignOutgoingInvoice(outgointinvoice);
        }

        public List<ForeignOutgoingInvoiceDTO> GetforeignOutgoingInvoicesByExpireDate()
        {
            return asAgentHelper.GetforeignOutgoingInvoicesByExpireDate();
        }

        public List<OutgoingInvoiceDTO> GetOutgoingInvoicesByExpireDate()
        {
            return asAgentHelper.GetOutgoingInvoicesByExpireDate();
        }

        public List<ShipperOfferDTO> GetShipperOffersByCOId(int id)
        {
            return asAgentHelper.GetShipperOffersByCOId(id);
        }

        #endregion

        #region DISTANCE TRANSPORT
        public List<DistanceTransportDTO> GetDistanceTransportList()
        {
            return asAgentHelper.GetDistanceTransportList();
        }

        public void DeleteDistanceTransport(int id)
        {
            efDataHelper.DeleteDistanceTransport(id);
        }

        public void SaveDistanceTransport(DistanceTransportDTO dto)
        {
            asAgentHelper.SaveDistanceTransport(dto);
        }

        public DistanceTransport FindDistanceTransportById(int id)
        {
            return asAgentHelper.FindDistanceTransportById(id);
        }

        public void SaveDistanceTransportChanges(DistanceTransportDTO dto)
        {
            asAgentHelper.SaveDistanceTransportChanges(dto);
        }

        public List<DistanceTransportDTO> GetDistanceTransportByShipperId(int id)
        {
            return asAgentHelper.GetDistanceTransportByShipperId(id);
        }

        #endregion
    }
}
