﻿using CommonLayer;
using DataLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;

namespace ServiceLayer
{
    public class EfDataHelper
    {
        private ContextClass contextClass;

        public EfDataHelper(ContextClass contextClass)
        {
            this.contextClass = contextClass;
        }

        public List<ShipperOfferDTO> GetShipperOffersByCOId(int id)
        {
            var offer =  FindClientOfferById(id);
            List<ShipperOffer> shipperOffers = contextClass.ShipperOffer.Where(p => p.ClientOffId == offer.ClientOfferId)
                .Include(p => p.Shipper)
                .Include(p => p.ClientOffer).ToList();

            List<ShipperOfferDTO> shipperOffersDTO = new List<ShipperOfferDTO>(shipperOffers.Count);
            foreach(var shipper in shipperOffers)
            {
                ShipperOfferDTO dto = new ShipperOfferDTO();
                dto.ShipperOfferId = shipper.ShipperOfferId;
                dto.ShipperOfferName = shipper.ShipperOfferName;
                dto.Date = shipper.Date;
                dto.Description = shipper.Description;
                dto.Price = shipper.Price;
                dto.Email = shipper.Email;
                dto.Telephone = shipper.Telephone;
                dto.Distance = shipper.Distance;
                dto.ClientOfferName = shipper.ClientOffer.OffersName;
                dto.ShipperName = shipper.Shipper.CompanyName;

                shipperOffersDTO.Add(dto);
            }
            return shipperOffersDTO;

        }
       

        public bool GetNameOfInputInvoice(string inputinvoice)
        {
            var invoice = contextClass.InputInvoice.Where(v => v.NumberOfInputInvoice == inputinvoice).Select(v => v.NumberOfInputInvoice).FirstOrDefault();

            if (invoice == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region ADDRESS BOOK
        public List<AddressBookDTO> GetAddressBookList()
        {
            List<AddressBook> addressBookList = contextClass.AddressBook.ToList();
            List<AddressBookDTO> addresses = new List<AddressBookDTO>(addressBookList.Count);

            foreach (var l in addressBookList)
            {
                AddressBookDTO dto = new AddressBookDTO();
                dto.AddressBookId = l.AddressBookId;
                dto.NameOfCompany = l.Company;
                dto.Name = l.Name;
                dto.LastName = l.LastName;
                dto.Email = l.Email;
                dto.Telephone = l.Telephone;

                addresses.Add(dto);
            }
            return addresses;
        }

        public void SaveAddressBook(AddressBookDTO dto)
        {
            AddressBook addressBook = new AddressBook();
            addressBook.Company = dto.NameOfCompany;
            addressBook.Name = dto.Name;
            addressBook.LastName = dto.LastName;
            addressBook.Email = dto.Email;
            addressBook.Telephone = dto.Telephone;

            contextClass.AddressBook.Add(addressBook);
            contextClass.SaveChanges();
        }

        public AddressBook AddressBookById(int id)
        {
            var addressBook = contextClass.AddressBook.Where(tr => tr.AddressBookId == id).FirstOrDefault();
            return addressBook;
        }

        public void SaveAddressChanges(AddressBookDTO dto)
        {
            AddressBook address = AddressBookById(dto.AddressBookId);
            address.Company = dto.NameOfCompany;
            address.Name = dto.Name;
            address.LastName = dto.LastName;
            address.Email = dto.Email;
            address.Telephone = dto.Telephone;

            contextClass.SaveChanges();
        }

        public void DeleteAddressBook(int id)
        {
            AddressBook address = contextClass.AddressBook.Where(tr => tr.AddressBookId == id).FirstOrDefault();
            contextClass.AddressBook.Remove(address);
            contextClass.SaveChanges();
        }

        public List<AddressBookDTO> FilterAddressBookByCompanyName(string nameOfCompany)
        {
            List<AddressBook> addressBooks = contextClass.AddressBook.Where(t => t.Company.StartsWith(nameOfCompany) || nameOfCompany == null).ToList();
            List<AddressBookDTO> addressBooksDTO = new List<AddressBookDTO>();
            addressBooks.ForEach(x => addressBooksDTO.Add(new AddressBookDTO(x.AddressBookId, x.Company, x.Name, x.LastName, x.Email, x.Telephone)));
            return addressBooksDTO;
        }
        #endregion

        #region ASA BOOKING NUMBER
        public void SaveAsaBookingNumberChanges(ASABookingNumberDTO dto)
        {
            AsaBookingNumber address = AsaBookingNumberById(dto.AsaBookingNumberId);
            address.AsaNumber = Convert.ToInt32(dto.AsaNumber);
            address.Distance = dto.Distance;
            address.Container = dto.Container;


            contextClass.SaveChanges();
        }

        public void SaveAsaBookingNumber(ASABookingNumberDTO dto)
        {
            IEnumerable<int> asaNumbers = contextClass.AsaBookingNumber.Select(x => x.AsaNumber).ToList<int>();

            int number = asaNumbers.Max();

            AsaBookingNumber asa = new AsaBookingNumber();

            asa.AsaNumber = number + 1;
            asa.Distance = dto.Distance;
            asa.Container = dto.Container;

            contextClass.AsaBookingNumber.Add(asa);
            contextClass.SaveChanges();
        }


        public AsaBookingNumber AsaBookingNumberById(int id)
        {
            var asa = contextClass.AsaBookingNumber.Where(tr => tr.AsaBookingNumberId == id).FirstOrDefault();
            return asa;
        }

        public void DeleteAsaBookingNumber(int id)
        {
            AsaBookingNumber asa = contextClass.AsaBookingNumber.Where(tr => tr.AsaBookingNumberId == id).FirstOrDefault();
            contextClass.AsaBookingNumber.Remove(asa);
            contextClass.SaveChanges();
        }

        public List<ASABookingNumberDTO> GetAllAsaNumbers()
        {
            var listAsaNumbers = contextClass.AsaBookingNumber.ToList();
            List<ASABookingNumberDTO> asanumbers = new List<ASABookingNumberDTO>(listAsaNumbers.Count);
            foreach (var l in listAsaNumbers)
            {
                ASABookingNumberDTO dto = new ASABookingNumberDTO()
                {
                    AsaBookingNumberId = l.AsaBookingNumberId,
                    AsaNumber = l.AsaNumber + "/18",
                    Distance = l.Distance,
                    Container = l.Container
                };
                asanumbers.Add(dto);
            }
            return asanumbers;
        }
        #endregion

        #region STANDARD BOOKING NUMBER
        public void SaveStandardBookingNumber(StandardBookingNumberDTO dto)
        {
            IEnumerable<int> standardNumbers = contextClass.StandardBookingNumber.Select(x => x.StandardNumber).ToList<int>();
            int number = standardNumbers.Max();

            StandardBookingNumber standard = new StandardBookingNumber();
            standard.StandardNumber = number + 1;
            standard.Distance = dto.Distance;
            standard.Container = dto.Container;

            contextClass.StandardBookingNumber.Add(standard);
            contextClass.SaveChanges();
        }

        public List<StandardBookingNumberDTO> GetAllStandardNumbers()
        {
            var listOfStandardNumbers = contextClass.StandardBookingNumber.ToList();
            List<StandardBookingNumberDTO> listaDTO = new List<StandardBookingNumberDTO>(listOfStandardNumbers.Count);

            foreach (var l in listOfStandardNumbers)
            {
                StandardBookingNumberDTO dto = new StandardBookingNumberDTO()
                {
                    StandardBookingNumberId = l.StandardBookingNumberId,
                    StandardNumber = l.StandardNumber + "/18",
                    Distance = l.Distance,
                    Container = l.Container
                };
                listaDTO.Add(dto);
            }
            return listaDTO;
        }

        public StandardBookingNumber StandardBookingNumberId(int id)
        {
            var asa = contextClass.StandardBookingNumber.Where(tr => tr.StandardBookingNumberId == id).FirstOrDefault();
            return asa;
        }

        public void SaveStandardBookingNumberChanges(StandardBookingNumberDTO dto)
        {
            StandardBookingNumber address = StandardBookingNumberId(dto.StandardBookingNumberId);
            address.StandardNumber = Convert.ToInt16(dto.StandardNumber);
            address.Distance = dto.Distance;
            address.Container = dto.Container;


            contextClass.SaveChanges();
        }

        public void DeleteStandardBookingNumber(int id)
        {
            StandardBookingNumber standard = contextClass.StandardBookingNumber.Where(tr => tr.StandardBookingNumberId == id).FirstOrDefault();
            contextClass.StandardBookingNumber.Remove(standard);
            contextClass.SaveChanges();
        }
        #endregion

        #region BOOK
        public bool DateOfBook(DateTime date)
        {
            var datee = contextClass.Book.Where(v => v.DateOfLoading == date).Select(v => v.DateOfLoading).FirstOrDefault();

            if (datee == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<BookDTO> GetBookCollection()
        {
            IOrderedEnumerable<Book> books = contextClass.Book.ToList().OrderBy(p => p.BookObject);

            List<BookDTO> booksDTO = new List<BookDTO>(books.Count());
            foreach (var b in books)
            {
                BookDTO dto = new BookDTO();
                dto.BookId = b.BookId;
                dto.ObjectBook = b.BookObject;
                dto.PlaceOfLoading = b.PlaceOfLoading;
                dto.DateOfLoading = b.DateOfLoading;
                dto.PlaceOfUnloading = b.PlaceOfUnloading;
                dto.Goods = b.Goods;
                dto.Customer = b.Customer;
                dto.QuantityOfGoods = b.QuantityOfGoods;
                dto.ContainerTruck = b.ContainerTruck;
                dto.BookingNumber = b.BookingNumber;
                booksDTO.Add(dto);
            }
            return booksDTO;
        }

        public List<string> GetDistinctBookingNumbers()
        {
            var listOfBookingNumbers = contextClass.Book.Select(x => x.BookingNumber).Distinct().ToList();
            return listOfBookingNumbers;
        }

        public BookDTO GetBookByBookingNumber(string bookingNumber)
        {
            var book = contextClass.Book.Where(x => x.BookingNumber == bookingNumber).FirstOrDefault();

            BookDTO dto = new BookDTO();
            dto.BookId = book.BookId;
            dto.ObjectBook = book.BookObject;
            dto.PlaceOfLoading = book.PlaceOfLoading;
            dto.DateOfLoading = book.DateOfLoading;
            dto.PlaceOfUnloading = book.PlaceOfUnloading;
            dto.Goods = book.Goods;
            dto.Customer = book.Customer;
            dto.QuantityOfGoods = book.QuantityOfGoods;
            dto.ContainerTruck = book.ContainerTruck;
            dto.BookingNumber = book.BookingNumber;

            return dto;
        }

        public string GetNameOfClientByBookId(int id)
        {
            List<OutgoingInvoice> fakture = contextClass.OutgoingInvoice.Where(p => p.BookId == id).ToList();
            if (fakture.Count == 0)
            {
                return string.Empty;
            }

            int klijentId = fakture[0].ClientId;
            Client klijent = contextClass.Client.Where(p => p.ClientId == klijentId).FirstOrDefault();
            return klijent.NameOfClientCompany;
        }

        public List<BookDTO> FindBookByObject(string objectBook)
        {
            List<Book> books = contextClass.Book.Where(p => p.BookObject.StartsWith(objectBook)).ToList();

            List<BookDTO> booksDTO = new List<BookDTO>(books.Count);
            foreach (var book in books)
            {
                BookDTO dto = new BookDTO();
                dto.BookId = book.BookId;
                dto.ObjectBook = book.BookObject;
                dto.PlaceOfLoading = book.PlaceOfLoading;
                dto.DateOfLoading = book.DateOfLoading;
                dto.PlaceOfUnloading = book.PlaceOfUnloading;
                dto.Goods = book.Goods;
                dto.Customer = book.Customer;
                dto.QuantityOfGoods = book.QuantityOfGoods;
                dto.ContainerTruck = book.ContainerTruck;
                dto.BookingNumber = book.BookingNumber;

                booksDTO.Add(dto);
            }
            return booksDTO;
        }

        public void DeleteBook(int id)
        {
            Book book = contextClass.Book.Where(p => p.BookId == id).FirstOrDefault();
            contextClass.Book.Remove(book);
            contextClass.SaveChanges();
        }

        public void SaveBook(BookDTO dto)
        {

            Book book = new Book();
            book.BookObject = dto.ObjectBook;
            book.PlaceOfLoading = dto.PlaceOfLoading;
            book.DateOfLoading = dto.DateOfLoading;
            book.PlaceOfUnloading = dto.PlaceOfUnloading;
            book.Goods = dto.Goods;
            book.Customer = dto.Customer;
            book.QuantityOfGoods = dto.QuantityOfGoods;
            book.ContainerTruck = dto.ContainerTruck;
            book.AsaBookingNumberId = dto.AsaBookingNumberId;
            book.StandardBookingNumberId = dto.StandardBookingNumberId;

            if (dto.StandardBookingNumberId != 0)
            {
                book.AsaBookingNumberId = null;
                var number = contextClass.StandardBookingNumber.Where(x => x.StandardBookingNumberId == book.StandardBookingNumberId).Select(x => x.StandardNumber).FirstOrDefault();
                book.BookingNumber = number.ToString();

            }
            else
            {
                book.StandardBookingNumberId = null;
                var nubmer = contextClass.AsaBookingNumber.Where(x => x.AsaBookingNumberId == book.AsaBookingNumberId).Select(x => x.AsaNumber).FirstOrDefault();
                book.BookingNumber = nubmer.ToString();

            }

            contextClass.Book.Add(book);
            contextClass.SaveChanges();
        }

        public Book BookById(int id)
        {
            var book = contextClass.Book.Where(p => p.BookId == id).FirstOrDefault();
            return book;
        }

        public void SaveBookChanges(BookDTO dto)
        {
            Book book = BookById(dto.BookId);
            book.BookObject = dto.ObjectBook;
            book.PlaceOfLoading = dto.PlaceOfLoading;
            book.DateOfLoading =dto.DateOfLoading;
            book.PlaceOfUnloading = dto.PlaceOfUnloading;
            book.Goods = dto.Goods;
            book.Customer = dto.Customer;
            book.QuantityOfGoods = dto.QuantityOfGoods;
            book.ContainerTruck = dto.ContainerTruck;
            book.BookingNumber = dto.BookingNumber;

            contextClass.SaveChanges();
        }

        public double? GetPriceOfInputInvoice(int bookId)
        {
            double? sum = 0;
            List<InputInvoice> invoices = contextClass.InputInvoice.Where(p => p.BookId == bookId).ToList();
            if (invoices.Count == 0)
            {
                return sum;
            }

            foreach (var invoice in invoices)
            {
                sum += invoice.AmountInEUR;
            }

            return sum;
        }

        public double? GetPriceOfForeignInputInovice(int bookId)
        {
            double? sum = 0;
            List<ForeignInputInvoice> invoices = contextClass.ForeignInputInvoice.Where(p => p.BookId == bookId).ToList();
            if (invoices.Count == 0)
            {
                return sum;
            }

            foreach (var invoice in invoices)
            {
                sum += invoice.AmountInEUR;
            }

            return sum;
        }

        public double? GetPriceOfOutgoingInvoice(int bookId)
        {
            double? sum = 0;
            List<OutgoingInvoice> invoices = contextClass.OutgoingInvoice.Where(p => p.BookId == bookId).ToList();
            if (invoices.Count == 0)
            {
                return sum;
            }

            foreach (var invoice in invoices)
            {
                sum += invoice.AmountInEUR;
            }

            return sum;
        }

        public double? GetPriceOfForeignOutgoingInvoice(int bookId)
        {
            double? sum = 0;
            List<ForeignOutgoingInvoice> invoices = contextClass.ForeignOutgoingInvoice.Where(p => p.BookId == bookId).ToList();
            if (invoices.Count == 0)
            {
                return sum;
            }

            foreach (var invoice in invoices)
            {
                sum += invoice.AmountInEUR;
            }

            return sum;
        }
        #endregion

        #region USER
        public bool GetUsername(string username)
        {
            var userName = contextClass.User.Where(v => v.Username == username).Select(v => v.Username).FirstOrDefault();

            if (userName == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public User GetUserByUsernameAndPassword(string username, string password)
        {
            var user = contextClass.User.Where(k => k.Username == username &&
                k.Password == password).FirstOrDefault();
            return user;
        }

        public List<UserDTO> AllUsers()
        {
            List<User> users;
            users = contextClass.User.ToList();

            List<UserDTO> usersDTO = new List<UserDTO>(users.Count());
            foreach (var k in users)
            {
                UserDTO dto = new UserDTO();
                dto.UserId = k.UserId;
                dto.Name = k.Name;
                dto.LastName = k.LastName;
                dto.Address = k.Address;
                dto.Email = k.Email;
                dto.Telephone = k.Telephone;
                dto.Username = k.Username;
                dto.Password = k.Password;
                dto.UserRole = k.Role;

                usersDTO.Add(dto);
            }
            return usersDTO;
        }

        public void SaveUser(UserDTO dto)
        {
            User user = new User();
            user.Name = dto.Name;
            user.LastName = dto.LastName;
            user.Address = dto.Address;
            user.Email = dto.Email;
            user.Telephone = dto.Telephone;
            user.Username = dto.Username;
            user.Password = dto.Password;
            user.Role = dto.UserRole;

            contextClass.User.Add(user);
            contextClass.SaveChanges();
        }

        public void SaveChanges(UserDTO dto)
        {
            User user = FindById(dto.UserId);

            user.Name = dto.Name;
            user.LastName = dto.LastName;
            user.Address = dto.Address;
            user.Email = dto.Email;
            user.Telephone = dto.Telephone;
            user.Username = dto.Username;
            user.Password = dto.Password;
            user.Role = dto.UserRole;

            contextClass.SaveChanges();
        }

        public void DeleteUser(int id)
        {
            User user = contextClass.User.Where(k => k.UserId == id).FirstOrDefault();
            contextClass.User.Remove(user);
            contextClass.SaveChanges();
        }

        public User FindById(int id)
        {
            var user = contextClass.User.Where(k => k.UserId == id).FirstOrDefault();
            return user;
        }
        #endregion

        #region SHIPPER
        public List<ShipperDTO> Shippers()
        {
            List<Shipper> shippers;
            shippers = contextClass.Shipper.ToList();

            List<ShipperDTO> list = new List<ShipperDTO>(shippers.Count());
            foreach (var par in shippers)
            {
                ShipperDTO dto = new ShipperDTO();
                dto.ShipperId = par.ShipperId;
                dto.Pib = par.PIB;
                dto.CompanyName = par.CompanyName;
                dto.Address = par.Address;
                dto.Email = par.Email;
                dto.Telephone = par.Telephone;
                dto.State = par.State;
                dto.ZipCode = par.ZipCode;
                dto.City = par.City;
                list.Add(dto);
            }
            return list;
        }

        public void SaveShipper(ShipperDTO dto)
        {
            Shipper shipper = new Shipper();
            shipper.PIB = dto.Pib;
            shipper.CompanyName = dto.CompanyName;
            shipper.Address = dto.Address;
            shipper.Email = dto.Email;
            shipper.Telephone = dto.Telephone;
            shipper.State = dto.State;
            shipper.ZipCode = dto.ZipCode;
            shipper.City = dto.City;

            contextClass.Shipper.Add(shipper);
            contextClass.SaveChanges();
        }

        public Shipper FindShipperById(int id)
        {
            var shipper = contextClass.Shipper.Where(p => p.ShipperId == id).FirstOrDefault();
            return shipper;
        }

        public void DeleteShipper(int id)
        {
            Shipper shipper = contextClass.Shipper.Where(p => p.ShipperId == id).FirstOrDefault();
            contextClass.Shipper.Remove(shipper);
            contextClass.SaveChanges();
        }

        public void SaveShipperChanges(ShipperDTO dto)
        {
            Shipper p = FindShipperById(dto.ShipperId);

            p.PIB = dto.Pib;
            p.CompanyName = dto.CompanyName;
            p.Address = dto.Address;
            p.Email = dto.Email;
            p.Telephone = dto.Telephone;
            p.State = dto.State;
            p.ZipCode = dto.ZipCode;
            p.City = dto.City;
            contextClass.SaveChanges();
        }
        #endregion

        #region CLIENT
        public void SaveClientChanges(ClientDTO dto)
        {
            Client client = FindClientById(dto.ClientId);

            client.PIB = dto.PIB;
            client.NameOfClientCompany = dto.NameOfClientCompany;
            client.Address = dto.Address;
            client.Email = dto.Email;
            client.Telephone = dto.Telephone;
            client.State = dto.State;
            client.ZipCode = dto.ZipCode;
            client.City = dto.City;
            contextClass.SaveChanges();
        }

        public Client FindClientById(int id)
        {
            var client = contextClass.Client.Where(kl => kl.ClientId == id).FirstOrDefault();
            return client;
        }

        public List<ClientDTO> AllClients()
        {
            List<Client> clients;
            clients = contextClass.Client.ToList();

            List<ClientDTO> clientsDTO = new List<ClientDTO>(clients.Count());
            foreach (var client in clients)
            {
                ClientDTO dto = new ClientDTO();
                dto.ClientId = client.ClientId;
                dto.PIB = client.PIB;
                dto.NameOfClientCompany = client.NameOfClientCompany;
                dto.Address = client.Address;
                dto.Email = client.Email;
                dto.Telephone = client.Telephone;
                dto.State = client.State;
                dto.ZipCode = client.ZipCode;
                dto.City = client.City;
                clientsDTO.Add(dto);
            }
            return clientsDTO;
        }

        public void AddClient(ClientDTO dto)
        {
            Client client = new Client();
            client.PIB = dto.PIB;
            client.NameOfClientCompany = dto.NameOfClientCompany;
            client.Address = dto.Address;
            client.Email = dto.Email;
            client.Telephone = dto.Telephone;
            client.State = dto.State;
            client.ZipCode = dto.ZipCode;
            client.City = dto.City;
            contextClass.Client.Add(client);
            contextClass.SaveChanges();
        }

        public List<Client> FilterClientByName(string companyName)
        {
            var clients = contextClass.Client.Where(k => k.NameOfClientCompany.StartsWith(companyName) || companyName == null).ToList();
            return clients;
        }

        public List<Client> FilterClientByState(string state)
        {
            var clients = contextClass.Client.Where(k => k.State.StartsWith(state) || state == null).ToList();
            return clients;
        }

        public List<ClientDTO> GetClientsByNameAndCountry(string name, string country)
        {
            List<Client> clients = contextClass.Client.Where(k => k.NameOfClientCompany.StartsWith(name) && k.State.StartsWith(country)).ToList();
            List<ClientDTO> clientsDTO = new List<ClientDTO>();
            clients.ForEach(x => clientsDTO.Add(new ClientDTO(x.ClientId, x.PIB, x.NameOfClientCompany, x.Address, x.Email, x.Telephone, x.State, x.ZipCode, x.City)));
            return clientsDTO;
        }

        public void DeleteClient(int id)
        {
            Client client = contextClass.Client.Where(kl => kl.ClientId == id).FirstOrDefault();
            contextClass.Client.Remove(client);
            contextClass.SaveChanges();
        }
        #endregion

        #region COSTS
        public List<CostsDTO> AllCosts()
        {
            List<Cost> costs;
            costs = contextClass.Cost.ToList();

            List<CostsDTO> costsDTO = new List<CostsDTO>(costs.Count());
            foreach (var cost in costs)
            {
                CostsDTO dto = new CostsDTO();
                dto.CostId = cost.CostId;
                dto.AccountNumber = cost.AccountNumber;
                dto.DateOfReceipt = cost.DateOfReceipt;
                dto.PaymentDate = cost.PaymentDate;
                dto.Amount = cost.Amount;
                dto.Paid = cost.Paid;
                costsDTO.Add(dto);
            }
            return costsDTO;
        }

        public void SaveCost(CostsDTO dto)
        {
            Cost cost = new Cost();
            cost.AccountNumber = dto.AccountNumber;
            cost.DateOfReceipt = dto.DateOfReceipt;
            cost.PaymentDate = dto.PaymentDate;
            cost.Amount = dto.Amount;
            cost.Paid = dto.Paid;

            contextClass.Cost.Add(cost);
            contextClass.SaveChanges();
        }

        public List<CostsDTO> FilterCostsByName(string name)
        {
            List<Cost> lista = contextClass.Cost.Where(t => t.AccountNumber.StartsWith(name) || name == null).ToList();
            List<CostsDTO> costsDTO = new List<CostsDTO>();
            lista.ForEach(x => costsDTO.Add(new CostsDTO(x.CostId, x.AccountNumber, x.DateOfReceipt, x.PaymentDate, x.Amount, x.Paid)));
            return costsDTO;
        }

        public Cost FindCostById(int id)
        {
            var cost = contextClass.Cost.Where(tr => tr.CostId == id).FirstOrDefault();
            return cost;
        }

        public void DeleteCost(int id)
        {
            Cost cost = contextClass.Cost.Where(tr => tr.CostId == id).FirstOrDefault();
            contextClass.Cost.Remove(cost);
            contextClass.SaveChanges();
        }

        public void SaveCostChanges(CostsDTO costs)
        {
            Cost cost = FindCostById(costs.CostId);
            cost.AccountNumber = costs.AccountNumber;
            cost.DateOfReceipt = costs.DateOfReceipt;
            cost.PaymentDate = costs.PaymentDate;
            cost.Amount = costs.Amount;
            cost.Paid = costs.Paid;
            contextClass.SaveChanges();
        }

        public List<CostsDTO> GetCostByDate(DateTime? date1, DateTime? date2)
        {
            List<Cost> costs = contextClass.Cost.Where(c => c.DateOfReceipt >= date1 && c.DateOfReceipt <= date2).ToList();
            List<CostsDTO> costsDTO = new List<CostsDTO>();

            costs.ForEach(x => costsDTO.Add(new CostsDTO(x.CostId, x.AccountNumber, x.DateOfReceipt, x.PaymentDate, x.Amount, x.Paid)));

            return costsDTO;
        }

        public List<CostsDTO> GetCostsByDateAndName(DateTime? date1, DateTime? date2, string account)
        {
            List<Cost> costs = contextClass.Cost.Where(c => c.DateOfReceipt >= date1 && c.DateOfReceipt <= date2 && c.AccountNumber.StartsWith(account)).ToList();
            List<CostsDTO> costsDTO = new List<CostsDTO>();
            costs.ForEach(x => costsDTO.Add(new CostsDTO(x.CostId, x.AccountNumber, x.DateOfReceipt, x.PaymentDate, x.Amount, x.Paid)));
            return costsDTO;
        }
        #endregion

        #region SHIPPER OFFER
        public List<ShipperOfferDTO> ShipperOffers()
        {
            List<ShipperOffer> offers;
            offers = contextClass.ShipperOffer.Select(row => row)
                .Include(row => row.ClientOffer)
                .Include(row => row.Shipper)
                .ToList();

            List<ShipperOfferDTO> offersDTO = new List<ShipperOfferDTO>(offers.Count());
            foreach (var offer in offers)
            {
                ShipperOfferDTO dto = new ShipperOfferDTO();
                dto.ShipperOfferId = offer.ShipperOfferId;
                dto.ShipperOfferName = offer.ShipperOfferName;
                dto.Date = offer.Date;
                dto.Description = offer.Description;
                dto.Price = offer.Price;
                dto.Email = offer.Email;
                dto.Telephone = offer.Telephone;
                dto.Distance = offer.Distance;
                dto.ClientOfferName = offer.ClientOffer.OffersName;
                dto.ShipperName = offer.Shipper.CompanyName;
                offersDTO.Add(dto);
            }
            return offersDTO;
        }

        public void SaveShipperOffer(ShipperOfferDTO dto)
        {

            ShipperOffer offer = new ShipperOffer();

            offer.ShipperOfferName = dto.ShipperOfferName;
            offer.Date = dto.Date;
            offer.Description = dto.Description;
            offer.Price = dto.Price;
            offer.Email = dto.Email;
            offer.Telephone = dto.Telephone;
            offer.Distance = dto.Distance;
            offer.ClientOffId = dto.ClientOfferId;
            offer.ShipperId = dto.ShipperId;

            contextClass.ShipperOffer.Add(offer);
            contextClass.SaveChanges();
        }

        public void DeleteOffer(int id)
        {
            ShipperOffer offer = contextClass.ShipperOffer.Where(p => p.ShipperOfferId == id).FirstOrDefault();
            contextClass.ShipperOffer.Remove(offer);
            contextClass.SaveChanges();
        }

      

        public ShipperOffer GetShipperOfferByShipperId(int id)
        {
            var offer = contextClass.ShipperOffer.Where(p => p.ShipperId == id).FirstOrDefault();
            return offer;
        }

        public ShipperOffer FindShipperOfferById(int id)
        {
            var shipper = contextClass.ShipperOffer.Where(p => p.ShipperOfferId == id).FirstOrDefault();
            return shipper;
        }

        public void SaveChangesOfShipperOffer(ShipperOfferDTO offer)
        {
            ShipperOffer pp = FindShipperOfferById(offer.ShipperOfferId);
            pp.ShipperOfferName = offer.ShipperOfferName;
            pp.Description = offer.Description;
            pp.Price = offer.Price;
            pp.Date = offer.Date;
            pp.Email = offer.Email;
            pp.Telephone = offer.Telephone;
            pp.Distance = offer.Distance;
            pp.ClientOffId = offer.ClientOfferId;
            pp.ShipperId = offer.ShipperId;

            contextClass.SaveChanges();

        }

        public List<ShipperOfferDTO> GetPartnerOffersByName(string name)
        {
            List<ShipperOffer> list = contextClass.ShipperOffer.Select(row => row)
                .Include(row => row.Shipper)
                .ToList();
            List<ShipperOffer> offer = list.Where(p => p.ShipperOfferName.StartsWith(name)).ToList();
            List<ShipperOfferDTO> offersDTO = new List<ShipperOfferDTO>();
            offer.ForEach(p => offersDTO.Add(new ShipperOfferDTO(p.ShipperOfferId, p.ShipperOfferName, p.Date, p.Description, p.Price, p.Email, p.Telephone, p.Distance, p.ClientOffer.OffersName, p.Shipper.CompanyName)));
            return offersDTO;
        }

        public List<ShipperOfferDTO> GetShipperOffersByShipper(string shipper)
        {
            List<ShipperOffer> list = contextClass.ShipperOffer.Select(row => row)
                .Include(row => row.Shipper)
                .ToList();
            List<ShipperOffer> offer = list.Where(p => p.Shipper.CompanyName.StartsWith(shipper)).ToList();
            List<ShipperOfferDTO> offerDTO = new List<ShipperOfferDTO>();
            offer.ForEach(p => offerDTO.Add(new ShipperOfferDTO(p.ShipperOfferId, p.ShipperOfferName, p.Date, p.Description, p.Price, p.Email, p.Telephone, p.Distance, p.ClientOffer.OffersName, p.Shipper.CompanyName)));
            return offerDTO;
        }

        public List<ShipperOfferDTO> GetShipperOffersByNameAndShipper(string shipper, string name)
        {
            List<ShipperOffer> offers = contextClass.ShipperOffer.Select(row => row)
                .Include(row => row.Shipper)
                .ToList();
            List<ShipperOffer> offersList = offers.Where(p => p.ShipperOfferName.StartsWith(name) && p.Shipper.CompanyName.StartsWith(shipper)).ToList();
            List<ShipperOfferDTO> offersDTO = new List<ShipperOfferDTO>();
            offersList.ForEach(p => offersDTO.Add(new ShipperOfferDTO(p.ShipperOfferId, p.ShipperOfferName, p.Date, p.Description, p.Price, p.Email, p.Telephone, p.Distance, p.ClientOffer.OffersName, p.Shipper.CompanyName)));
            return offersDTO;
        }
        #endregion

        #region INPUT INVOICE
        public List<InputInvoiceDTO> GetInvoiceByBookId(int id)
        {
            var book = BookById(id);
            List<InputInvoice> inputInvoice = contextClass.InputInvoice.Where(p => p.BookId == book.BookId)
                .Include(p => p.Shipper)
                .Include(p => p.Book)
                .ToList();

            List<InputInvoiceDTO> inputInvoiceDTO = new List<InputInvoiceDTO>(inputInvoice.Count);
            foreach (var inv in inputInvoice)
            {
                InputInvoiceDTO dto = new InputInvoiceDTO();
                dto.DateOfReceiptInvoice = inv.DateOfReceiptInvoice;
                dto.ShipperName = inv.Shipper.CompanyName;
                dto.ShipperPlace = inv.Shipper.State;
                dto.NumberOfInputInvoice = inv.NumberOfInputInvoice;
                dto.AmountInDIN = inv.AmountInDIN;
                dto.AmountInEUR = inv.AmountInEUR;
                dto.ValueDate = inv.ValueDate;
                dto.PaymentDate = inv.PaymentDate;
                dto.Paid = inv.Paid;
                dto.Comment = inv.Comment;
                dto.ObjectBook = inv.Book.BookObject;
                inputInvoiceDTO.Add(dto);
            }
            return inputInvoiceDTO;
        }

        public List<InputInvoiceDTO> InputInvoice()
        {
            List<InputInvoice> list;
            list = contextClass.InputInvoice.Select(row => row)
                .Include(row => row.Shipper)
                .Include(row => row.Book)
                .ToList();

            List<InputInvoiceDTO> inputInvoicesDTO = new List<InputInvoiceDTO>(list.Count());
            foreach (var invoice in list)
            {
                InputInvoiceDTO dto = new InputInvoiceDTO();
                dto.SerialNumberOfInputInvoice = invoice.SerialNumberOfInputInvoice;
                dto.DateOfReceiptInvoice = invoice.DateOfReceiptInvoice;
                dto.ShipperName = invoice.Shipper.CompanyName;
                dto.ShipperPlace = invoice.Shipper.State;
                dto.NumberOfInputInvoice = invoice.NumberOfInputInvoice;
                dto.AmountInDIN = invoice.AmountInDIN;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                inputInvoicesDTO.Add(dto);
            }
            return inputInvoicesDTO;
        }

        public List<InputInvoiceDTO> GetInInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            List<InputInvoice> list;
            list = contextClass.InputInvoice.Select(row => row)
                .Include(row => row.Shipper)
                .Include(row => row.Book)
                .ToList();
            List<InputInvoice> invoices = list.Where(c => c.DateOfReceiptInvoice >= dat1 && c.DateOfReceiptInvoice <= dat2).ToList();
            List<InputInvoiceDTO> inputInvoicesDTO = new List<InputInvoiceDTO>(invoices.Count);
            foreach (var invoice in invoices)
            {
                InputInvoiceDTO dto = new InputInvoiceDTO();
                dto.SerialNumberOfInputInvoice = invoice.SerialNumberOfInputInvoice;
                dto.DateOfReceiptInvoice = invoice.DateOfReceiptInvoice;
                dto.ShipperName = invoice.Shipper.CompanyName;
                dto.ShipperPlace = invoice.Shipper.State;
                dto.NumberOfInputInvoice = invoice.NumberOfInputInvoice;
                dto.AmountInDIN = invoice.AmountInDIN;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                inputInvoicesDTO.Add(dto);
            }
            return inputInvoicesDTO;
        }

        public List<InputInvoiceDTO> GetInInvoicesByName(string name)
        {
            List<InputInvoice> list;
            list = contextClass.InputInvoice.Select(row => row)
                .Include(row => row.Shipper)
                .Include(row => row.Book)
                .ToList();

            List<InputInvoice> invoices = list.Where(p => p.NumberOfInputInvoice.StartsWith(name)).ToList();
            List<InputInvoiceDTO> inputInvoicesDTO = new List<InputInvoiceDTO>(invoices.Count);
            foreach (var invoice in invoices)
            {
                InputInvoiceDTO dto = new InputInvoiceDTO();
                dto.SerialNumberOfInputInvoice = invoice.SerialNumberOfInputInvoice;
                dto.DateOfReceiptInvoice = invoice.DateOfReceiptInvoice;
                dto.ShipperName = invoice.Shipper.CompanyName;
                dto.ShipperPlace = invoice.Shipper.State;
                dto.NumberOfInputInvoice = invoice.NumberOfInputInvoice;
                dto.AmountInDIN = invoice.AmountInDIN;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;
                inputInvoicesDTO.Add(dto);
            }
            return inputInvoicesDTO;
        }

        public List<InputInvoiceDTO> GetInInvoicesBySubject(string subject)
        {
            List<InputInvoice> lista;
            lista = contextClass.InputInvoice.Select(row => row)
                .Include(row => row.Shipper)
                .Include(row => row.Book)
                .ToList();
            List<InputInvoice> invoices = lista.Where(p => p.Book.BookObject.StartsWith(subject)).ToList();
            List<InputInvoiceDTO> inputInvoicesDTO = new List<InputInvoiceDTO>(invoices.Count);
            foreach (var invoice in invoices)
            {
                InputInvoiceDTO dto = new InputInvoiceDTO();
                dto.SerialNumberOfInputInvoice = invoice.SerialNumberOfInputInvoice;
                dto.DateOfReceiptInvoice = invoice.DateOfReceiptInvoice;
                dto.ShipperName = invoice.Shipper.CompanyName;
                dto.ShipperPlace = invoice.Shipper.State;
                dto.NumberOfInputInvoice = invoice.NumberOfInputInvoice;
                dto.AmountInDIN = invoice.AmountInDIN;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;
                inputInvoicesDTO.Add(dto);
            }
            return inputInvoicesDTO;
        }

        public void DeleteInInvoice(int id)
        {
            InputInvoice invoice = contextClass.InputInvoice.Where(f => f.SerialNumberOfInputInvoice == id).FirstOrDefault();
            contextClass.InputInvoice.Remove(invoice);
            contextClass.SaveChanges();
        }


        public InputInvoice FindInputInvoiceById(int id)
        {
            var invoice = contextClass.InputInvoice.Where(u => u.SerialNumberOfInputInvoice == id).FirstOrDefault();
            return invoice;
        }

        public void SaveInInvoice(InputInvoiceDTO dto)
        {
            InputInvoice invoice = new InputInvoice();
            invoice.DateOfReceiptInvoice = dto.DateOfReceiptInvoice;
            invoice.ShipperId = dto.ShipperId;
            invoice.NumberOfInputInvoice = dto.NumberOfInputInvoice;
            invoice.AmountInDIN = dto.AmountInDIN;
            invoice.AmountInEUR = dto.AmountInEUR;
            invoice.ValueDate = dto.ValueDate;
            invoice.PaymentDate = dto.PaymentDate;
            invoice.Paid = dto.Paid;
            invoice.Comment = dto.Comment;
            invoice.BookId = dto.BookId;

            contextClass.InputInvoice.Add(invoice);
            contextClass.SaveChanges();
        }

        public void SaveInInvoiceChanges(InputInvoiceDTO dto)
        {
            InputInvoice faktura = FindInputInvoiceById(dto.SerialNumberOfInputInvoice);
            faktura.SerialNumberOfInputInvoice = dto.SerialNumberOfInputInvoice;
            faktura.DateOfReceiptInvoice = dto.DateOfReceiptInvoice;
            faktura.ShipperId = dto.ShipperId;
            faktura.NumberOfInputInvoice = dto.NumberOfInputInvoice;
            faktura.AmountInDIN = dto.AmountInDIN;
            faktura.AmountInEUR = dto.AmountInEUR;
            faktura.ValueDate = dto.ValueDate;
            faktura.PaymentDate = dto.PaymentDate;
            faktura.Paid = dto.Paid;
            faktura.Comment = dto.Comment;
            faktura.BookId = dto.BookId;

            contextClass.SaveChanges();

        }


        #endregion

        #region FOREIGN INPUT INVOICE
        public bool GetNameOfForeignInputInvoice(string inputinvoice)
        {
            var invoice = contextClass.ForeignInputInvoice.Where(v => v.NumberOfForeignInputInvoice == inputinvoice).Select(v => v.NumberOfForeignInputInvoice).FirstOrDefault();

            if (invoice == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<ForeignInputInvoiceDTO> GetInoInvoiceByBookId(int id)
        {
            var book = BookById(id);
            List<ForeignInputInvoice> inputInvoice = contextClass.ForeignInputInvoice.Where(p => p.BookId == book.BookId)
                .Include(p => p.Shipper)
                .Include(p => p.Book)
                .ToList();

            List<ForeignInputInvoiceDTO> inputInvoicDTO = new List<ForeignInputInvoiceDTO>(inputInvoice.Count);
            foreach (var inv in inputInvoice)
            {
                ForeignInputInvoiceDTO dto = new ForeignInputInvoiceDTO();
                dto.DateOfReceiptInvoice = inv.DateOfReceiptInvoice;
                dto.ShipperName = inv.Shipper.CompanyName;
                dto.ShipperPlace = inv.Shipper.State;
                dto.NumberOfForeignInputInvoice = inv.NumberOfForeignInputInvoice;
                dto.AmountInEUR = inv.AmountInEUR;
                dto.ValueDate = inv.ValueDate;
                dto.PaymentDate = inv.PaymentDate;
                dto.Paid = inv.Paid;
                dto.Comment = inv.Comment;
                dto.ObjectBook = inv.Book.BookObject;
                inputInvoicDTO.Add(dto);
            }
            return inputInvoicDTO;
        }

        public List<ForeignInputInvoiceDTO> GetAllInoInInvoices()
        {
            List<ForeignInputInvoice> list;
            list = contextClass.ForeignInputInvoice.Select(row => row)
                .Include(row => row.Shipper)
                .Include(row => row.Book)
                .ToList();

            List<ForeignInputInvoiceDTO> inputInvoices = new List<ForeignInputInvoiceDTO>(list.Count());
            foreach (var invoice in list)
            {
                ForeignInputInvoiceDTO dto = new ForeignInputInvoiceDTO();
                dto.SerialNumberOfForeignInputInvoice = invoice.SerialNumberOfForeignInputInvoice;
                dto.DateOfReceiptInvoice = invoice.DateOfReceiptInvoice;
                dto.ShipperName = invoice.Shipper.CompanyName;
                dto.ShipperPlace = invoice.Shipper.State;
                dto.NumberOfForeignInputInvoice = invoice.NumberOfForeignInputInvoice;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                inputInvoices.Add(dto);
            }
            return inputInvoices;
        }

        public List<ForeignInputInvoiceDTO> GetInInoInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            List<ForeignInputInvoice> list;
            list = contextClass.ForeignInputInvoice.Select(row => row)
                .Include(row => row.Shipper)
                .Include(row => row.Book)
                .ToList();
            List<ForeignInputInvoice> invoices = list.Where(c => c.DateOfReceiptInvoice >= dat1 && c.DateOfReceiptInvoice <= dat2).ToList();
            List<ForeignInputInvoiceDTO> inputInvoices = new List<ForeignInputInvoiceDTO>(invoices.Count);
            foreach (var invoice in invoices)
            {
                ForeignInputInvoiceDTO dto = new ForeignInputInvoiceDTO();
                dto.SerialNumberOfForeignInputInvoice = invoice.SerialNumberOfForeignInputInvoice;
                dto.DateOfReceiptInvoice = invoice.DateOfReceiptInvoice;
                dto.ShipperName = invoice.Shipper.CompanyName;
                dto.ShipperPlace = invoice.Shipper.State;
                dto.NumberOfForeignInputInvoice = invoice.NumberOfForeignInputInvoice;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                inputInvoices.Add(dto);
            }
            return inputInvoices;
        }

        public List<ForeignInputInvoiceDTO> GetInInoInvoicesByName(string name)
        {
            List<ForeignInputInvoice> list;
            list = contextClass.ForeignInputInvoice.Select(row => row)
                .Include(row => row.Shipper)
                .Include(row => row.Book)
                .ToList();

            List<ForeignInputInvoice> invoices = list.Where(p => p.NumberOfForeignInputInvoice.StartsWith(name)).ToList();
            List<ForeignInputInvoiceDTO> inputInvoices = new List<ForeignInputInvoiceDTO>(invoices.Count);
            foreach (var invoice in invoices)
            {
                ForeignInputInvoiceDTO dto = new ForeignInputInvoiceDTO();
                dto.SerialNumberOfForeignInputInvoice = invoice.SerialNumberOfForeignInputInvoice;
                dto.DateOfReceiptInvoice = invoice.DateOfReceiptInvoice;
                dto.ShipperName = invoice.Shipper.CompanyName;
                dto.ShipperPlace = invoice.Shipper.State;
                dto.NumberOfForeignInputInvoice = invoice.NumberOfForeignInputInvoice;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                inputInvoices.Add(dto);
            }
            return inputInvoices;
        }

        public List<ForeignInputInvoiceDTO> GetInInoInvoicesBySubject(string subject)
        {
            List<ForeignInputInvoice> list;
            list = contextClass.ForeignInputInvoice.Select(row => row)
                .Include(row => row.Shipper)
                .Include(row => row.Book)
                .ToList();
            List<ForeignInputInvoice> invoices = list.Where(p => p.Book.BookObject.StartsWith(subject)).ToList();
            List<ForeignInputInvoiceDTO> inputInvoices = new List<ForeignInputInvoiceDTO>(invoices.Count);
            foreach (var invoice in invoices)
            {
                ForeignInputInvoiceDTO dto = new ForeignInputInvoiceDTO();
                dto.SerialNumberOfForeignInputInvoice = invoice.SerialNumberOfForeignInputInvoice;
                dto.DateOfReceiptInvoice = invoice.DateOfReceiptInvoice;
                dto.ShipperName = invoice.Shipper.CompanyName;
                dto.ShipperPlace = invoice.Shipper.State;
                dto.NumberOfForeignInputInvoice = invoice.NumberOfForeignInputInvoice;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                inputInvoices.Add(dto);
            }
            return inputInvoices;
        }


        public void DeleteInoInInvoice(int id)
        {
            ForeignInputInvoice invoice = contextClass.ForeignInputInvoice.Where(f => f.SerialNumberOfForeignInputInvoice == id).FirstOrDefault();
            contextClass.ForeignInputInvoice.Remove(invoice);
            contextClass.SaveChanges();
        }

        public ForeignInputInvoice FindInoInInvoiceById(int id)
        {
            var invoice = contextClass.ForeignInputInvoice.Where(u => u.SerialNumberOfForeignInputInvoice == id).FirstOrDefault();
            return invoice;
        }

        public void SaveInoInInvoice(ForeignInputInvoiceDTO dto)
        {
            ForeignInputInvoice invoice = new ForeignInputInvoice();
            invoice.DateOfReceiptInvoice = dto.DateOfReceiptInvoice;
            invoice.ShipperId = dto.ShipperId;
            invoice.NumberOfForeignInputInvoice = dto.NumberOfForeignInputInvoice;
            invoice.AmountInEUR = dto.AmountInEUR;
            invoice.ValueDate = dto.ValueDate;
            invoice.PaymentDate = dto.PaymentDate;
            invoice.Paid = dto.Paid;
            invoice.Comment = dto.Comment;
            invoice.BookId = dto.BookId;

            contextClass.ForeignInputInvoice.Add(invoice);
            contextClass.SaveChanges();
        }

        public void SaveInoInInvoiceChanges(ForeignInputInvoiceDTO dto)
        {
            ForeignInputInvoice invoice = FindInoInInvoiceById(dto.SerialNumberOfForeignInputInvoice);
            invoice.SerialNumberOfForeignInputInvoice = dto.SerialNumberOfForeignInputInvoice;
            invoice.DateOfReceiptInvoice = dto.DateOfReceiptInvoice;
            invoice.ShipperId = dto.ShipperId;
            invoice.NumberOfForeignInputInvoice = dto.NumberOfForeignInputInvoice;
            invoice.AmountInEUR = dto.AmountInEUR;
            invoice.ValueDate = dto.ValueDate;
            invoice.PaymentDate = dto.PaymentDate;
            invoice.Paid = dto.Paid;
            invoice.Comment = dto.Comment;
            invoice.BookId = dto.BookId;


            contextClass.SaveChanges();

        }
        #endregion

        #region OUTGOING INVOICE
        public bool GetNameOfOutgoingInvoice(string outgointinvoice)
        {
            var invoice = contextClass.OutgoingInvoice.Where(v => v.NumberOfOutgoingInvoice == outgointinvoice).Select(v => v.NumberOfOutgoingInvoice).FirstOrDefault();

            if (invoice == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<OutgoingInvoiceDTO> GetOutInvoicesByBookId(int id)
        {
            var book = BookById(id);
            List<OutgoingInvoice> outgoingInvoices = contextClass.OutgoingInvoice.Where(p => p.BookId == book.BookId)
                .Include(p => p.Client)
                .Include(p => p.Book).ToList();
            List<OutgoingInvoiceDTO> outgoingDTO = new List<OutgoingInvoiceDTO>(outgoingInvoices.Count);
            foreach (var invoice in outgoingInvoices)
            {
                OutgoingInvoiceDTO dto = new OutgoingInvoiceDTO();
                dto.DateOfReceipt = invoice.DateOfReceipt;
                dto.ClientName = invoice.Client.NameOfClientCompany;
                dto.ClientPlace = invoice.Client.State;
                dto.NumberOfOutgoingInvoice = invoice.NumberOfOutgoingInvoice;
                dto.AmountDIN = invoice.AmountInDIN;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                outgoingDTO.Add(dto);
            }
            return outgoingDTO;
        }

        public List<OutgoingInvoiceDTO> OutgoingInvoices()
        {
            List<OutgoingInvoice> list;
            list = contextClass.OutgoingInvoice.Select(row => row)
                .Include(row => row.Book)
                .Include(row => row.Client)
                .ToList();

            List<OutgoingInvoiceDTO> invoicesDTO = new List<OutgoingInvoiceDTO>(list.Count());
            foreach (var invoice in list)
            {
                OutgoingInvoiceDTO dto = new OutgoingInvoiceDTO();
                dto.OutgoingInvoiceId = invoice.OutgoingInvoiceId;
                dto.DateOfReceipt = invoice.DateOfReceipt;
                dto.ClientName = invoice.Client.NameOfClientCompany;
                dto.ClientPlace = invoice.Client.State;
                dto.NumberOfOutgoingInvoice = invoice.NumberOfOutgoingInvoice;
                dto.AmountDIN = invoice.AmountInDIN;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                invoicesDTO.Add(dto);
            }
            return invoicesDTO;
        }

        public void SaveInvoice(OutgoingInvoiceDTO dto)
        {
            OutgoingInvoice invoice = new OutgoingInvoice();
            invoice.DateOfReceipt = dto.DateOfReceipt;
            invoice.ClientId = dto.ClientId;
            invoice.NumberOfOutgoingInvoice = dto.NumberOfOutgoingInvoice;
            invoice.AmountInDIN = dto.AmountDIN;
            invoice.AmountInEUR = dto.AmountInEUR;
            invoice.ValueDate = dto.ValueDate;
            invoice.PaymentDate = dto.PaymentDate;
            invoice.Paid = dto.Paid;
            invoice.Comment = dto.Comment;
            invoice.BookId = dto.BookId;

            contextClass.OutgoingInvoice.Add(invoice);
            contextClass.SaveChanges();

        }

        public List<OutgoingInvoiceDTO> GetOutInvoiceByName(string name)
        {
            List<OutgoingInvoice> list = contextClass.OutgoingInvoice.Select(row => row)
                .Include(row => row.Book)
                .Include(row => row.Client)
                .ToList();
            List<OutgoingInvoice> invoices = list.Where(p => p.NumberOfOutgoingInvoice.StartsWith(name)).ToList();
            List<OutgoingInvoiceDTO> invoicesDTO = new List<OutgoingInvoiceDTO>(invoices.Count());
            foreach (var invoice in list)
            {
                OutgoingInvoiceDTO dto = new OutgoingInvoiceDTO();
                dto.OutgoingInvoiceId = invoice.OutgoingInvoiceId;
                dto.DateOfReceipt = invoice.DateOfReceipt;
                dto.ClientName = invoice.Client.NameOfClientCompany;
                dto.ClientPlace = invoice.Client.State;
                dto.NumberOfOutgoingInvoice = invoice.NumberOfOutgoingInvoice;
                dto.AmountDIN = invoice.AmountInDIN;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                invoicesDTO.Add(dto);
            }
            return invoicesDTO;

        }
        

        public List<OutgoingInvoiceDTO> GetOutInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            List<OutgoingInvoice> list = contextClass.OutgoingInvoice.Select(row => row)
                .Include(row => row.Book)
                .Include(row => row.Client)
                .ToList();
            List<OutgoingInvoice> invoices = list.Where(c => c.DateOfReceipt >= dat1 && c.DateOfReceipt <= dat2).ToList();
            List<OutgoingInvoiceDTO> invoicesDTO = new List<OutgoingInvoiceDTO>(invoices.Count());
            foreach (var invoice in invoices)
            {
                OutgoingInvoiceDTO dto = new OutgoingInvoiceDTO();
                dto.OutgoingInvoiceId = invoice.OutgoingInvoiceId;
                dto.DateOfReceipt = invoice.DateOfReceipt;
                dto.ClientName = invoice.Client.NameOfClientCompany;
                dto.ClientPlace = invoice.Client.State;
                dto.NumberOfOutgoingInvoice = invoice.NumberOfOutgoingInvoice;
                dto.AmountDIN = invoice.AmountInDIN;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                invoicesDTO.Add(dto);
            }
            return invoicesDTO;
        }

        public OutgoingInvoice FindIInvoiceById(int id)
        {
            var invoice = contextClass.OutgoingInvoice.Where(i => i.OutgoingInvoiceId == id).FirstOrDefault();
            return invoice;
        }

        public void DeleteOutInvoice(int id)
        {
            OutgoingInvoice invoice = contextClass.OutgoingInvoice.Where(f => f.OutgoingInvoiceId == id).FirstOrDefault();
            contextClass.OutgoingInvoice.Remove(invoice);
            contextClass.SaveChanges();
        }

        public void SaveChangesOfInvoice(OutgoingInvoiceDTO dto)
        {
            OutgoingInvoice invoice = FindIInvoiceById(dto.OutgoingInvoiceId);
            invoice.OutgoingInvoiceId = dto.OutgoingInvoiceId;
            invoice.NumberOfOutgoingInvoice = dto.NumberOfOutgoingInvoice;
            invoice.DateOfReceipt = dto.DateOfReceipt;
            invoice.ClientId = dto.ClientId;
            invoice.AmountInDIN = dto.AmountDIN;
            invoice.AmountInEUR = dto.AmountInEUR;
            invoice.ValueDate = dto.ValueDate;
            invoice.PaymentDate = dto.PaymentDate;
            invoice.Paid = dto.Paid;
            invoice.Comment = dto.Comment;
            invoice.BookId = dto.BookId;

            contextClass.SaveChanges();
        }

        #endregion

        #region FOREIGN OUTGOING INVOICE
        public bool GetNameOfForeignOutgoingInvoice(string outgointinvoice)
        {
            var invoice = contextClass.ForeignOutgoingInvoice.Where(v => v.NumberOfForeignOutgoingInvoice == outgointinvoice).Select(v => v.NumberOfForeignOutgoingInvoice).FirstOrDefault();

            if (invoice == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<ForeignOutgoingInvoiceDTO> GetOutInoInvoicesByBookId(int id)
        {
            var book = BookById(id);
            List<ForeignOutgoingInvoice> invoices = contextClass.ForeignOutgoingInvoice.Where(p => p.BookId == book.BookId)
                .Include(p => p.Client)
                .Include(p => p.Book).ToList();
            List<ForeignOutgoingInvoiceDTO> invoicesDTO = new List<ForeignOutgoingInvoiceDTO>(invoices.Count);
            foreach (var invoice in invoices)
            {
                ForeignOutgoingInvoiceDTO dto = new ForeignOutgoingInvoiceDTO();
                dto.DateOfReceipt = invoice.DateOfReceipt;
                dto.ClientName = invoice.Client.NameOfClientCompany;
                dto.ClientPlace = invoice.Client.State;
                dto.NumberOfOutgoingInvoice = invoice.NumberOfForeignOutgoingInvoice;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                invoicesDTO.Add(dto);
            }
            return invoicesDTO;
        }

        public List<ForeignOutgoingInvoiceDTO> AllInoOutInvoices()
        {
            List<ForeignOutgoingInvoice> list;
            list = contextClass.ForeignOutgoingInvoice.Select(row => row)
                .Include(row => row.Book)
                .Include(row => row.Client)
                .ToList();

            List<ForeignOutgoingInvoiceDTO> invoicesDTO = new List<ForeignOutgoingInvoiceDTO>(list.Count());
            foreach (var invoice in list)
            {
                ForeignOutgoingInvoiceDTO dto = new ForeignOutgoingInvoiceDTO();
                dto.ForeignOutgoingInvoiceId = invoice.ForeignOutgoingInvoiceId;
                dto.DateOfReceipt = invoice.DateOfReceipt;
                dto.ClientName = invoice.Client.NameOfClientCompany;
                dto.ClientPlace = invoice.Client.State;
                dto.NumberOfOutgoingInvoice = invoice.NumberOfForeignOutgoingInvoice;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                invoicesDTO.Add(dto);
            }
            return invoicesDTO;
        }

        public void SaveInoInvoice(ForeignOutgoingInvoiceDTO dto)
        {
            ForeignOutgoingInvoice invoice = new ForeignOutgoingInvoice();
            invoice.DateOfReceipt = dto.DateOfReceipt;
            invoice.ClientId = dto.ClientId;
            invoice.NumberOfForeignOutgoingInvoice = dto.NumberOfOutgoingInvoice;
            invoice.AmountInEUR = dto.AmountInEUR;
            invoice.ValueDate = dto.ValueDate;
            invoice.PaymentDate = dto.PaymentDate;
            invoice.Paid = dto.Paid;
            invoice.Comment = dto.Comment;
            invoice.BookId = dto.BookId;

            contextClass.ForeignOutgoingInvoice.Add(invoice);
            contextClass.SaveChanges();

        }

        public List<ForeignOutgoingInvoiceDTO> GetOutInoInvoiceByName(string name)
        {
            List<ForeignOutgoingInvoice> list = contextClass.ForeignOutgoingInvoice.Select(row => row)
                .Include(row => row.Book)
                .Include(row => row.Client)
                .ToList();
            List<ForeignOutgoingInvoice> invoices = list.Where(p => p.NumberOfForeignOutgoingInvoice.StartsWith(name)).ToList();
            List<ForeignOutgoingInvoiceDTO> invoicesDTO = new List<ForeignOutgoingInvoiceDTO>(invoices.Count());
            foreach (var invoice in invoices)
            {
                ForeignOutgoingInvoiceDTO dto = new ForeignOutgoingInvoiceDTO();
                dto.ForeignOutgoingInvoiceId = invoice.ForeignOutgoingInvoiceId;
                dto.DateOfReceipt = invoice.DateOfReceipt;
                dto.ClientName = invoice.Client.NameOfClientCompany;
                dto.ClientPlace = invoice.Client.State;
                dto.NumberOfOutgoingInvoice = invoice.NumberOfForeignOutgoingInvoice;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                invoicesDTO.Add(dto);
            }
            return invoicesDTO;

        }

        public List<ForeignOutgoingInvoiceDTO> GetOutInoInvoicesByDate(DateTime? dat1, DateTime? dat2)
        {
            List<ForeignOutgoingInvoice> list = contextClass.ForeignOutgoingInvoice.Select(row => row)
                .Include(row => row.Book)
                .Include(row => row.Client)
                .ToList();
            List<ForeignOutgoingInvoice> invoices = list.Where(c => c.DateOfReceipt >= dat1 && c.DateOfReceipt <= dat2).ToList();
            List<ForeignOutgoingInvoiceDTO> invoicesDTO = new List<ForeignOutgoingInvoiceDTO>(invoices.Count());
            foreach (var invoice in invoices)
            {
                ForeignOutgoingInvoiceDTO dto = new ForeignOutgoingInvoiceDTO();
                dto.ForeignOutgoingInvoiceId = invoice.ForeignOutgoingInvoiceId;
                dto.DateOfReceipt = invoice.DateOfReceipt;
                dto.ClientName = invoice.Client.NameOfClientCompany;
                dto.ClientPlace = invoice.Client.State;
                dto.NumberOfOutgoingInvoice = invoice.NumberOfForeignOutgoingInvoice;
                dto.AmountInEUR = invoice.AmountInEUR;
                dto.ValueDate = invoice.ValueDate;
                dto.PaymentDate = invoice.PaymentDate;
                dto.Paid = invoice.Paid;
                dto.Comment = invoice.Comment;
                dto.ObjectBook = invoice.Book.BookObject;

                invoicesDTO.Add(dto);
            }
            return invoicesDTO;
        }

        public ForeignOutgoingInvoice FindInoOutInvoiceById(int id)
        {
            var invoice = contextClass.ForeignOutgoingInvoice.Where(i => i.ForeignOutgoingInvoiceId == id).FirstOrDefault();
            return invoice;
        }

        public void DeleteOutInoInvoice(int id)
        {
            ForeignOutgoingInvoice invoice = contextClass.ForeignOutgoingInvoice.Where(f => f.ForeignOutgoingInvoiceId == id).FirstOrDefault();
            contextClass.ForeignOutgoingInvoice.Remove(invoice);
            contextClass.SaveChanges();
        }

        public void SaveChangesOfOutInoInvoice(ForeignOutgoingInvoiceDTO dto)
        {
            ForeignOutgoingInvoice invoice = FindInoOutInvoiceById(dto.ForeignOutgoingInvoiceId);
            invoice.ForeignOutgoingInvoiceId = dto.ForeignOutgoingInvoiceId;
            invoice.NumberOfForeignOutgoingInvoice = dto.NumberOfOutgoingInvoice;
            invoice.DateOfReceipt = dto.DateOfReceipt;
            invoice.ClientId = dto.ClientId;
            invoice.AmountInEUR = dto.AmountInEUR;
            invoice.ValueDate = dto.ValueDate;
            invoice.PaymentDate = dto.PaymentDate;
            invoice.Paid = dto.Paid;
            invoice.Comment = dto.Comment;
            invoice.BookId = dto.BookId;

            contextClass.SaveChanges();
        }
        #endregion

        #region ADDITIONAL DOCUMENTATION
        public List<AdditionalDocumentationDTO> AdditionalDocumentationList()
        {
            List<AdditionalDocumentation> list;
            list = contextClass.AdditionalDocumentation.Select(row => row)
                .Include(row => row.Book)
                .ToList();

            List<AdditionalDocumentationDTO> allDocuments = new List<AdditionalDocumentationDTO>(list.Count());
            foreach (var el in list)
            {
                AdditionalDocumentationDTO dto = new AdditionalDocumentationDTO();
                dto.AdditionalDocumentationId = el.AdditionalDocumentationId;
                dto.Name = el.Name;
                dto.FileName = el.FileName;
                dto.ObjectBook = el.Book.BookObject;
                allDocuments.Add(dto);
            }
            return allDocuments;
        }

        public List<AdditionalDocumentationDTO> GetDocumentByName(string name)
        {
            List<AdditionalDocumentation> lista = contextClass.AdditionalDocumentation.Where(p => p.Name.StartsWith(name) || name == null).ToList();

            List<AdditionalDocumentationDTO> dokumenta = new List<AdditionalDocumentationDTO>();
            lista.ForEach(x => dokumenta.Add(new AdditionalDocumentationDTO(x.AdditionalDocumentationId, x.Name, x.Book.BookObject, x.Content)));
            return dokumenta;
        }

        public List<AdditionalDocumentationDTO> GetDocumentByNameAndSubject(string subject, string name)
        {
            List<AdditionalDocumentation> list = contextClass.AdditionalDocumentation.Select(row => row)
                .Include(row => row.Book)
                .ToList();
            List<AdditionalDocumentation> documents = list.Where(p => p.Book.BookObject == subject && p.Name.StartsWith(name)).ToList();
            List<AdditionalDocumentationDTO> dokumentsList = new List<AdditionalDocumentationDTO>();
            documents.ForEach(x => dokumentsList.Add(new AdditionalDocumentationDTO(x.AdditionalDocumentationId, x.Name, x.Book.BookObject, x.Content)));
            return dokumentsList;
        }


        public List<AdditionalDocumentationDTO> GetDocumentBySubject(string subject)
        {
            List<AdditionalDocumentation> list = contextClass.AdditionalDocumentation.Select(row => row)
                .Include(row => row.Book)
                .ToList();
            List<AdditionalDocumentation> documents = list.Where(p => p.Book.BookObject.StartsWith(subject) || subject == null).ToList();
            List<AdditionalDocumentationDTO> documentsList = new List<AdditionalDocumentationDTO>();
            documents.ForEach(x => documentsList.Add(new AdditionalDocumentationDTO(x.AdditionalDocumentationId, x.Name, x.Book.BookObject, x.Content)));
            return documentsList;
        }

        public void DeleteAttachments(int id)
        {
            AdditionalDocumentation document = contextClass.AdditionalDocumentation.Where(d => d.AdditionalDocumentationId == id).FirstOrDefault();
            contextClass.AdditionalDocumentation.Remove(document);
            contextClass.SaveChanges();
        }

        public void SaveDocument(AdditionalDocumentationDTO document, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var doc = new AdditionalDocumentation
                {
                    Name = document.Name,
                    FileName = System.IO.Path.GetFileName(upload.FileName),
                    ContentType = upload.ContentType,
                    BookId = document.BookId
                };

                using (var reader = new System.IO.BinaryReader(upload.InputStream))
                {
                    doc.Content = reader.ReadBytes(upload.ContentLength);
                }

                contextClass.AdditionalDocumentation.Add(doc);
                contextClass.SaveChanges();
            }

        }

        public void SaveAdditionalDocumentation(AdditionalDocumentationDTO document)
        {
            AdditionalDocumentation doc = new AdditionalDocumentation();
            doc.Name = document.Name;
            doc.BookId = document.BookId;

            contextClass.AdditionalDocumentation.Add(doc);
            contextClass.SaveChanges();
        }

        public AdditionalDocumentation GetDocumentById(int id)
        {
            var document = contextClass.AdditionalDocumentation.Where(d => d.AdditionalDocumentationId == id).FirstOrDefault();
            return document;
        }

        public void SaveDocumentChanges(AdditionalDocumentationDTO dto)
        {
            AdditionalDocumentation doc = GetDocumentById(dto.AdditionalDocumentationId);
            doc.Name = dto.Name;
            doc.FileName = dto.FileName;
            doc.BookId = dto.BookId;

            contextClass.SaveChanges();
        }

        public void SaveDetail(AdditionalDocumentationDTO model)
        {
            AdditionalDocumentation dok = GetDocumentById(model.AdditionalDocumentationId);
            dok.Name = model.Name;
            dok.FileName = model.FileName;
            dok.BookId = model.BookId;

            contextClass.SaveChanges();
        }
        #endregion

        #region CLIENT OFFER
        public List<ClientOffersDTO> ClientOffers()
        {
            List<ClientOffer> list;
            list = contextClass.ClientOffer.Select(row => row)
                .Include(row => row.Book)
                .Include(row => row.Client)
                .ToList();

            List<ClientOffersDTO> allOffers = new List<ClientOffersDTO>(list.Count());

            foreach (var po in list)
            {
                ClientOffersDTO dto = new ClientOffersDTO();
                dto.ClientOfferId = po.ClientOfferId;
                dto.OffersName = po.OffersName;
                dto.Description = po.Descritpion;
                dto.Date = po.Date;
                dto.ASA = po.ASA;
                dto.Accepted = po.Accepted;
                dto.ClientName = po.Client.NameOfClientCompany;
                dto.ObjectBook = po.Book.BookObject;
                allOffers.Add(dto);
            }
            return allOffers;
        }

        public void DeleteClientOffer(int id)
        {
            ClientOffer client = contextClass.ClientOffer.Where(k => k.ClientOfferId == id).FirstOrDefault();
            contextClass.ClientOffer.Remove(client);
            contextClass.SaveChanges();
        }

        public void SaveClientOffer(ClientOffersDTO dto)
        {
            ClientOffer offer = new ClientOffer();
            offer.OffersName = dto.OffersName;
            offer.Descritpion = dto.Description;
            offer.ASA = dto.ASA;
            offer.Accepted = dto.Accepted;
            offer.Date = dto.Date;
            offer.ClientId = dto.ClientId;
            offer.BookId = dto.BookId;


            contextClass.ClientOffer.Add(offer);
            contextClass.SaveChanges();
        }

        public ClientOffer FindClientOfferById(int id)
        {
            var offer = contextClass.ClientOffer.Where(p => p.ClientOfferId == id).FirstOrDefault();
            return offer;
        }

        public void SaveClientOfferChanges(ClientOffersDTO ponuda)
        {
            ClientOffer kp = FindClientOfferById(ponuda.ClientOfferId);
            //kp.ClientOfferId = ponuda.ClientOfferId;
            kp.OffersName = ponuda.OffersName;
            kp.Descritpion = ponuda.Description;
            kp.ASA = ponuda.ASA;
            kp.Accepted = ponuda.Accepted;
            kp.Date = ponuda.Date;
            kp.ClientId = ponuda.ClientId;
            kp.BookId = ponuda.BookId;
      

            contextClass.SaveChanges();
        }

        public List<ClientOffersDTO> GetClientOfferByName(string name)
        {
            List<ClientOffer> list = contextClass.ClientOffer.Select(row => row)
                .Include(row => row.Book)
                .Include(row => row.Client)
                .ToList();

            List<ClientOffer> offers = list.Where(p => p.OffersName.StartsWith(name)).ToList();
            List<ClientOffersDTO> listaDto = new List<ClientOffersDTO>(offers.Count);
            foreach (var po in offers)
            {
                ClientOffersDTO dto = new ClientOffersDTO();
                dto.OffersName = po.OffersName;
                dto.Description = po.Descritpion;
                dto.Date = po.Date;
                dto.ASA = po.ASA;
                dto.ClientName = po.Client.NameOfClientCompany;
                dto.ObjectBook = po.Book.BookObject;
                listaDto.Add(dto);
            }
            return listaDto;
        }

        public List<ClientOffersDTO> GetClientOfferByClient(string client)
        {
            List<ClientOffer> list = contextClass.ClientOffer.Select(row => row)
                .Include(row => row.Book)
                .Include(row => row.Client)
                .ToList();
            List<ClientOffer> offers = list.Where(p => p.Client.NameOfClientCompany.StartsWith(client)).ToList();

            List<ClientOffersDTO> offersDTO = new List<ClientOffersDTO>(offers.Count);
            foreach (var po in offers)
            {
                ClientOffersDTO dto = new ClientOffersDTO();
                dto.ClientOfferId = po.ClientOfferId;
                dto.OffersName = po.OffersName;
                dto.Description = po.Descritpion;
                dto.Date = po.Date;
                dto.ASA = po.ASA;
                dto.Accepted = po.Accepted;
               
                dto.ClientName = po.Client.NameOfClientCompany;
                dto.ObjectBook = po.Book.BookObject;
                offersDTO.Add(dto);
            }
            return offersDTO;
        }

        public List<ClientOffersDTO> FindClientOfferByNameAndClient(string name, string client)
        {
            List<ClientOffer> list = contextClass.ClientOffer.Select(row => row)
              
                .Include(row => row.Book)
                .Include(row => row.Client)
                .ToList();
            List<ClientOffer> offers = list.Where(p => p.OffersName.StartsWith(name) && p.Client.NameOfClientCompany.StartsWith(client)).ToList();
            List<ClientOffersDTO> offersDTO = new List<ClientOffersDTO>(offers.Count);
            foreach (var po in offers)
            {
                ClientOffersDTO dto = new ClientOffersDTO();
                dto.ClientOfferId = po.ClientOfferId;
                dto.OffersName = po.OffersName;
                dto.Description = po.Descritpion;
                dto.Date = po.Date;
                dto.ASA = po.ASA;
                dto.Accepted = po.Accepted;
               
                dto.ClientName = po.Client.NameOfClientCompany;
                dto.ObjectBook = po.Book.BookObject;
                offersDTO.Add(dto);
            }
            return offersDTO;
        }
        #endregion

        #region NOTIFICATIONS
        public List<InputInvoiceDTO> GetInvoicesByExpireDate()
        {
            var listOfInputInvoices = contextClass.InputInvoice.ToList();
            DateTime today = DateTime.Today;

            //Dictionary<int, DateTime> invoicesToPay = new Dictionary<int, DateTime>(listOfInputInvoices.Count);
            List<InputInvoiceDTO> invoices = new List<InputInvoiceDTO>();

            foreach (var i in listOfInputInvoices)
            {
                InputInvoiceDTO dto = new InputInvoiceDTO();
                if (DateTime.Compare(today, i.PaymentDate) >= -3 && i.Paid == false)
                {
                    var partner = contextClass.Shipper.Where(x => x.ShipperId == i.ShipperId).FirstOrDefault();
                    var knjiga = contextClass.Book.Where(x => x.BookId == i.BookId).FirstOrDefault();
                    dto.NumberOfInputInvoice = i.NumberOfInputInvoice;
                    dto.DateOfReceiptInvoice = i.DateOfReceiptInvoice;
                    dto.ShipperName = partner.CompanyName;
                    dto.ShipperPlace = partner.State;
                    dto.AmountInDIN = i.AmountInDIN;

                    dto.AmountInEUR = i.AmountInEUR;
                    dto.ValueDate = i.ValueDate;
                    dto.PaymentDate = i.PaymentDate;
                    dto.Paid = i.Paid;
                    dto.Comment = i.Comment;
                    dto.ObjectBook = knjiga.BookObject;

                    invoices.Add(dto);
                }
            }

            return invoices;
        }


        public List<ForeignInputInvoiceDTO> GetInoInvoicesByExpireDate()
        {
            var listOfInputInvoices = contextClass.ForeignInputInvoice.ToList();
            DateTime today = DateTime.Today;

            //Dictionary<int, DateTime> invoicesToPay = new Dictionary<int, DateTime>(listOfInputInvoices.Count);
            List<ForeignInputInvoiceDTO> invoices = new List<ForeignInputInvoiceDTO>();

            foreach (var i in listOfInputInvoices)
            {
                ForeignInputInvoiceDTO dto = new ForeignInputInvoiceDTO();
                if (DateTime.Compare(today, i.PaymentDate) >= -3 && i.Paid == false)
                {
                    var partner = contextClass.Shipper.Where(x => x.ShipperId == i.ShipperId).FirstOrDefault();
                    var knjiga = contextClass.Book.Where(x => x.BookId == i.BookId).FirstOrDefault();
                    dto.NumberOfForeignInputInvoice = i.NumberOfForeignInputInvoice;
                    dto.DateOfReceiptInvoice = i.DateOfReceiptInvoice;
                    dto.ValueDate = i.ValueDate;
                    dto.AmountInEUR = i.AmountInEUR;
                    dto.Comment = i.Comment;
                    dto.ShipperName = partner.CompanyName;
                    dto.ShipperPlace = partner.State;
                    dto.Paid = i.Paid;
                    dto.PaymentDate = i.PaymentDate;
                    dto.Comment = i.Comment;
                    dto.ObjectBook = knjiga.BookObject;


                    invoices.Add(dto);
                }
            }

            return invoices;
        }

        public List<OutgoingInvoiceDTO> GetOutgoingInvoicesByExpireDate()
        {
            var listOfOutgoingInvoices = contextClass.OutgoingInvoice.ToList();
            DateTime today = DateTime.Today;

            List<OutgoingInvoiceDTO> invoices = new List<OutgoingInvoiceDTO>();

            foreach(var i in listOfOutgoingInvoices)
            {
                OutgoingInvoiceDTO dto = new OutgoingInvoiceDTO();
                if(DateTime.Compare(today, i.PaymentDate) >=-3 && i.Paid == false)
                {
                    var client = contextClass.Client.Where(x => x.ClientId == i.ClientId).FirstOrDefault();
                    var book = contextClass.Book.Where(x => x.BookId == i.BookId).FirstOrDefault();
                    dto.NumberOfOutgoingInvoice = i.NumberOfOutgoingInvoice;
                    dto.DateOfReceipt = i.DateOfReceipt;
                    dto.ValueDate = i.ValueDate;
                    dto.AmountDIN = i.AmountInDIN;
                    dto.AmountInEUR = i.AmountInEUR;
                    dto.Comment = i.Comment;
                    dto.ClientName = client.NameOfClientCompany;
                    dto.ClientPlace = client.State;
                    dto.Paid = i.Paid;
                    dto.PaymentDate = i.PaymentDate;
                    dto.ObjectBook = book.BookObject;

                    invoices.Add(dto);
                }
            }
            return invoices;
        }

        public List<ForeignOutgoingInvoiceDTO> GetforeignOutgoingInvoicesByExpireDate()
        {
            var listOfOutgoingInvoices = contextClass.ForeignOutgoingInvoice.ToList();
            DateTime today = DateTime.Today;

            List<ForeignOutgoingInvoiceDTO> invoices = new List<ForeignOutgoingInvoiceDTO>();

            foreach (var i in listOfOutgoingInvoices)
            {
                ForeignOutgoingInvoiceDTO dto = new ForeignOutgoingInvoiceDTO();
                if (DateTime.Compare(today, i.PaymentDate) >= -3 && i.Paid == false)
                {
                    var client = contextClass.Client.Where(x => x.ClientId == i.ClientId).FirstOrDefault();
                    var book = contextClass.Book.Where(x => x.BookId == i.BookId).FirstOrDefault();
                    dto.NumberOfOutgoingInvoice = i.NumberOfForeignOutgoingInvoice;
                    dto.DateOfReceipt = i.DateOfReceipt;
                    dto.ValueDate = i.ValueDate;
                    dto.AmountInEUR = i.AmountInEUR;
                    dto.Comment = i.Comment;
                    dto.ClientName = client.NameOfClientCompany;
                    dto.ClientPlace = client.State;
                    dto.Paid = i.Paid;
                    dto.PaymentDate = i.PaymentDate;
                    dto.ObjectBook = book.BookObject;

                    invoices.Add(dto);
                }
            }
            return invoices;
        }
        #endregion

        public List<InputInvoiceDTO> GetEmailInvoicesByExpireDate()
        {
            var listOfInputInvoices = contextClass.InputInvoice.ToList();

            List<InputInvoiceDTO> invoices = new List<InputInvoiceDTO>(listOfInputInvoices.Count);

            DateTime today = DateTime.Today;

            foreach (var i in listOfInputInvoices)
            {
                //Compare(today, i.DatumPlacanja) >= -3 i napraviti da li je placeno!!!!! && 
                if (DateTime.Compare(today, i.PaymentDate) >= -3 && i.Paid == false)
                {
                    InputInvoiceDTO ulaznaFakturaDto = new InputInvoiceDTO();
                    ulaznaFakturaDto.NumberOfInputInvoice = i.NumberOfInputInvoice;
                    ulaznaFakturaDto.AmountInEUR = i.AmountInEUR;
                    ulaznaFakturaDto.AmountInDIN = i.AmountInDIN;

                    invoices.Add(ulaznaFakturaDto);
                }
            }

            return invoices;
        }

        public List<ForeignInputInvoiceDTO> GetEmailINOInvoicesByExpireDate()
        {
            var listOfInputInvoices = contextClass.ForeignInputInvoice.ToList();

            List<ForeignInputInvoiceDTO> invoicesIno = new List<ForeignInputInvoiceDTO>(listOfInputInvoices.Count);

            DateTime today = DateTime.Today;

            foreach (var i in listOfInputInvoices)
            {
                if (DateTime.Compare(today, i.PaymentDate) >= -3 && i.Paid == false)
                {
                    ForeignInputInvoiceDTO inoUlaznaFakturaDto = new ForeignInputInvoiceDTO();
                    inoUlaznaFakturaDto.NumberOfForeignInputInvoice = i.NumberOfForeignInputInvoice;
                    inoUlaznaFakturaDto.AmountInEUR = i.AmountInEUR;

                    invoicesIno.Add(inoUlaznaFakturaDto);
                }
            }

            return invoicesIno;
        }

        #region DISTANCE TRANSPORT
        public List<DistanceTransportDTO> GetDistanceTransportList()
        {
            List<DistanceTransport> list = contextClass.DistanceTransport.Select(row => row).Include(row => row.Shipper).ToList();

            List<DistanceTransportDTO> listDTO = new List<DistanceTransportDTO>(list.Count());
            foreach (var li in list)
            {
                DistanceTransportDTO dto = new DistanceTransportDTO();
                dto.DtId = li.DTId;
                dto.DistanceName = li.DistanceName;
                dto.TransportName = li.TransportName;
                dto.ShipperName = li.Shipper.CompanyName;

                listDTO.Add(dto);
            }
            return listDTO;
        }

        public void DeleteDistanceTransport(int id)
        {
            DistanceTransport distance = contextClass.DistanceTransport.Where(f => f.DTId == id).FirstOrDefault();
            contextClass.DistanceTransport.Remove(distance);
            contextClass.SaveChanges();
        }

        public void SaveDistanceTransport(DistanceTransportDTO dto)
        {
            DistanceTransport distance = new DistanceTransport();
            distance.DistanceName = dto.DistanceName;
            distance.TransportName = dto.TransportName;
            distance.ShipperId = dto.ShipperId;


            contextClass.DistanceTransport.Add(distance);
            contextClass.SaveChanges();
        }

        public DistanceTransport FindDistanceTransportById(int id)
        {
            var distance = contextClass.DistanceTransport.Where(u => u.DTId == id).FirstOrDefault();
            return distance;
        }

        public void SaveDistanceTransportChanges(DistanceTransportDTO dto)
        {
            DistanceTransport distance = FindDistanceTransportById(dto.DtId);
            distance.DTId = dto.DtId;
            distance.DistanceName = dto.DistanceName;
            distance.TransportName = dto.TransportName;
            distance.ShipperId = dto.ShipperId;

            contextClass.SaveChanges();
        }

        public List<DistanceTransportDTO> GetDistanceTransportByShipperId(int id)
        {
            var distance = FindShipperById(id);
            List<DistanceTransport> distances = contextClass.DistanceTransport.Where(p => p.ShipperId == distance.ShipperId)
                .Include(p => p.Shipper).ToList();

            List<DistanceTransportDTO> distanceTransportDTO = new List<DistanceTransportDTO>(distances.Count);
            foreach (var d in distances)
            {
                DistanceTransportDTO dto = new DistanceTransportDTO();
                dto.DtId = d.DTId;
                dto.DistanceName = d.DistanceName;
                dto.TransportName = d.TransportName;
                dto.ShipperName = d.Shipper.CompanyName;

                distanceTransportDTO.Add(dto);
            }
            return distanceTransportDTO;

        }
        #endregion
    }
}