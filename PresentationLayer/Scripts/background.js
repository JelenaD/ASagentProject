﻿const blur = document.querySelector(".blur");
const focus = document.querySelector(".focus");
// const focusWidth = window.getComputedStyle(focus, null).width;
// const focusWidth = focus.style.width;
const focusHalfWidth = focus.offsetWidth / 2;
const focusHalfHeight = focus.offsetHeight / 2;
blur.addEventListener("mousemove", event => {
    const cursorX = event.clientX;
    const cursorY = event.clientY;
    const x = cursorX - focusHalfWidth;
    const y = cursorY - focusHalfHeight;
    focus.style.top = `${y}px`;
    // focus.style.left = `${cursorX}px`;
    focus.style.left = `${x}px`;
});
console.dir(focusHalfHeight);