﻿function myFunction() {
    // Declare variables 
    var input, filter, table, tr, td, i, td1;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 1; i < tr.length + 1; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        td1 = tr[i].getElementsByTagName("td")[1];
        td2 = tr[i].getElementsByTagName("td")[8];

        if (td || td1 || td2) {

            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";

                continue;
            }
            else {

                if (td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";

                    continue;
                }
                else {
                    if (td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";

                        continue;
                    }
                }
            }


            tr[i].style.display = "none";
        }
    }
}