﻿$(document).ready(() => {
    $('#homepage').click(() => {
        localStorage.setItem("page", "/KnjigaNaloga/Index")
        $('#content').load('/KnjigaNaloga/Index');
    });

    $('#klijenti').click(() => {
        localStorage.setItem("page", "/Klijent/ListaKlijenata")
        $('#content').load('/Klijent/ListaKlijenata');
    });

    $('#vozari').click(() => {
        localStorage.setItem("page", "/Partner/ListaPartnera")
        $('#content').load('/Partner/ListaPartnera');
    });

    $('#evidencija').click(() => {
        localStorage.setItem("page", "/Evidencija/Index")
        $('#content').load('/Evidencija/Index');
    });

    $('#dokumentacija').click(() => {
        localStorage.setItem("page","/Dokumentacija/Index")
        $('#content').load('/Dokumentacija/Index');
    });

    $('#bla').click(() => {
        localStorage.setItem("page", "/KnjigaNaloga/Index")
        $('#content').load('/KnjigaNaloga/Index');
    });

   

})
$(document).ready(() => {
    let path = localStorage.getItem("page");
    $('#content').load(path);
});