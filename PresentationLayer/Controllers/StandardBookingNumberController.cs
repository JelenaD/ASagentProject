﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class StandardBookingNumberController : Controller
    {
        IAsAgentService service1 = new AsAgentService();
        

        // GET: AsaBookingNumber
        public ActionResult StandardList(UserDTO user , int? page)
        {
            var lista = service1.GetAllStandardNumbers().ToPagedList(page?? 1,10);
            return View(lista);
        }

        public ActionResult StandardList11(UserDTO user, int? page)
        {
            var lista = service1.GetAllStandardNumbers().ToPagedList(page ?? 1, 10);
            return View(lista);
        }

        [HttpGet]
        public ActionResult AddStandardBookingNumber()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddStandardBookingNumber11()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddStandardBookingNumber(StandardBookingNumberDTO dto)
        {
            service1.SaveStandardBookingNumber(dto);
            return RedirectToAction("StandardList");

        }

        [HttpPost]
        public ActionResult AddStandardBookingNumber11(StandardBookingNumberDTO dto)
        {
            service1.SaveStandardBookingNumber(dto);
            return RedirectToAction("StandardList11");

        }

        public ActionResult Edit(int id)
        {
            var number = service1.StandardBookingNumberId(id);

            return View(number);
        }

        public ActionResult Edit11(int id)
        {
            var number = service1.StandardBookingNumberId(id);

            return View(number);
        }

        [HttpPost]
        public ActionResult Edit(StandardBookingNumberDTO dto)
        {

            service1.SaveStandardBookingNumberChanges(dto);
            return RedirectToAction("StandardList");

        }

        [HttpPost]
        public ActionResult Edit11(StandardBookingNumberDTO dto)
        {

            service1.SaveStandardBookingNumberChanges(dto);
            return RedirectToAction("StandardList11");

        }

        [HttpPost]
        public ActionResult DeleteStandardBookingNumber(int id)
        {
            service1.DeleteStandardBookingNumber(id);
            return RedirectToAction("StandardList");
        }

        [HttpPost]
        public ActionResult DeleteStandardBookingNumber11(int id)
        {
            service1.DeleteStandardBookingNumber(id);
            return RedirectToAction("StandardList11");
        }
    }
}
