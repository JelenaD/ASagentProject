﻿using CommonLayer;
using DataLayer;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class TrosakController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        // GET: Trosak
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListaTroskova()
        {
            var troskovi = service1.GetAllCost();
            return View(troskovi);
        }

        [HttpGet]
        public ActionResult KreirajTrosak()
        {
            return View();
        }

        [HttpPost]
        public ActionResult KreirajTrosak(TroskoviDTO dto)
        {
            try
            {
                service1.SaveCost(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ListaTroskova");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var trosak = service1.EditCost(id);
            return View(trosak);
        }

        [HttpPost]
        public ActionResult Edit(TroskoviDTO trosak)
        {
            if (ModelState.IsValid)
            {
                service1.SaveCostsChanges(trosak);
                return RedirectToAction("ListaTroskova");
            }

            return View(trosak);
        }

        [HttpPost]
        public ActionResult DeleteCost(int id)
        {
            service1.DeleteCost(id);
            return RedirectToAction("ListaTroskova");
        }

    }
}