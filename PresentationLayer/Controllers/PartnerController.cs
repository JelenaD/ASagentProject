﻿using CommonLayer;
using DataLayer;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class PartnerController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListaPartnera()
        {
            var partneri = service1.GetPartners();
            return View(partneri);
        }

        [HttpGet]
        public ActionResult KreirajPartnera()
        {
            return View();
        }

        [HttpPost]
        public ActionResult KreirajPartnera(PartnerDTO dto)
        {
            try
            {
                service1.SavePartner(dto);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return RedirectToAction("ListaPartnera");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var partner = service1.EditPartner(id);
            return View(partner);
        }

        [HttpPost]
        public ActionResult Edit(PartnerDTO partner)
        {
            if (ModelState.IsValid)
            {
                service1.SavePartnerChanges(partner);
                return RedirectToAction("ListaPartnera");
            }

            return View(partner);
        }

        [HttpPost]
        public ActionResult DeletePartner(int id)
        {
            service1.DeletePartner(id);
            return RedirectToAction("ListaPartnera");
        }

    }
}