﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class InputInvoiceController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        public ActionResult InputInvoices(DateTime? dat1, DateTime? dat2, int? page)
        {

         if (dat1 != null && dat2 != null)
            {
                return View(service1.GetInInvoicesByDate(dat1, dat2).ToPagedList(page?? 1,10));
            }

            var fakture = service1.InputInvoice().ToPagedList(page?? 1,10);
            return View(fakture);
        }

        public ActionResult InputInvoices11(DateTime? dat1, DateTime? dat2, int? page)
        {

            if (dat1 != null && dat2 != null)
            {
                return View(service1.GetInInvoicesByDate(dat1, dat2).ToPagedList(page ?? 1, 10));
            }

            var fakture = service1.InputInvoice().ToPagedList(page ?? 1, 10);
            return View(fakture);
        }

        [HttpPost]
        public ActionResult DeleteInvoice(int id)
        {
            service1.DeleteInInvoice(id);
            return RedirectToAction("InputInvoices");
        }

        [HttpPost]
        public ActionResult DeleteInvoice11(int id)
        {
            service1.DeleteInInvoice(id);
            return RedirectToAction("InputInvoices11");
        }

        public ActionResult CreateInputInvoice()
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<BookDTO> knjigaNaloga = service1.GetBookCollection().ToList<BookDTO>();
            InputInvoiceDTO dto = new InputInvoiceDTO();
            dto.ShipperCollection = partneri;
            dto.BookCollection = knjigaNaloga;
            return View(dto);
        }

        public ActionResult CreateInputInvoice11()
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<BookDTO> knjigaNaloga = service1.GetBookCollection().ToList<BookDTO>();
            InputInvoiceDTO dto = new InputInvoiceDTO();
            dto.ShipperCollection = partneri;
            dto.BookCollection = knjigaNaloga;
            return View(dto);
        }

        [HttpPost]
        public ActionResult CreateInputInvoice(InputInvoiceDTO faktura)
        {
            var invoice = service1.GetNameOfInputInvoice(faktura.NumberOfInputInvoice);

            if (invoice != false)
            {
                service1.SaveInInvoice(faktura);
                return RedirectToAction("InputInvoices");
            }
            else
            {
                return RedirectToAction("CreateInputInvoice");
            }         
        }

        [HttpPost]
        public ActionResult CreateInputInvoice11(InputInvoiceDTO faktura)
        {
            var invoice = service1.GetNameOfInputInvoice(faktura.NumberOfInputInvoice);

            if (invoice != false)
            {
                service1.SaveInInvoice(faktura);
                return RedirectToAction("InputInvoices11");
            }
            else
            {
                return RedirectToAction("CreateInputInvoice11");
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<BookDTO> knjigeNaloga = service1.GetBookCollection().ToList<BookDTO>();
            var ulaznaFaktura = service1.FindInputInvoiceById(id);
            InputInvoiceDTO dto = new InputInvoiceDTO();
            dto.SerialNumberOfInputInvoice = id;
            dto.DateOfReceiptInvoice = ulaznaFaktura.DateOfReceiptInvoice;
            dto.ShipperId = ulaznaFaktura.ShipperId;
            dto.NumberOfInputInvoice = ulaznaFaktura.NumberOfInputInvoice;
            dto.AmountInDIN = ulaznaFaktura.AmountInDIN;
            dto.AmountInEUR = ulaznaFaktura.AmountInEUR;
            dto.ValueDate = ulaznaFaktura.ValueDate;
            dto.PaymentDate = ulaznaFaktura.PaymentDate;
            dto.Paid = ulaznaFaktura.Paid;
            dto.Comment = ulaznaFaktura.Comment;
            dto.BookId = ulaznaFaktura.BookId;
            dto.ShipperCollection = partneri;
            dto.BookCollection = knjigeNaloga;


            return View(dto);
        }

        [HttpGet]
        public ActionResult Edit11(int id)
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<BookDTO> knjigeNaloga = service1.GetBookCollection().ToList<BookDTO>();
            var ulaznaFaktura = service1.FindInputInvoiceById(id);
            InputInvoiceDTO dto = new InputInvoiceDTO();
            dto.SerialNumberOfInputInvoice = id;
            dto.DateOfReceiptInvoice = ulaznaFaktura.DateOfReceiptInvoice;
            dto.ShipperId = ulaznaFaktura.ShipperId;
            dto.NumberOfInputInvoice = ulaznaFaktura.NumberOfInputInvoice;
            dto.AmountInDIN = ulaznaFaktura.AmountInDIN;
            dto.AmountInEUR = ulaznaFaktura.AmountInEUR;
            dto.ValueDate = ulaznaFaktura.ValueDate;
            dto.PaymentDate = ulaznaFaktura.PaymentDate;
            dto.Paid = ulaznaFaktura.Paid;
            dto.Comment = ulaznaFaktura.Comment;
            dto.BookId = ulaznaFaktura.BookId;
            dto.ShipperCollection = partneri;
            dto.BookCollection = knjigeNaloga;


            return View(dto);
        }

        [HttpPost]
        public ActionResult Edit(InputInvoiceDTO dto)
        {
            
                service1.SaveInInvoiceChanges(dto);
                return RedirectToAction("InputInvoices");
            
        }

        [HttpPost]
        public ActionResult Edit11(InputInvoiceDTO dto)
        {

            service1.SaveInInvoiceChanges(dto);
            return RedirectToAction("InputInvoices11");

        }
    }
}