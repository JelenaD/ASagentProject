﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class ClientController : Controller
    {
        IAsAgentService service1 = new AsAgentService();


        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ClientList(int? page)
        {
            var klijenti = service1.AllClients().ToPagedList(page ?? 1, 10);
            return View(klijenti);
        }

        public ActionResult ClientList11(int? page)
        {
            var klijenti = service1.AllClients().ToPagedList(page ?? 1, 10);
            return View(klijenti);
        }

        [HttpGet]
        public ActionResult CreateClient()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateClient(ClientDTO dto)
        {
            try
            {
                service1.SaveClient(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ClientList");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var klijent = service1.FindClientById(id);
            ClientDTO dto = new ClientDTO();
            dto.ClientId = id;
            dto.NameOfClientCompany = klijent.NameOfClientCompany;
            dto.PIB = klijent.PIB;
            dto.Address = klijent.Address;
            dto.Email = klijent.Email;
            dto.Telephone = klijent.Telephone;
            dto.State = klijent.State;
            dto.ZipCode = klijent.ZipCode;
            dto.City = klijent.City;
            return View(dto);
        }

        [HttpGet]
        public ActionResult Edit11(int id)
        {
            var klijent = service1.FindClientById(id);
            ClientDTO dto = new ClientDTO();
            dto.ClientId = id;
            dto.NameOfClientCompany = klijent.NameOfClientCompany;
            dto.PIB = klijent.PIB;
            dto.Address = klijent.Address;
            dto.Email = klijent.Email;
            dto.Telephone = klijent.Telephone;
            dto.State = klijent.State;
            dto.ZipCode = klijent.ZipCode;
            dto.City = klijent.City;
            return View(dto);
        }

        [HttpPost]
        public ActionResult Edit(ClientDTO klijent)
        {
            if (ModelState.IsValid)
            {
                service1.SaveClientChanges(klijent);
                return RedirectToAction("ClientList");
            }

            return View(klijent);
        }

        [HttpPost]
        public ActionResult Edit11(ClientDTO klijent)
        {
            if (ModelState.IsValid)
            {
                service1.SaveClientChanges(klijent);
                return RedirectToAction("ClientList11");
            }

            return View(klijent);
        }

        [HttpPost]
        public ActionResult DeleteClient(int id)
        {
            service1.DeleteClient(id);
            return RedirectToAction("ClientList");
        }

        [HttpPost]
        public ActionResult DeleteClient11(int id)
        {
            service1.DeleteClient(id);
            return RedirectToAction("ClientList11");
        }

        [HttpGet]
        public ActionResult CreateClient11()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateClient11(ClientDTO dto)
        {
            try
            {
                service1.SaveClient(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ClientList11");
        }
    }
}