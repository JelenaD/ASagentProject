﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class AsaBookingNumberController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        // GET: AsaBookingNumber
        public ActionResult ASAList(int? page)
        {
            var lista = service1.GetAllAsaNumbers().ToPagedList(page?? 1,10);
            return View(lista);
        }

        public ActionResult ASAList11(int? page)
        {
            var lista = service1.GetAllAsaNumbers().ToPagedList(page ?? 1, 10);
            return View(lista);
        }

        [HttpGet]
        public ActionResult AddAsaBookingNumber()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddAsaBookingNumber(ASABookingNumberDTO dto)
        {
            service1.SaveAsaBookingNumber(dto);
            return RedirectToAction("ASAList");

        }

        [HttpGet]
        public ActionResult AddAsaBookingNumber11()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddAsaBookingNumber11(ASABookingNumberDTO dto)
        {
            service1.SaveAsaBookingNumber(dto);
            return RedirectToAction("ASAList11");

        }

        public ActionResult Edit(int id)
        {
            var number = service1.AsaBookingNumberById(id);

            return View(number);
        }

        public ActionResult Edit11(int id)
        {
            var number = service1.AsaBookingNumberById(id);

            return View(number);
        }

        [HttpPost]
        public ActionResult Edit(ASABookingNumberDTO dto)
        {

            service1.SaveAsaBookingNumberChanges(dto);
            return RedirectToAction("ASAList");

        }

        [HttpPost]
        public ActionResult Edit11(ASABookingNumberDTO dto)
        {

            service1.SaveAsaBookingNumberChanges(dto);
            return RedirectToAction("ASAList11");

        }

        [HttpPost]
        public ActionResult DeleteAsaBookingNumber(int id)
        {
            service1.DeleteAsaBookingNumber(id);
            return RedirectToAction("ASAList");
        }

        [HttpPost]
        public ActionResult DeleteAsaBookingNumber11(int id)
        {
            service1.DeleteAsaBookingNumber(id);
            return RedirectToAction("ASAList11");
        }
    }
}