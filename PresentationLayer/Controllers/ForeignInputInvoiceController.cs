﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class ForeignInputInvoiceController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        public ActionResult ForeignInputInvoices(string naziv, string predmet, DateTime? dat1, DateTime? dat2, int? page)
        {

            if (!String.IsNullOrEmpty(naziv) && String.IsNullOrEmpty(predmet) && dat1 == null && dat2 == null)
            {
                return View(service1.GetInInoInvoicesByName(naziv));
            }
            else if (!String.IsNullOrEmpty(predmet) && String.IsNullOrEmpty(naziv) && dat1 == null && dat2 == null)
            {
                return View(service1.GetInInoInvoicesBySubject(predmet));
            }
            else if (String.IsNullOrEmpty(naziv) && String.IsNullOrEmpty(predmet) && dat1 != null && dat2 != null)
            {
                return View(service1.GetInInoInvoicesByDate(dat1, dat2));
            }

            var fakture = service1.GetAllInoInInvoices().ToPagedList(page?? 1,10);
            return View(fakture);
        }

        public ActionResult ForeignInputInvoices11(string naziv, string predmet, DateTime? dat1, DateTime? dat2, int? page)
        {

            if (!String.IsNullOrEmpty(naziv) && String.IsNullOrEmpty(predmet) && dat1 == null && dat2 == null)
            {
                return View(service1.GetInInoInvoicesByName(naziv));
            }
            else if (!String.IsNullOrEmpty(predmet) && String.IsNullOrEmpty(naziv) && dat1 == null && dat2 == null)
            {
                return View(service1.GetInInoInvoicesBySubject(predmet));
            }
            else if (String.IsNullOrEmpty(naziv) && String.IsNullOrEmpty(predmet) && dat1 != null && dat2 != null)
            {
                return View(service1.GetInInoInvoicesByDate(dat1, dat2));
            }

            var fakture = service1.GetAllInoInInvoices().ToPagedList(page ?? 1, 10);
            return View(fakture);
        }

        [HttpPost]
        public ActionResult DeleteInvoice(int id)
        {
            service1.DeleteInoInInvoice(id);
            return RedirectToAction("ForeignInputInvoices");
        }

        [HttpPost]
        public ActionResult DeleteInvoice11(int id)
        {
            service1.DeleteInoInInvoice(id);
            return RedirectToAction("ForeignInputInvoices11");
        }

        public ActionResult CreateInvoice()
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<BookDTO> knjigaNaloga = service1.GetBookCollection().ToList<BookDTO>();
            ForeignInputInvoiceDTO dto = new ForeignInputInvoiceDTO();
            dto.ShipperCollection = partneri;
            dto.BookCollection = knjigaNaloga;
            return View(dto);
        }

        public ActionResult CreateInvoice11()
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<BookDTO> knjigaNaloga = service1.GetBookCollection().ToList<BookDTO>();
            ForeignInputInvoiceDTO dto = new ForeignInputInvoiceDTO();
            dto.ShipperCollection = partneri;
            dto.BookCollection = knjigaNaloga;
            return View(dto);
        }

        [HttpPost]
        public ActionResult CreateInvoice(ForeignInputInvoiceDTO faktura)
        {
            var invoice = service1.GetNameOfForeignInputInvoice(faktura.NumberOfForeignInputInvoice);

            if (invoice != false)
            {
                service1.SaveInoInInvoice(faktura);
                return RedirectToAction("ForeignInputInvoices");
            }
            else
            {
                return RedirectToAction("CreateInvoice");
            }
            
        }

        [HttpPost]
        public ActionResult CreateInvoice11(ForeignInputInvoiceDTO faktura)
        {
            var invoice = service1.GetNameOfForeignInputInvoice(faktura.NumberOfForeignInputInvoice);

            if (invoice != false)
            {
                service1.SaveInoInInvoice(faktura);
                return RedirectToAction("ForeignInputInvoices11");
            }
            else
            {
                return RedirectToAction("CreateInvoice11");
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<BookDTO> knjigeNaloga = service1.GetBookCollection().ToList<BookDTO>();
            var ulaznaFaktura = service1.FindInoInInvoiceById(id);
            ForeignInputInvoiceDTO dto = new ForeignInputInvoiceDTO();
            dto.SerialNumberOfForeignInputInvoice = id;
            dto.DateOfReceiptInvoice = ulaznaFaktura.DateOfReceiptInvoice;
            dto.ShipperId = ulaznaFaktura.ShipperId;
            dto.NumberOfForeignInputInvoice = ulaznaFaktura.NumberOfForeignInputInvoice;
            dto.AmountInEUR = ulaznaFaktura.AmountInEUR;
            dto.ValueDate = ulaznaFaktura.ValueDate;
            dto.PaymentDate = ulaznaFaktura.PaymentDate;
            dto.Paid = ulaznaFaktura.Paid;
            dto.Comment = ulaznaFaktura.Comment;
            dto.BookId = ulaznaFaktura.BookId;
            dto.ShipperCollection = partneri;
            dto.BookCollection = knjigeNaloga;


            return View(dto);
        }

        [HttpGet]
        public ActionResult Edit11(int id)
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<BookDTO> knjigeNaloga = service1.GetBookCollection().ToList<BookDTO>();
            var ulaznaFaktura = service1.FindInoInInvoiceById(id);
            ForeignInputInvoiceDTO dto = new ForeignInputInvoiceDTO();
            dto.SerialNumberOfForeignInputInvoice = id;
            dto.DateOfReceiptInvoice = ulaznaFaktura.DateOfReceiptInvoice;
            dto.ShipperId = ulaznaFaktura.ShipperId;
            dto.NumberOfForeignInputInvoice = ulaznaFaktura.NumberOfForeignInputInvoice;
            dto.AmountInEUR = ulaznaFaktura.AmountInEUR;
            dto.ValueDate = ulaznaFaktura.ValueDate;
            dto.PaymentDate = ulaznaFaktura.PaymentDate;
            dto.Paid = ulaznaFaktura.Paid;
            dto.Comment = ulaznaFaktura.Comment;
            dto.BookId = ulaznaFaktura.BookId;
            dto.ShipperCollection = partneri;
            dto.BookCollection = knjigeNaloga;


            return View(dto);
        }

        [HttpPost]
        public ActionResult Edit(ForeignInputInvoiceDTO dto)
        {
            
                service1.SaveInoInInvoiceChanges(dto);
                return RedirectToAction("ForeignInputInvoices");
            
           
        }

        [HttpPost]
        public ActionResult Edit11(ForeignInputInvoiceDTO dto)
        {

            service1.SaveInoInInvoiceChanges(dto);
            return RedirectToAction("ForeignInputInvoices11");


        }
    }
}