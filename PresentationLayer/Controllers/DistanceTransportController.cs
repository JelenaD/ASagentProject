﻿using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using CommonLayer;

namespace PresentationLayer.Controllers
{
    public class DistanceTransportController : Controller
    {
        IAsAgentService service1 = new AsAgentService();
        // GET: DistanceTransport
        public ActionResult Index(int? page)
        {
            var distancetransport = service1.GetDistanceTransportList().ToPagedList(page ?? 1, 10);
            return View(distancetransport);
        }

        public ActionResult Index11(int? page)
        {
            var distancetransport = service1.GetDistanceTransportList().ToPagedList(page ?? 1, 10);
            return View(distancetransport);
        }

        [HttpPost]
        public ActionResult DeleteDistanceTransport(int id)
        {
            service1.DeleteDistanceTransport(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteDistanceTransport11(int id)
        {
            service1.DeleteDistanceTransport(id);
            return RedirectToAction("Index11");
        }

        public ActionResult Add()
        {
            List<ShipperDTO> shippers = service1.Shippers().ToList<ShipperDTO>();
            DistanceTransportDTO dto = new DistanceTransportDTO();
            dto.ShipperCollection = shippers;

            return View(dto);
        }

        public ActionResult Add11()
        {
            List<ShipperDTO> shippers = service1.Shippers().ToList<ShipperDTO>();
            DistanceTransportDTO dto = new DistanceTransportDTO();
            dto.ShipperCollection = shippers;

            return View(dto);
        }

        [HttpPost]
        public ActionResult Add(DistanceTransportDTO dto)
        {
            try
            {
                service1.SaveDistanceTransport(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Add11(DistanceTransportDTO dto)
        {
            try
            {
                service1.SaveDistanceTransport(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            List<ShipperDTO> shippers = service1.Shippers().ToList<ShipperDTO>();
            var distance = service1.FindDistanceTransportById(id);
            DistanceTransportDTO dto = new DistanceTransportDTO();
            dto.DtId = id;
            dto.DistanceName = distance.DistanceName;
            dto.ShipperId = distance.ShipperId;
            dto.TransportName = distance.TransportName;
            dto.ShipperCollection = shippers;

            return View(dto);
        }

        [HttpGet]
        public ActionResult Edit11(int id)
        {
            List<ShipperDTO> shippers = service1.Shippers().ToList<ShipperDTO>();
            var distance = service1.FindDistanceTransportById(id);
            DistanceTransportDTO dto = new DistanceTransportDTO();
            dto.DtId = id;
            dto.DistanceName = distance.DistanceName;
            dto.ShipperId = distance.ShipperId;
            dto.TransportName = distance.TransportName;
            dto.ShipperCollection = shippers;

            return View(dto);
        }

        [HttpPost]
        public ActionResult Edit(DistanceTransportDTO dto)
        {

            service1.SaveDistanceTransportChanges(dto);
            return RedirectToAction("Index");

        }

        [HttpPost]
        public ActionResult Edit11(DistanceTransportDTO dto)
        {

            service1.SaveDistanceTransportChanges(dto);
            return RedirectToAction("Index11");

        }
    }
}