﻿using CommonLayer;
using DataLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace PresentationLayer.Controllers
{
    public class UserController : Controller
    {
        IAsAgentService service1 = new AsAgentService();
       
       public ActionResult Index()
        {
            return View();
        }

        public ActionResult Landing()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(UserDTO userModel)
        {
            var user = service1.CheckIfUserExistsByUsernameAndPassword(userModel.Username, userModel.Password);

            if (user == null)
            {
                userModel.LoginErrorMessage = "Pokusaj ponovo";
                return View("Index", userModel);
            }
            else if(user.Role==UserRole.Administrator) 
            {
                Session["userID"] = user.UserId;
                Session["userName"] = user.Username;
                return RedirectToAction("Index", "Book");
            }
            else
            {
                Session["userID"] = user.UserId;
                Session["userName"] = user.Username;
                return RedirectToAction("Index11", "Book");
            }
        }

        public ActionResult Show(int? page)
        {
           
            var users = service1.AllUsers().ToPagedList(page ?? 1, 10);
            return View(users);
        }

        public ActionResult LogOut()
        {
            Session.Abandon();
            return RedirectToAction("Landing", "User");
        }

        [AllowAnonymous]
        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registrate(UserDTO dto)
        {
            var username = service1.GetUsername(dto.Username);

            if (username != false)
            {
                service1.SaveUser(dto);
                return RedirectToAction("Show");
            }
            else
            {
                return RedirectToAction("Registration");
            }

        }


        public ActionResult Edit(int id)
        {
            var user = service1.FindById(id);
            return View(user);
        }

        [HttpPost]
        public ActionResult Edit(UserDTO user)
        {
            if (ModelState.IsValid)
            {
                service1.SaveChanges(user);
                return RedirectToAction("Show");
            }
            return View(user);
        }

        [HttpPost]
        public ActionResult DeleteUser(int id)
        {
            service1.DeleteUser(id);
            return RedirectToAction("Show");
        }

        
    }
}