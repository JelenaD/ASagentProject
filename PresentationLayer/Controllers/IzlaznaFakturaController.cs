﻿using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class IzlaznaFakturaController : Controller
    {
        IAsAgentService service1 = new AsAgentService();
        // GET: IzlaznaFaktura
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListaIzlaznihFaktura()
        {
            var iznazneFakture = service1.GetAllOutInvoices();
            return View(iznazneFakture);
        }

        [HttpPost]
        public ActionResult DeleteInvoice(int id)
        {
            service1.DeleteOutgoingInvoice(id);
            return RedirectToAction("ListaIzlaznihFaktura");
        }
    }
}