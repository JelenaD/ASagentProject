﻿using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class DodatnaDokumentaController : Controller
    {

        IAsAgentService service1 = new AsAgentService();

        // GET: DodatnaDokumenta
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DodatnaDokumenta()
        {
            var dokumenta = service1.GetAllDocuments();
            return View(dokumenta);
        }

        [HttpPost]
        public ActionResult DeleteAttachment(int id)
        {
            service1.DeleteAddAttachments(id);
            return RedirectToAction("DodatnaDokumenta");
        }
    }
}