﻿using CommonLayer;
using DataLayer;
using ServiceLayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class ParnterPonudaController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        // GET: ParnterPonuda
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListaPonuda()
        {
            var ponude = service1.GetAllOffers();
            return View(ponude);
        }

       

        [HttpGet]
        public ActionResult KreirajPonudu()
        {
            var lista = service1.GetPartners();
            PartnerPonudaDTO dto = new PartnerPonudaDTO();

            foreach(var p in lista)
            {
                //PartnerDTO dtopa = (PartnerDTO)p;
                PartnerDTO dtop = new PartnerDTO(((PartnerDTO)p).PartnerId, ((PartnerDTO)p).Pib, ((PartnerDTO)p).Naziv, ((PartnerDTO)p).Ime, ((PartnerDTO)p).Prezime, ((PartnerDTO)p).Adresa,
                   ((PartnerDTO)p).Email, ((PartnerDTO)p).Telefon,  ((PartnerDTO)p).Drzava);

                //dtop staviti u listu i onda gore proslediti u dto
                dto.PartnerItems.Add(new SelectListItem { Value = dtop.PartnerId.ToString(), Text = dtop.Naziv} );
            }
            
            return View(dto);
        }

        [HttpPost]
        public ActionResult KreirajPonudu(PartnerPonudaDTO partnerii)
        {
            try
            {
                PartnerPonudaDTO partnerDto = new PartnerPonudaDTO();
                //partnerDto.PartnerId = Int32.Parse(partnerii.ListaPartnera.SelectedValue);
                //partnerDto.Nazivpartnera = partnerii.First().Text;
                partnerDto.Id = 3333;
                partnerDto.NazivPonude = "nazivPonude3333";
                partnerDto.Opis = "djadjadadadadad";
                partnerDto.Cena = 266;
                partnerDto.Datum = DateTime.Now;

                service1.SavePartnerOffer(partnerDto);
            }
            catch (Exception )
            {
                throw;
            }
            return RedirectToAction("ListaPonuda");
        }

        [HttpPost]
        public ActionResult DeleteOffer(int id)
        {
            service1.DeletePartnerOffer(id);
            return RedirectToAction("ListaPonuda");
        }
    }
}