﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class OutgoingInvoiceController : Controller
    {
        IAsAgentService service1 = new AsAgentService();


        public ActionResult OutgoingInvoiceList(DateTime? dat1, DateTime? dat2, int? page)
        {
            if (dat1 != null && dat2 != null)
            {
                return View(service1.GetOutInvoicesByDate(dat1, dat2).ToPagedList(page ?? 1, 10));
            }

            var iznazneFakture = service1.OutgoingInvoices().ToPagedList(page?? 1,10);
            return View(iznazneFakture);
        }

        public ActionResult OutgoingInvoiceList11(DateTime? dat1, DateTime? dat2, int? page)
        {
            if (dat1 != null && dat2 != null)
            {
                return View(service1.GetOutInvoicesByDate(dat1, dat2).ToPagedList(page ?? 1, 10));
            }

            var iznazneFakture = service1.OutgoingInvoices().ToPagedList(page ?? 1, 10);
            return View(iznazneFakture);
        }

        public ActionResult CreateOugoingInvoice()
        {
            List<ClientDTO> klijenti = service1.AllClients().ToList<ClientDTO>();
            List<BookDTO> knjige = service1.GetBookCollection().ToList<BookDTO>();
            OutgoingInvoiceDTO dto = new OutgoingInvoiceDTO();
            dto.ClientCollection = klijenti;
            dto.BookCollection = knjige;

            return View(dto);
        }

        [HttpPost]
        public ActionResult CreateOugoingInvoice(OutgoingInvoiceDTO faktura)
        {
            var invoice = service1.GetNameOfOutgoingInvoice(faktura.NumberOfOutgoingInvoice);

            if (invoice != false)
            {
                service1.SaveInvoice(faktura);
                return RedirectToAction("OutgoingInvoiceList");
            }
            else
            {
                return RedirectToAction("CreateOugoingInvoice");
            }

        }

        public ActionResult CreateOugoingInvoice11()
        {
            List<ClientDTO> klijenti = service1.AllClients().ToList<ClientDTO>();
            List<BookDTO> knjige = service1.GetBookCollection().ToList<BookDTO>();
            OutgoingInvoiceDTO dto = new OutgoingInvoiceDTO();
            dto.ClientCollection = klijenti;
            dto.BookCollection = knjige;

            return View(dto);
        }

        [HttpPost]
        public ActionResult CreateOugoingInvoice11(OutgoingInvoiceDTO faktura)
        {
            var invoice = service1.GetNameOfOutgoingInvoice(faktura.NumberOfOutgoingInvoice);

            if (invoice != false)
            {
                service1.SaveInvoice(faktura);
                return RedirectToAction("OutgoingInvoiceList11");
            }
            else
            {
                return RedirectToAction("CreateOugoingInvoice11");
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            List<BookDTO> books = service1.GetBookCollection().ToList<BookDTO>();
            List<ClientDTO> clients = service1.AllClients().ToList<ClientDTO>();

            var invoice = service1.FindIInvoiceById(id);
            OutgoingInvoiceDTO dto = new OutgoingInvoiceDTO();
            dto.OutgoingInvoiceId = id;
            dto.DateOfReceipt = invoice.DateOfReceipt;
            dto.NumberOfOutgoingInvoice = invoice.NumberOfOutgoingInvoice;
            dto.AmountDIN = invoice.AmountDIN;
            dto.AmountInEUR = invoice.AmountInEUR;
            dto.ValueDate = invoice.ValueDate;
            dto.PaymentDate = invoice.PaymentDate;
            dto.Paid = invoice.Paid;
            dto.Comment = invoice.Comment;
            dto.BookId = invoice.BookId;
            dto.ClientId = invoice.ClientId;
            dto.BookCollection = books;
            dto.ClientCollection = clients;

            return View(dto);

        }

        [HttpPost]
        public ActionResult Edit(OutgoingInvoiceDTO dto)
        {

            service1.SaveChangesOfInvoice(dto);
            return RedirectToAction("OutgoingInvoiceList");

        }

        [HttpPost]
        public ActionResult DeleteInvoice(int id)
        {
            service1.DeleteOutInvoice(id);
            return RedirectToAction("OutgoingInvoiceList");
        }

        [HttpGet]
        public ActionResult Edit11(int id)
        {
            List<BookDTO> books = service1.GetBookCollection().ToList<BookDTO>();
            List<ClientDTO> clients = service1.AllClients().ToList<ClientDTO>();

            var invoice = service1.FindIInvoiceById(id);
            OutgoingInvoiceDTO dto = new OutgoingInvoiceDTO();
            dto.OutgoingInvoiceId = id;
            dto.DateOfReceipt = invoice.DateOfReceipt;
            dto.NumberOfOutgoingInvoice = invoice.NumberOfOutgoingInvoice;
            dto.AmountDIN = invoice.AmountDIN;
            dto.AmountInEUR = invoice.AmountInEUR;
            dto.ValueDate = invoice.ValueDate;
            dto.PaymentDate = invoice.PaymentDate;
            dto.Paid = invoice.Paid;
            dto.Comment = invoice.Comment;
            dto.BookId = invoice.BookId;
            dto.ClientId = invoice.ClientId;
            dto.BookCollection = books;
            dto.ClientCollection = clients;

            return View(dto);

        }

        [HttpPost]
        public ActionResult Edit11(OutgoingInvoiceDTO dto)
        {

            service1.SaveChangesOfInvoice(dto);
            return RedirectToAction("OutgoingInvoiceList11");

        }

        [HttpPost]
        public ActionResult DeleteInvoice11(int id)
        {
            service1.DeleteOutInvoice(id);
            return RedirectToAction("OutgoingInvoiceList11");
        }
    }
}
