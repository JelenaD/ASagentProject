﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class ClientOfferController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        public ActionResult ClientOfferList(int? page)
        {
            var ponude = service1.ClientOffers().ToPagedList(page?? 1,10) ;
            return View(ponude);
        }

        public ActionResult ClientOfferList11(int? page)
        {
            var ponude = service1.ClientOffers().ToPagedList(page ?? 1, 10);
            return View(ponude);
        }

        public ActionResult CreateClientOffer()
        {
            List<ClientDTO> klijenti = service1.AllClients().ToList<ClientDTO>();
            List<BookDTO> knjige = service1.GetBookCollection().ToList<BookDTO>();
            List<ShipperOfferDTO> ponude = service1.ShipperOffers().ToList<ShipperOfferDTO>();
            ClientOffersDTO dto = new ClientOffersDTO();
            dto.ClientCollection = klijenti;
            dto.BookCollection = knjige;
            dto.ShipperOfferCollection = ponude;

            return View(dto);
        }

        public ActionResult CreateClientOffer11()
        {
            List<ClientDTO> klijenti = service1.AllClients().ToList<ClientDTO>();
            List<BookDTO> knjige = service1.GetBookCollection().ToList<BookDTO>();
            List<ShipperOfferDTO> ponude = service1.ShipperOffers().ToList<ShipperOfferDTO>();
            ClientOffersDTO dto = new ClientOffersDTO();
            dto.ClientCollection = klijenti;
            dto.BookCollection = knjige;
            dto.ShipperOfferCollection = ponude;

            return View(dto);
        }

        [HttpPost]
        public ActionResult CreateClientOffer(ClientOffersDTO ponuda)
        {
            try
            {
                service1.SaveClientOffer(ponuda);
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("ClientOfferList");
        }

        [HttpPost]
        public ActionResult CreateClientOffer11(ClientOffersDTO ponuda)
        {
            try
            {
                service1.SaveClientOffer(ponuda);
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("ClientOfferList11");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            List<ClientDTO> klijenti = service1.AllClients().ToList<ClientDTO>();
            List<BookDTO> knjige = service1.GetBookCollection().ToList<BookDTO>();
            List<ShipperOfferDTO> ponude = service1.ShipperOffers().ToList<ShipperOfferDTO>();
            var p = service1.FindClientOfferById(id);
            ClientOffersDTO dto = new ClientOffersDTO();
            dto.ClientOfferId = id;
            dto.ClientCollection = klijenti;
            dto.BookCollection = knjige;
            dto.ShipperOfferCollection = ponude;
            dto.OffersName = p.OffersName;
            dto.Description = p.Description;
            dto.ASA = p.ASA;
            dto.Accepted = p.Accepted;
            dto.Date = p.Date;
            dto.ClientId = p.ClientId;
            dto.BookId = p.BookId;
            dto.ShipperOfferId = p.ShipperOfferId;

            return View(dto);
        }

        [HttpPost]
        public ActionResult Edit(ClientOffersDTO dto)
        {

            service1.SaveClientOfferChanges(dto);
            return RedirectToAction("ClientOfferList");

        }

        public ActionResult ShowShipperOffer(int id)
        {
            ShowShipperOfferInClientOfferDTO dto = new ShowShipperOfferInClientOfferDTO();
            dto.ShipperOffers = service1.GetShipperOffersByCOId(id);

            return View(dto);
        }

        [HttpGet]
        public ActionResult Edit11(int id)
        {
            List<ClientDTO> klijenti = service1.AllClients().ToList<ClientDTO>();
            List<BookDTO> knjige = service1.GetBookCollection().ToList<BookDTO>();
            List<ShipperOfferDTO> ponude = service1.ShipperOffers().ToList<ShipperOfferDTO>();
            var p = service1.FindClientOfferById(id);
            ClientOffersDTO dto = new ClientOffersDTO();
            dto.ClientOfferId = id;
            dto.ClientCollection = klijenti;
            dto.BookCollection = knjige;
            dto.ShipperOfferCollection = ponude;
            dto.OffersName = p.OffersName;
            dto.Description = p.Description;
            dto.ASA = p.ASA;
            dto.Accepted = p.Accepted;
            dto.Date = p.Date;
            dto.ClientId = p.ClientId;
            dto.BookId = p.BookId;
            dto.ShipperOfferId = p.ShipperOfferId;

            return View(dto);
        }

        [HttpPost]
        public ActionResult Edit11(ClientOffersDTO dto)
        {

            service1.SaveClientOfferChanges(dto);
            return RedirectToAction("ClientOfferList11");

        }

        [HttpPost]
        public ActionResult DeleteClientOffer(int id)
        {
            service1.DeleteClientOffer(id);
            return RedirectToAction("ClientOfferList");
        }

        [HttpPost]
        public ActionResult DeleteClientOffer11(int id)
        {
            service1.DeleteClientOffer(id);
            return RedirectToAction("ClientOfferList11");
        }
    }
}