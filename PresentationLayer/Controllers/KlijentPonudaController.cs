﻿using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class KlijentPonudaController : Controller
    {

        IAsAgentService service1 = new AsAgentService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListaKlijentPonuda()
        {
            var ponude = service1.GetAllClientOffers();
            return View(ponude);
        }

        [HttpPost]
        public ActionResult DeleteClientOffer(int id)
        {
            service1.DeleteClientOffer(id);
            return RedirectToAction("ListaKlijentPonuda");
        }
    }
}