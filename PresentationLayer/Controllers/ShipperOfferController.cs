﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class ShipperOfferController : Controller
    {
        IAsAgentService service1 = new AsAgentService();


        public ActionResult ShipperOfferList(int? page)
        {

            var ponude = service1.ShipperOffers().ToPagedList(page?? 1,10);
            return View(ponude);
        }

        public ActionResult ShipperOfferList11(int? page)
        {

            var ponude = service1.ShipperOffers().ToPagedList(page ?? 1, 10);
            return View(ponude);
        }

        public ActionResult CreateShipperOffer()
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<ClientOffersDTO> clients = service1.ClientOffers().ToList<ClientOffersDTO>();
            ShipperOfferDTO dto = new ShipperOfferDTO();
            dto.ShipperCollection = partneri;
            dto.ClientOffersDTO = clients;
            return View(dto);
        }

        public ActionResult CreateShipperOffer11()
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<ClientOffersDTO> clients = service1.ClientOffers().ToList<ClientOffersDTO>();
            ShipperOfferDTO dto = new ShipperOfferDTO();
            dto.ShipperCollection = partneri;
            dto.ClientOffersDTO = clients;
            return View(dto);
        }

        [HttpPost]
        public ActionResult CreateShipperOffer(ShipperOfferDTO ponuda)
        {
            try
            {
                service1.SaveShipperOffer(ponuda);
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("ShipperOfferList");
        }

        [HttpPost]
        public ActionResult CreateShipperOffer11(ShipperOfferDTO ponuda)
        {
            try
            {
                service1.SaveShipperOffer(ponuda);
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("ShipperOfferList11");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<ClientOffersDTO> clientOffers = service1.ClientOffers().ToList<ClientOffersDTO>();

            var ponuda = service1.FindShipperOfferById(id);
            ShipperOfferDTO dto = new ShipperOfferDTO();
            dto.ShipperOfferId = ponuda.ShipperOfferId;
            dto.ShipperOfferName = ponuda.ShipperOfferName;
            dto.Description = ponuda.Description;
            dto.Price = ponuda.Price;
            dto.Email = ponuda.Email;
            dto.Telephone = ponuda.Telephone;
            dto.Distance = ponuda.Distance;
            dto.Date = ponuda.Date;
            dto.ClientOffersDTO = clientOffers;
            dto.ShipperCollection = partneri;
            dto.ShipperId = ponuda.ShipperId;
            dto.ClientOfferId = ponuda.ClientOfferId;

            return View(dto);
        }

        [HttpGet]
        public ActionResult Edit11(int id)
        {
            List<ShipperDTO> partneri = service1.Shippers().ToList<ShipperDTO>();
            List<ClientOffersDTO> clientOffers = service1.ClientOffers().ToList<ClientOffersDTO>();

            var ponuda = service1.FindShipperOfferById(id);
            ShipperOfferDTO dto = new ShipperOfferDTO();
            dto.ShipperOfferId = ponuda.ShipperOfferId;
            dto.ShipperOfferName = ponuda.ShipperOfferName;
            dto.Description = ponuda.Description;
            dto.Price = ponuda.Price;
            dto.Email = ponuda.Email;
            dto.Telephone = ponuda.Telephone;
            dto.Distance = ponuda.Distance;
            dto.Date = ponuda.Date;
            dto.ClientOffersDTO = clientOffers;
            dto.ShipperCollection = partneri;
            dto.ShipperId = ponuda.ShipperId;
            dto.ClientOfferId = ponuda.ClientOfferId;

            return View(dto);
        }

        [HttpPost]
        public ActionResult Edit(ShipperOfferDTO dto)
        {

            service1.SaveChangesOfShipperOffer(dto);
            return RedirectToAction("ShipperOfferList");

        }

        [HttpPost]
        public ActionResult Edit11(ShipperOfferDTO dto)
        {

            service1.SaveChangesOfShipperOffer(dto);
            return RedirectToAction("ShipperOfferList11");

        }

        [HttpPost]
        public ActionResult DeleteOffer(int id)
        {
            service1.DeleteOffer(id);
            return RedirectToAction("ShipperOfferList");
        }

        [HttpPost]
        public ActionResult DeleteOffer11(int id)
        {
            service1.DeleteOffer(id);
            return RedirectToAction("ShipperOfferList11");
        }
    }
}