﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class BookController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        public ActionResult Index(int? page, string sortBy)
        {
            ViewBag.SortDateParametar = string.IsNullOrEmpty(sortBy) ? "Name desc" : "";
            ViewBag.SortBookObjectParametar = sortBy == "ObjectBook" ? "ObjectBook desc" : "ObjectBook";

            //var knjige = service1.GetBookCollection().ToPagedList(page?? 1,10);

            var books = service1.GetBookCollection().AsQueryable();
            foreach (var k in books)
            {
                k.ClientName = service1.GetNameOfClientByBookId(k.BookId);
                k.PriceOfInputInvoice = service1.GetPriceOfInputInvoice(k.BookId) + service1.GetPriceOfForeignInputInovice(k.BookId);
                k.AsaPrice = service1.GetPriceOfOutgoingInvoice(k.BookId) + service1.GetPriceOfForeignOutgoingInvoice(k.BookId);
                k.Difference = k.AsaPrice - k.PriceOfInputInvoice;
            }

            switch (sortBy)
            {
                case "Name desc":
                    books = books.OrderByDescending(x => x.DateOfLoading);
                    break;
                case "ObjectBook desc":
                    books = books.OrderByDescending(x => x.ObjectBook);
                    break;
                case "Name":
                    books = books.OrderBy(x => x.DateOfLoading);
                    break;
                default:
                    books = books.OrderBy(x => x.ObjectBook);
                    break;
            }
           
            return View(books.ToPagedList(page?? 1,10));
        }

        public ActionResult Index11(int? page, string sortBy)
        {
            ViewBag.SortDateParametar = string.IsNullOrEmpty(sortBy) ? "Name desc" : "";
            ViewBag.SortBookObjectParametar = sortBy == "ObjectBook" ? "ObjectBook desc" : "ObjectBook";

            //var knjige = service1.GetBookCollection().ToPagedList(page?? 1,10);

            var books = service1.GetBookCollection().AsQueryable();
            foreach (var k in books)
            {
                k.ClientName = service1.GetNameOfClientByBookId(k.BookId);
                k.PriceOfInputInvoice = service1.GetPriceOfInputInvoice(k.BookId) + service1.GetPriceOfForeignInputInovice(k.BookId);
                k.AsaPrice = service1.GetPriceOfOutgoingInvoice(k.BookId) + service1.GetPriceOfForeignOutgoingInvoice(k.BookId);
                k.Difference = k.AsaPrice - k.PriceOfInputInvoice;
            }

            switch (sortBy)
            {
                case "Name desc":
                    books = books.OrderByDescending(x => x.DateOfLoading);
                    break;
                case "ObjectBook desc":
                    books = books.OrderByDescending(x => x.ObjectBook);
                    break;
                case "ObjectBook":
                    books = books.OrderBy(x => x.ObjectBook);
                    break;
                default:
                    books = books.OrderBy(x => x.DateOfLoading);
                    break;
            }

            return View(books.ToPagedList(page ?? 1, 10));
        }


        public ActionResult AddBook()
        {
            List<ASABookingNumberDTO> asaBookingNumbers = service1.GetAllAsaNumbers();
            List<StandardBookingNumberDTO> obicanBookingNumbers = service1.GetAllStandardNumbers();

            BookDTO dto = new BookDTO();

            dto.AsaCollection = asaBookingNumbers;

            dto.StandardCollection = obicanBookingNumbers;
            return View(dto);
        }

        [HttpPost]
        public ActionResult AddBook(BookDTO dto)
        {
                service1.SaveBook(dto);
                return RedirectToAction("Index");

        }

        public ActionResult AddBook11()
        {
            List<ASABookingNumberDTO> asaBookingNumbers = service1.GetAllAsaNumbers();
            List<StandardBookingNumberDTO> obicanBookingNumbers = service1.GetAllStandardNumbers();

            BookDTO dto = new BookDTO();

            dto.AsaCollection = asaBookingNumbers;

            dto.StandardCollection = obicanBookingNumbers;
            return View(dto);
        }

        [HttpPost]
        public ActionResult AddBook11(BookDTO dto)
        {
            service1.SaveBook(dto);
            return RedirectToAction("Index11");

        }



        [HttpGet]
        public ActionResult Edit(int id)
        {
            var b = service1.BookById(id);
            BookDTO dto = new BookDTO();
            dto.BookId = b.BookId;
            dto.ObjectBook = b.ObjectBook;
            dto.PlaceOfLoading = b.PlaceOfLoading;
            dto.DateOfLoading = b.DateOfLoading;
            dto.PlaceOfUnloading = b.PlaceOfUnloading;
            dto.Goods = b.Goods;
            dto.Customer = b.Customer;
            dto.QuantityOfGoods = b.QuantityOfGoods;
            dto.ContainerTruck = b.ContainerTruck;
            dto.BookingNumber = b.BookingNumber;

            return View(dto);

        }

        [HttpPost]
        public ActionResult Edit(BookDTO dto)
        { 
                service1.SaveBookChanges(dto);
                return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit11(int id)
        {
            var b = service1.BookById(id);
            BookDTO dto = new BookDTO();
            dto.BookId = b.BookId;
            dto.ObjectBook = b.ObjectBook;
            dto.PlaceOfLoading = b.PlaceOfLoading;
            dto.DateOfLoading = b.DateOfLoading;
            dto.PlaceOfUnloading = b.PlaceOfUnloading;
            dto.Goods = b.Goods;
            dto.Customer = b.Customer;
            dto.QuantityOfGoods = b.QuantityOfGoods;
            dto.ContainerTruck = b.ContainerTruck;
            dto.BookingNumber = b.BookingNumber;

            return View(dto);

        }

        [HttpPost]
        public ActionResult Edit11(BookDTO dto)
        {
            service1.SaveBookChanges(dto);
            return RedirectToAction("Index11");
        }

        public ActionResult OutInvoicesList(int id)
        {
            InvoiceMultipleDTO myModel = new InvoiceMultipleDTO();
            myModel.OutInvoices = service1.GetOutInvoicesByBookId(id);
            myModel.OutInoInvoices = service1.GetOutInoInvoicesByBookId(id);

            return View(myModel);
        }

        public ActionResult ShowTableOfInvoices(int id)
        {
            InvoiceMultipleDTO myModel = new InvoiceMultipleDTO();
            myModel.InputInvoices = service1.GetInvoiceByBookId(id);
            myModel.ForeignInputInvoices = service1.GetInoInvoiceByBookId(id);
            return View(myModel);
        }

        [HttpPost]
        public ActionResult DeleteBook(int id)
        {
            service1.DeleteBook(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteBook11(int id)
        {
            service1.DeleteBook(id);
            return RedirectToAction("Index11");
        }

        public ActionResult OutInvoicesList11(int id)
        {
            InvoiceMultipleDTO myModel = new InvoiceMultipleDTO();
            myModel.OutInvoices = service1.GetOutInvoicesByBookId(id);
            myModel.OutInoInvoices = service1.GetOutInoInvoicesByBookId(id);

            return View(myModel);
        }

        public ActionResult ShowTableOfInvoices11(int id)
        {
            InvoiceMultipleDTO myModel = new InvoiceMultipleDTO();
            myModel.InputInvoices = service1.GetInvoiceByBookId(id);
            myModel.ForeignInputInvoices = service1.GetInoInvoiceByBookId(id);
            return View(myModel);
        }


    }
}