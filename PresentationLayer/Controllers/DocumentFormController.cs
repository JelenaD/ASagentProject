﻿using PresentationLayer.Models;
using Rotativa;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class DocumentFormController : Controller
    {
        public ActionResult ForeignOffer()
        {
            return View();
        }

        public ActionResult Invoice()
        {
            return View();
        }

        public ActionResult OutgoingInvoice()
        {
            return View();
        }

        public ActionResult Offer()
        {
            return View();
        }

        public ActionResult REUSEFORM()
        {
            return View();
        }


        public ActionResult BinaryTest()
        {
            var file = new ActionAsPdf("REUSEFORM", new { name = "Giorgio" });
            var byteArray = file.BuildFile(this.ControllerContext);

            return file;

        }

        public ActionResult ReuseForm01()
        {
            var file = new ActionAsPdf("REUSEFORM11", new { name = "Giorgio" });
            var byteArray = file.BuildFile(this.ControllerContext);

            return file;

        }

        public ActionResult OutgoingInvoicePDF()
        {
            var file = new ActionAsPdf("OutgoingInvoice", new { name = "Giorgio" });
            var byteArray = file.BuildFile(this.ControllerContext);

            return file;

        }

        public ActionResult OutgoingInvoice11PDF()
        {
            var file = new ActionAsPdf("OutgoingInvoice11", new { name = "Giorgio" });
            var byteArray = file.BuildFile(this.ControllerContext);

            return file;

        }


        public ActionResult ForeignOfferPDF()
        {
            var file = new ActionAsPdf("ForeignOffer", new { name = "Giorgio" });
            var byteArray = file.BuildFile(this.ControllerContext);

            return file;

        }

        public ActionResult ForeignOffer11PDF()
        {

            var file = new ActionAsPdf("ForeignOffer11", new { name = "Giorgio" });
            var byteArray = file.BuildFile(this.ControllerContext);

            return file;

        }



        public ActionResult InvoicePDF()
        {
            var file = new ActionAsPdf("Invoice", new { name = "ASagent" });
            var byteArray = file.BuildFile(this.ControllerContext);

            return file;
        }

        public ActionResult Invoice11PDF()
        {
            var file = new ActionAsPdf("Invoice11", new { name = "ASagent" });
            var byteArray = file.BuildFile(this.ControllerContext);

            return file;
        }

        public ActionResult OfferPDF()
        {
            var file = new ActionAsPdf("Offer", new { name = "ASagent" });
            var byteArray = file.BuildFile(this.ControllerContext);

            return file;
        }

        public ActionResult Offer11PDF()
        {
            var file = new ActionAsPdf("Offer11", new { name = "ASagent" });
            var byteArray = file.BuildFile(this.ControllerContext);

            return file;
        }


        public ActionResult ForeignOffer11()
        {
            return View();
        }

        public ActionResult Invoice11()
        {
            return View();
        }

        public ActionResult OutgoingInvoice11()
        {
            return View();
        }

        public ActionResult Offer11()
        {
            return View();
        }

        public ActionResult REUSEFORM11()
        {
            return View();
        }

        public ActionResult SendEmail()
        {

            return View();
        }

        public ActionResult Pocetna()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(MailModel mailModel, HttpPostedFileBase fileUploader)
        {
            if (ModelState.IsValid)
            {
                string from = "asagentns@gmail.com";
                using (MailMessage mail = new MailMessage(from, mailModel.To))
                {
                    mail.Subject = mailModel.Subject;
                    mail.Body = mailModel.Body;
                    if (!string.IsNullOrEmpty(mailModel.Cc))
                    {
                        mail.CC.Add(mailModel.Cc);
                    }
                    if (fileUploader != null)
                    {
                        string fileName = Path.GetFileName(fileUploader.FileName);
                        mail.Attachments.Add(new Attachment(fileUploader.InputStream, fileName));
                    }
                    mail.IsBodyHtml = false;
                    SmtpClient smtp = new SmtpClient();

                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;

                    NetworkCredential networkCredential = new NetworkCredential(from, "dusanverica1983");

                    smtp.UseDefaultCredentials = true;

                    smtp.Credentials = networkCredential;

                    smtp.Port = 587;

                    smtp.Send(mail);

                    ViewBag.Message = "Sent";

                    return View("Pocetna");

                }

            }

            else

            {

                return View("Pocetna");

            }

        }




    }
    }
