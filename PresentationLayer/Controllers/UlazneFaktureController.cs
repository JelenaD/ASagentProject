﻿using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class UlazneFaktureController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        // GET: UlazneFakture
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllInvoices()
        {
            var fakture = service1.GetAllInvoices();
            return View(fakture);
        }

        [HttpPost]
        public ActionResult DeleteInvoice(int id)
        {
            service1.DeleteInovice(id);
            return RedirectToAction("GetAllInvoices");
        }
    }
}