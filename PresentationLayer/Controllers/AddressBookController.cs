﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class AddressBookController : Controller
    {
        IAsAgentService service = new AsAgentService();

        public ActionResult AddressBook(int? page)
        {
            var lista = service.GetAddressBookList().ToPagedList(page?? 1,10);
            return View(lista);
        }

        public ActionResult AddressBook11(int? page)
        {
            var lista = service.GetAddressBookList().ToPagedList(page ?? 1, 10);
            return View(lista);
        }

        [HttpGet]
        public ActionResult CreateAddressBook()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CreateAddressBook11()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateAddressBook(AddressBookDTO dto)
        {
            try
            {
                service.SaveAddressBook(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AddressBook");
        }

        [HttpPost]
        public ActionResult CreateAddressBook11(AddressBookDTO dto)
        {
            try
            {
                service.SaveAddressBook(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AddressBook11");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var address = service.AddressBookById(id);

            return View(address);
        }

        [HttpGet]
        public ActionResult Edit11(int id)
        {
            var address = service.AddressBookById(id);

            return View(address);
        }

        [HttpPost]
        public ActionResult Edit(AddressBookDTO dto)
        {
            if (ModelState.IsValid)
            {
                service.SaveAddressChanges(dto);
                return RedirectToAction("AddressBook");
            }

            return View(dto);
        }

        [HttpPost]
        public ActionResult Edit11(AddressBookDTO dto)
        {
            if (ModelState.IsValid)
            {
                service.SaveAddressChanges(dto);
                return RedirectToAction("AddressBook11");
            }

            return View(dto);
        }

        [HttpPost]
        public ActionResult DeleteAddressBook(int id)
        {
            service.DeleteAddressBook(id);
            return RedirectToAction("AddressBook");
        }

        [HttpPost]
        public ActionResult DeleteAddressBook11(int id)
        {
            service.DeleteAddressBook(id);
            return RedirectToAction("AddressBook11");
        }
    }
}
