﻿using CommonLayer;
using DataLayer;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class KlijentController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListaKlijenata()
        {
            var klijenti = service1.GetClients();
            return View(klijenti);
        }

        [HttpGet]
        public ActionResult KreirajKlijenta()
        {
            return View();
        }

        [HttpPost]
        public ActionResult KreirajKlijenta(KlijentDTO dto)
        {
            try
            {
                service1.SaveClient(dto);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ListaKlijenata");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var klijent = service1.EditClient(id);
            return View(klijent);
        }

        [HttpPost]
        public ActionResult Edit(KlijentDTO klijent)
        {
            if (ModelState.IsValid)
            {
                service1.SaveClientChanges(klijent);
                return RedirectToAction("ListaKlijenata");
            }
           
            return View(klijent);
        }

        [HttpPost]
        public ActionResult DeleteClient(int id)
        {
            service1.DeleteClient(id);
            return RedirectToAction("ListaKlijenata");
        }

    }
}