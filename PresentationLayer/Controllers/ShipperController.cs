﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class ShipperController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        // GET: Shipper
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Shippers(int? page)
        { 
            var shipper = service1.Shippers().ToPagedList(page?? 1, 10);
            return View(shipper);
        }

        public ActionResult Shippers11(int? page)
        {
            var shipper = service1.Shippers().ToPagedList(page ?? 1, 10);
            return View(shipper);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create11()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ShipperDTO dto)
        {
            try
            {
                service1.SaveShipper(dto);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return RedirectToAction("Shippers");
        }

        [HttpPost]
        public ActionResult Create11(ShipperDTO dto)
        {
            try
            {
                service1.SaveShipper(dto);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return RedirectToAction("Shippers11");
        }


        public ActionResult Edit(int id)
        {
            var partner = service1.FindShipperById(id);
            return View(partner);
        }

        public ActionResult Edit11(int id)
        {
            var partner = service1.FindShipperById(id);
            return View(partner);
        }

        [HttpPost]
        public ActionResult Edit(ShipperDTO shipper)
        { 
                service1.SaveShipperChanges(shipper);

                return RedirectToAction("Shippers");
            
        }

        [HttpPost]
        public ActionResult Edit11(ShipperDTO shipper)
        {
            service1.SaveShipperChanges(shipper);

            return RedirectToAction("Shippers11");

        }

        [HttpPost]
        public ActionResult DeletePartner(int id)
        {
            service1.DeleteShipper(id);
            return RedirectToAction("Shippers");
        }

        [HttpPost]
        public ActionResult DeletePartner11(int id)
        {
            service1.DeleteShipper(id);
            return RedirectToAction("Shippers11");
        }

        public ActionResult DistanceTransportList(int id)
        {
            DistanceTransportListDTO dto = new DistanceTransportListDTO();
            dto.DistanceTransportList = service1.GetDistanceTransportByShipperId(id);

            return View(dto);
        }

        public ActionResult DistanceTransportList11(int id)
        {
            DistanceTransportListDTO dto = new DistanceTransportListDTO();
            dto.DistanceTransportList = service1.GetDistanceTransportByShipperId(id);

            return View(dto);
        }
    }
}