﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class ForeignOutgoingInvoiceController : Controller
    {
        IAsAgentService service1 = new AsAgentService();


        public ActionResult InoOutInvoicesList(DateTime? dat1, DateTime? dat2, int? page)
        {          
            if (dat1 != null && dat2 != null)
            {
                return View(service1.GetOutInoInvoicesByDate(dat1, dat2).ToPagedList(page?? 1,10));
            }

            var iznazneFakture = service1.AllInoOutInvoices().ToPagedList(page?? 1,10);
            return View(iznazneFakture);
        }

        public ActionResult InoOutInvoicesList11(DateTime? dat1, DateTime? dat2, int? page)
        {
            if (dat1 != null && dat2 != null)
            {
                return View(service1.GetOutInoInvoicesByDate(dat1, dat2).ToPagedList(page ?? 1, 10));
            }

            var iznazneFakture = service1.AllInoOutInvoices().ToPagedList(page ?? 1, 10);
            return View(iznazneFakture);
        }

        public ActionResult CreateForeignInvoice()
        {
            List<ClientDTO> klijenti = service1.AllClients().ToList<ClientDTO>();
            List<BookDTO> knjige = service1.GetBookCollection().ToList<BookDTO>();
            ForeignOutgoingInvoiceDTO dto = new ForeignOutgoingInvoiceDTO();
            dto.ClientCollection = klijenti;
            dto.BookCollection = knjige;

            return View(dto);
        }

        public ActionResult CreateForeignInvoice11()
        {
            List<ClientDTO> klijenti = service1.AllClients().ToList<ClientDTO>();
            List<BookDTO> knjige = service1.GetBookCollection().ToList<BookDTO>();
            ForeignOutgoingInvoiceDTO dto = new ForeignOutgoingInvoiceDTO();
            dto.ClientCollection = klijenti;
            dto.BookCollection = knjige;

            return View(dto);
        }

        [HttpPost]
        public ActionResult CreateForeignInvoice(ForeignOutgoingInvoiceDTO faktura)
        {
            var invoice = service1.GetNameOfForeignOutgoingInvoice(faktura.NumberOfOutgoingInvoice);

            if (invoice != false)
            {
                service1.SaveInoInvoice(faktura);
                return RedirectToAction("InoOutInvoicesList");
            }
            else
            {
                return RedirectToAction("CreateForeignInvoice");
            }

        }

        [HttpPost]
        public ActionResult CreateForeignInvoice11(ForeignOutgoingInvoiceDTO faktura)
        {

            var invoice = service1.GetNameOfForeignOutgoingInvoice(faktura.NumberOfOutgoingInvoice);

            if (invoice != false)
            {
                service1.SaveInoInvoice(faktura);
                return RedirectToAction("InoOutInvoicesList11");
            }
            else
            {
                return RedirectToAction("CreateForeignInvoice11");
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            List<BookDTO> knjige = service1.GetBookCollection().ToList<BookDTO>();
            List<ClientDTO> klijenti = service1.AllClients().ToList<ClientDTO>();


            var faktura = service1.FindInoOutInvoiceById(id);
            ForeignOutgoingInvoiceDTO dto = new ForeignOutgoingInvoiceDTO();
            dto.ForeignOutgoingInvoiceId = id;
            dto.DateOfReceipt = faktura.DateOfReceipt;
            dto.NumberOfOutgoingInvoice = faktura.NumberOfOutgoingInvoice;
            dto.AmountInEUR = faktura.AmountInEUR;
            dto.ValueDate = faktura.ValueDate;
            dto.PaymentDate = faktura.PaymentDate;
            dto.Paid = faktura.Paid;
            dto.Comment = faktura.Comment;
            dto.BookId = faktura.BookId;
            dto.ClientId = faktura.ClientId;
            dto.BookCollection = knjige;
            dto.ClientCollection = klijenti;

            return View(dto);

        }

        [HttpGet]
        public ActionResult Edit11(int id)
        {
            List<BookDTO> knjige = service1.GetBookCollection().ToList<BookDTO>();
            List<ClientDTO> klijenti = service1.AllClients().ToList<ClientDTO>();


            var faktura = service1.FindInoOutInvoiceById(id);
            ForeignOutgoingInvoiceDTO dto = new ForeignOutgoingInvoiceDTO();
            dto.ForeignOutgoingInvoiceId = id;
            dto.DateOfReceipt = faktura.DateOfReceipt;
            dto.NumberOfOutgoingInvoice = faktura.NumberOfOutgoingInvoice;
            dto.AmountInEUR = faktura.AmountInEUR;
            dto.ValueDate = faktura.ValueDate;
            dto.PaymentDate = faktura.PaymentDate;
            dto.Paid = faktura.Paid;
            dto.Comment = faktura.Comment;
            dto.BookId = faktura.BookId;
            dto.ClientId = faktura.ClientId;
            dto.BookCollection = knjige;
            dto.ClientCollection = klijenti;

            return View(dto);

        }

        [HttpPost]
        public ActionResult Edit(ForeignOutgoingInvoiceDTO dto)
        {

            service1.SaveChangesOfOutInoInvoice(dto);
            return RedirectToAction("InoOutInvoicesList");

        }

        [HttpPost]
        public ActionResult Edit11(ForeignOutgoingInvoiceDTO dto)
        {

            service1.SaveChangesOfOutInoInvoice(dto);
            return RedirectToAction("InoOutInvoicesList11");

        }

        [HttpPost]
        public ActionResult DeleteInvoice(int id)
        {
            service1.DeleteOutInoInvoice(id);
            return RedirectToAction("InoOutInvoicesList");
        }

        [HttpPost]
        public ActionResult DeleteInvoice11(int id)
        {
            service1.DeleteOutInoInvoice(id);
            return RedirectToAction("InoOutInvoicesList11");
        }
    }
}