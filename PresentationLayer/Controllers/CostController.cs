﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class CostController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        public ActionResult CostList(string racun, DateTime? date1, DateTime? date2, int? page)
        {
            ViewBag.troskovi = service1.AllCosts();

            if (date1 != null && date2 != null)
            {
                return View(service1.GetCostByDate(date1, date2));
            }
            else if (racun != null)
            {
                return View(service1.FilterCostsByName(racun));
            }

            var troskovi = service1.AllCosts().ToPagedList(page?? 1, 10);
            return View(troskovi);
        }

        public ActionResult CostList11(string racun, DateTime? date1, DateTime? date2, int? page)
        {
            ViewBag.troskovi = service1.AllCosts();

            if (date1 != null && date2 != null)
            {
                return View(service1.GetCostByDate(date1, date2));
            }
            else if (racun != null)
            {
                return View(service1.FilterCostsByName(racun));
            }

            var troskovi = service1.AllCosts().ToPagedList(page ?? 1, 10);
            return View(troskovi);
        }

        [HttpGet]
        public ActionResult AddCost()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCost(CostsDTO dto)
        {
            try
            {
                service1.SaveCost(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("CostList");
        }

        [HttpGet]
        public ActionResult AddCost11()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCost11(CostsDTO dto)
        {
            try
            {
                service1.SaveCost(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("CostList11");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var trosak = service1.FindCostById(id);
            CostsDTO dto = new CostsDTO();
            dto.CostId = id;
            dto.AccountNumber = trosak.AccountNumber;
            dto.DateOfReceipt = trosak.DateOfReceipt;
            dto.PaymentDate = trosak.PaymentDate;
            dto.Amount = trosak.Amount;
            dto.Paid = trosak.Paid;

            return View(dto);
        }

        [HttpPost]
        public ActionResult Edit(CostsDTO trosak)
        {
            if (ModelState.IsValid)
            {
                service1.SaveCostChanges(trosak);
                return RedirectToAction("CostList");
            }

            return View(trosak);
        }

        [HttpGet]
        public ActionResult Edit11(int id)
        {
            var trosak = service1.FindCostById(id);
            CostsDTO dto = new CostsDTO();
            dto.CostId = id;
            dto.AccountNumber = trosak.AccountNumber;
            dto.DateOfReceipt = trosak.DateOfReceipt;
            dto.PaymentDate = trosak.PaymentDate;
            dto.Amount = trosak.Amount;
            dto.Paid = trosak.Paid;

            return View(dto);
        }

        [HttpPost]
        public ActionResult Edit11(CostsDTO trosak)
        {
            if (ModelState.IsValid)
            {
                service1.SaveCostChanges(trosak);
                return RedirectToAction("CostList11");
            }

            return View(trosak);
        }

        [HttpPost]
        public ActionResult DeleteCost(int id)
        {
            service1.DeleteCost(id);
            return RedirectToAction("CostList");
        }

        [HttpPost]
        public ActionResult DeleteCost11(int id)
        {
            service1.DeleteCost(id);
            return RedirectToAction("CostList11");
        }
    }
}
