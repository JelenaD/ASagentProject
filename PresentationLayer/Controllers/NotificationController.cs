﻿using CommonLayer;
using PagedList;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class NotificationController : Controller
    {
        IAsAgentService service = new AsAgentService();

        public ActionResult Index(UserDTO userModel)
        {
            InvoiceMultipleDTO myModel = new InvoiceMultipleDTO();
            myModel.InputInvoices = service.GetInvoicesByExpireDate();
            myModel.ForeignInputInvoices = service.GetInoInvoicesByExpireDate();
            myModel.OutInvoices = service.GetOutgoingInvoicesByExpireDate();
            myModel.OutInoInvoices = service.GetforeignOutgoingInvoicesByExpireDate();
           
            return View(myModel);
        }

        public ActionResult Report(DateTime? dat1, DateTime? dat2,int? page)
        {
            if (dat1 != null && dat2 != null)
            {
                return View(service.GetOutInvoicesByDate(dat1, dat2).ToPagedList(page ?? 1, 10));
            }
            var reporting = service.OutgoingInvoices().Distinct().ToPagedList(page?? 1,10);
            return View(reporting);
        }
    }
}