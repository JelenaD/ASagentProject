﻿using PresentationLayer.Models;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PresentationLayer
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private Thread mailThread { get; set; }
        MailModel mailModel;
        IAsAgentService service = new AsAgentService();

        protected void Application_Start()
        {
            mailModel = new MailModel();
            mailThread = new Thread(new ThreadStart(ThreadSendNextMail));

            mailThread.Start();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void ThreadSendNextMail()
        {
            while (true)
            {
                DateTime currentDateTime = DateTime.Now;
                int hour = currentDateTime.Hour;
                int minutes = currentDateTime.Minute;
                int seconds = currentDateTime.Second;

                if (hour == 12 && minutes == 04 && seconds == 00)
                {
                    HostingEnvironment.QueueBackgroundWorkItem(async cancellationToken =>
                    {
                        var listInvoices = service.GetEmailInvoicesByExpireDate();
                        var listInoInvoices = service.GetEmailINOInvoicesByExpireDate();

                        if (listInvoices != null)
                        {
                            foreach (var va in listInvoices)
                            {
                                string temp = "Molimo vas izmirite zaduzenje po fakturi " + va.NumberOfInputInvoice + " u iznosu od " + va.AmountInEUR + ".";
                                string subject = va.NumberOfInputInvoice;
                                mailModel.SendEmail("asagentns@gmail.com", "asagentns@gmail.com", subject, temp);
                            }
                        }

                        if (listInoInvoices != null)
                        {
                            foreach (var va in listInoInvoices)
                            {
                                string temp = "Molimo vas izmirite zaduzenje po fakturi " + va.NumberOfForeignInputInvoice + " u iznosu od " + va.AmountInEUR + ".";
                                string subject = va.NumberOfForeignInputInvoice;
                                mailModel.SendEmail("asagentns@gmail.com", "asagentns@gmail.com", subject, temp);
                            }
                        }

                    });

                    Thread.Sleep(new TimeSpan(0, 0, 30));
                }

            }
        }


    }
}
