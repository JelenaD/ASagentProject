﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Models
{
    public class ButtonModel
    {
        private bool isButtonVisible = true;
        
        public bool IsButtonTrue { 
            get { return isButtonVisible; }
            set { this.isButtonVisible = value; } }
    }
}