﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PresentationLayer
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "User", action = "Landing", id = UrlParameter.Optional }
            );

           // routes.MapRoute(
           //    name: "registracija",
           //    url: "{controller}/{action}/{id}",
           //    defaults: new { controller = "Korisnik", action = "Registracija", id = UrlParameter.Optional }
           //);



            //  routes.MapRoute(
            //    name: "dodajpartnera",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Partner", action = "DodajPartnera", id = UrlParameter.Optional }
            //);

            //  routes.MapRoute(
            //    name: "dodajklijenta",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Klijent", action = "KreirajKlijenta", id = UrlParameter.Optional }
            //);
        }
    }
}
