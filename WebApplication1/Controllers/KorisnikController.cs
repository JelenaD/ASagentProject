﻿using CommonLayer;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class KorisnikController : Controller
    {
        IAsAgentService service1 = new AsAgentService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LandingPage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Logovanje(KorisnikDTO korisnikModel)
        {
            var korisnik = service1.CheckIfUserExistsByUsernameAndPassword(korisnikModel.Korisnickoime, korisnikModel.Lozinka);

            if (korisnik == null)
            {
                korisnikModel.LoginErrorMessage = "Neispravno korisnicko ime ili lozinka.";
                return View("Index", korisnikModel);
            }
            else
            {
                Session["userID"] = korisnik.KorisnikId;
                Session["userName"] = korisnik.KorisnickoIme;
                return RedirectToAction("Index", "KnjigaNaloga");
            }
        }

        public ActionResult Prikazi(string ime, string prezime)
        {
            ViewBag.korisnici = service1.GetAll();

            if (ime != null && prezime != null)
            {
                return View(service1.GetUsersByNameAndLastName(ime, prezime));
            }
            var korisnici = service1.GetAll();
            return View(korisnici);
        }

        public ActionResult LogOut()
        {
            return RedirectToAction("Index", "Korisnik");
        }

        [AllowAnonymous]
        public ActionResult Registracija()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registruj(KorisnikDTO dto)
        {
            try
            {
                service1.SaveUser(dto);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return RedirectToAction("Prikazi");
        }


        public ActionResult Edit(int id)
        {
            var korisnik = service1.EditUser(id);
            return View(korisnik);
        }

        [HttpPost]
        public ActionResult Edit(KorisnikDTO korisnik)
        {
            if (ModelState.IsValid)
            {
                service1.SaveChanges(korisnik);
                return RedirectToAction("Prikazi");
            }
            return View(korisnik);
        }

        [HttpPost]
        public ActionResult DeleteUser(int id)
        {
            service1.DeleteUser(id);
            return RedirectToAction("Prikazi");
        }


    }
}