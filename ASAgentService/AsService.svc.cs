﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using CommonLayer;
using DataLayer;

namespace AsService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AsAgentService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AsAgentService.svc or AsAgentService.svc.cs at the Solution Explorer and start debugging.
    public class AsService : IAsAgentService
    {
        public bool CheckIfUserExists(string username, string password)
        {
            using (ContextClass cc = new ContextClass())
            {
                var korisnik = cc.Korisnik.Where(k => k.KorisnickoIme == username &&
                k.Lozinka == password).FirstOrDefault();

                if (korisnik == null)
                {
                    
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public List<KorisnikDTO> GetAll()
        {
            List<Korisnik> korisnici;
            using (ContextClass cc = new ContextClass())
            {
                korisnici = cc.Korisnik.ToList();

            }

            List<KorisnikDTO> korisniciDTO = new List<KorisnikDTO>(korisnici.Count);

            foreach (var k in korisnici)
            {
                KorisnikDTO dto = new KorisnikDTO();
                dto.Ime = k.Ime;
                dto.Prezime = k.Prezime;
                dto.Adresa = k.Adresa;
                dto.Email = k.Email;
                dto.Telefon = k.Telefon;
                dto.Korisnickoime = k.KorisnickoIme;
                dto.Lozinka = k.Lozinka;
                dto.Uloga = k.Uloga;
                korisniciDTO.Add(dto);
            }

            return korisniciDTO;
        }
    }
}
