﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer
{
    public class DistanceTransportListDTO
    {
        public List<DistanceTransportDTO> DistanceTransportList { get; set; }
    }
}
