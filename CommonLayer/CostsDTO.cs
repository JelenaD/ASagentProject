﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class CostsDTO
    {
        private int costId;
        private string accountNumber;
        private DateTime dateOfReceipt;
        private DateTime paymentDate;
        private double amount;
        private bool paid;

        public CostsDTO() { }

        public CostsDTO(int costId, string accountNumber, DateTime dateOfReceipt, DateTime paymentDate, double amount,
            bool paid)
        {
            this.costId = costId;
            this.accountNumber = accountNumber;
            this.dateOfReceipt = dateOfReceipt;
            this.paymentDate = paymentDate;
            this.amount = amount;
            this.paid = paid;
        }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int CostId
        {
            get { return costId; }
            set { costId = value; }
        }

        [DataMember]
        [Display(Name = "RAČUN")]
        [Required(ErrorMessage = "Unesite račun!")]
        public string AccountNumber
        {
            get { return accountNumber; }
            set { accountNumber = value; }
        }

        [DataMember]
        [Display(Name = "DATUM PRIJEMA")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Unesite datum prijema!")]
        public DateTime DateOfReceipt
        {
            get { return dateOfReceipt; }
            set { dateOfReceipt = value; }
        }

        [DataMember]
        [Display(Name = "DATUM PLAĆANJA")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Unesite datum plaćanja!")]
        public DateTime PaymentDate
        {
            get { return paymentDate; }
            set { paymentDate = value; }
        }

        [DataMember]
        [Display(Name = "IZNOS")]
        [Required(ErrorMessage = "Unesite iznos!")]
        public double Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        [DataMember]
        [Display(Name = "PLAĆENO")]
        [Required(ErrorMessage = "Označi da li je plaćeno!")]
        public bool Paid
        {
            get { return paid; }
            set { paid = value; }
        }
    }
}
