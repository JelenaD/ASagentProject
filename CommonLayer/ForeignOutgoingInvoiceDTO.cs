﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class ForeignOutgoingInvoiceDTO
    {
        private int foreignOutgoingInvoiceId;
        private DateTime dateOfReceipt;
        private string clientName;
        private string clientPlace;
        private string numberOfOutgoingInvoice;
        private double? amountInEUR;
        private DateTime valueDate;
        private DateTime paymentDate;
        private bool paid;
        private string comment;
        private int bookId;
        private int clientId;
        private string objectBook;
        private List<ClientDTO> clientCollection;
        private List<BookDTO> bookCollection;
        private DateTime? dateFrom;
        private DateTime? dateTo;

        public ForeignOutgoingInvoiceDTO() { }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int ForeignOutgoingInvoiceId
        {
            get { return foreignOutgoingInvoiceId; }
            set { foreignOutgoingInvoiceId = value; }
        }

        [DataMember]
        [Display(Name = "DATUM FAKTURISANJA")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Unesite datum fakturisanja!")]
        public DateTime DateOfReceipt
        {
            get { return dateOfReceipt; }
            set { dateOfReceipt = value; }
        }

        [DataMember]
        [Display(Name = "NAZIV KLIJENTA")]
        [Required(ErrorMessage = "Unesite naziv klijenta!")]
        public string ClientName
        {
            get { return clientName; }
            set { clientName = value; }
        }

        [DataMember]
        [Display(Name = "MESTO KLIJENTA")]
        [Required(ErrorMessage = "Unesite mesto klijenta!")]
        public string ClientPlace
        {
            get { return clientPlace; }
            set { clientPlace = value; }
        }

        [DataMember]
        [Display(Name = "BROJ FAKTURE")]
        [Required(ErrorMessage = "Unesite broj fakture!")]
        public string NumberOfOutgoingInvoice
        {
            get { return numberOfOutgoingInvoice; }
            set { numberOfOutgoingInvoice = value; }
        }

        [DataMember]
        [Display(Name = "EUR/USD")]
        [Required(ErrorMessage = "Unesite iznos u EUR/USD!")]
        public double? AmountInEUR
        {
            get { return amountInEUR; }
            set { amountInEUR = value; }
        }

        [DataMember]
        [Display(Name = "DATUM VALUTE")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Unesite datum valute!")]
        public DateTime ValueDate
        {
            get { return valueDate; }
            set { valueDate = value; }
        }

        [DataMember]
        [Display(Name = "DATUM PLAĆANJA")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Unesite datum plaćanja!")]
        public DateTime PaymentDate
        {
            get { return paymentDate; }
            set { paymentDate = value; }
        }


        [DataMember]
        [Display(Name = "PLAĆENO")]
        [Required(ErrorMessage = "Označite da li je plaćeno!")]
        public bool Paid
        {
            get { return paid; }
            set { paid = value; }
        }

        [DataMember]
        [Display(Name = "KOMENTAR")]
        [Required(ErrorMessage = "Unesite komentar!")]
        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        [DataMember]
        [Display(Name = "KLIJENT")]
        public int ClientId
        {
            get { return clientId; }
            set { clientId = value; }
        }

        [DataMember]
        [Display(Name = "KNJIGA NALOGA")]
        public int BookId
        {
            get { return bookId; }
            set { bookId = value; }
        }

        [DataMember]
        [Display(Name = "PREDMET")]
        [Required(ErrorMessage = "Unesite predmet!")]
        public string ObjectBook
        {
            get { return objectBook; }
            set { objectBook = value; }
        }

        [DataMember]
        [Display(Name = "KLIJENT")]
        public List<ClientDTO> ClientCollection
        {
            get { return clientCollection; }
            set { clientCollection = value; }
        }

        [DataMember]
        [Display(Name = "KNJIGA NALOGA")]
        public List<BookDTO> BookCollection
        {
            get { return bookCollection; }
            set { bookCollection = value; }
        }

        [DataMember]
        public DateTime? DateTo
        {
            get { return dateTo; }
            set { dateTo = value; }
        }

        [DataMember]
        public DateTime? DateFrom
        {
            get { return dateFrom; }
            set { dateFrom = value; }
        }
    }
}
