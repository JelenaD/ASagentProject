﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class ShipperDTO
    {
        private int shipperId;
        private string pib;
        private string companyName;
        private string address;
        private string email;
        private string telephone;
        private string state;
        private string distanceTransportName;
        private int distanceTransportId;
        private List<DistanceTransportDTO> distanceTransportCollection;
        private List<ShipperOfferDTO> offerCollection;
        private string zipCode;
        private string city;

        public ShipperDTO() { }

        public ShipperDTO(int shipperId, string pib, string companyName, string address, string email, string telephone, string state, string zipCode, string city)
        {
            this.shipperId = shipperId;
            this.pib = pib;
            this.companyName = companyName;
            this.address = address;
            this.email = email;
            this.telephone = telephone;
            this.state = state;
            this.zipCode = zipCode;
            this.city = city;
        }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int ShipperId
        {
            get { return shipperId; }
            set { shipperId = value; }
        }

        [DataMember]
        [Display(Name = "PIB")]
        [Required(ErrorMessage = "Unesite pib!")]
        public string Pib
        {
            get { return pib; }
            set { pib = value; }
        }

        [DataMember]
        [Display(Name = "VOZAR")]
        [Required(ErrorMessage = "Unesite naziv kompanije!")]
        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        [DataMember]
        [Display(Name = "ZIP CODE")]
        [Required(ErrorMessage = "Unesite zip code!")]
        public string ZipCode
        {
            get { return zipCode; }
            set { zipCode = value; }
        }

        [DataMember]
        [Display(Name = "RELACIJA-TRANSPORT")]
        [Required(ErrorMessage = "Unesite relaciju!")]
        public string DistanceTransportName
        {
            get { return distanceTransportName; }
            set { distanceTransportName = value; }
        }

        [DataMember]
        public int DistanceTransportId
        {
            get { return distanceTransportId; }
            set { distanceTransportId = value; }
        }

        [DataMember]
        [Display(Name = "GRAD")]
        [Required(ErrorMessage = "Unesite grad!")]
        public string City
        {
            get { return city; }
            set { city = value; }
        }


        [DataMember]
        [Display(Name = "ADRESA")]
        [Required(ErrorMessage = "Unesite adresu!")]
        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        [DataMember]
        [Display(Name = "E-MAIL ADRESA")]
        [Required(ErrorMessage = "Unesite e-mail adresu!")]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        [Display(Name = "TELEFON")]
        [Required(ErrorMessage = "Unesite broj telefona!")]
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        [DataMember]
        [Display(Name = "DRŽAVA")]
        [Required(ErrorMessage = "Unesite državu!")]
        public string State
        {
            get { return state; }
            set { state = value; }
        }

        [DataMember]
        [Display(Name = "PONUDE")]
        [Required(ErrorMessage = "Odaberite ponudu!")]
        public List<ShipperOfferDTO> OfferCollection
        {
            get { return offerCollection; }
            set { offerCollection = value; }
        }

        [DataMember]
        [Display(Name = "RELACIJE")]
        public List<DistanceTransportDTO> DistanceTransportCollection
        {
            get { return distanceTransportCollection; }
            set { distanceTransportCollection = value; }
        }
    }
}
