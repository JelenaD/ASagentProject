﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer
{
    [DataContract]
    public class UlaznaFakturaDTO
    {
        private int id;
        private string nazivfakture;
        private string imevozara;
        private string prezimevozara;
        private string opis;
        private DateTime datumprijema;
        private DateTime datumplacanja;
        private double iznos;
        private bool placeno;
        private string nazivpartnera;
        private string predmet;

        public UlaznaFakturaDTO()
        {

        }

        public UlaznaFakturaDTO(int id, string nazivfakture, string imevozara, string prezimevozara, string opis,
            DateTime datumprijema, DateTime datumplacanja, double iznos, bool placeno, string nazivpartnera,
            string predmet)
        {
            this.id = id;
            this.nazivfakture = nazivfakture;
            this.imevozara = imevozara;
            this.prezimevozara = prezimevozara;
            this.opis = opis;
            this.datumprijema = datumprijema;
            this.datumplacanja = datumplacanja;
            this.iznos = iznos;
            this.placeno = placeno;
            this.nazivpartnera = nazivpartnera;
            this.predmet = predmet;
        }
        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string NazivFakture
        {
            get { return nazivfakture; }
            set { nazivfakture = value; }
        }

        [DataMember]
        public string Imevozara
        {
            get { return imevozara; }
            set { imevozara = value; }
        }

        [DataMember]
        public string Prezimevozara
        {
            get { return prezimevozara; }
            set { prezimevozara = value; }
        }

        [DataMember]
        public string Opis
        {
            get { return opis; }
            set { opis = value; }
        }

        [DataMember]
        public DateTime Datumprijema
        {
            get { return datumprijema; }
            set { datumprijema = value; }
        }

        [DataMember]
        public DateTime Datumplacanja
        {
            get { return datumplacanja; }
            set { datumplacanja = value; }
        }

        [DataMember]
        public double Iznos
        {
            get { return iznos; }
            set { iznos = value; }
        }

        [DataMember]
        public bool Placeno
        {
            get { return placeno; }
            set { placeno = value; }
        }

        [DataMember]
        public string Nazivpartnera
        {
            get { return nazivpartnera; }
            set { nazivpartnera = value; }
        }

        [DataMember]
        public string Predmet
        {
            get { return predmet; }
            set { predmet = value; }
        }

    }
}
