﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class DistanceTransportDTO
    {
        private int dtId;
        private string distanceName;
        private string transportName;
        private string shipperName;
        private int shipperId;
        private List<ShipperDTO> shipperCollection;

        public DistanceTransportDTO() { }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int DtId
        {
            get { return dtId; }
            set { dtId = value; }
        }

        [DataMember]
        [Display(Name = "RELACIJA")]
        [Required(ErrorMessage = "Unesite relaciju!")]
        public string DistanceName
        {
            get { return distanceName; }
            set { distanceName = value; }
        }

        [DataMember]
        [Display(Name = "VOZAR")]
        public string ShipperName
        {
            get { return shipperName; }
            set { shipperName = value; }
        }

        [DataMember]
        [Display(Name = "VOZILO")]
        [Required(ErrorMessage = "Unesite vrstu vozila!")]
        public string TransportName
        {
            get { return transportName; }
            set { transportName = value; }
        }

        [DataMember]
        [Display(Name = "VOZAR")]
        public int ShipperId
        {
            get { return shipperId; }
            set { shipperId = value; }
        }

        [DataMember]
        [Display(Name = "VOZARI")]
        public List<ShipperDTO> ShipperCollection
        {
            get { return shipperCollection; }
            set { shipperCollection = value; }
        }
    }
}
