﻿using System.Runtime.Serialization;

namespace CommonLayer
{
    [DataContract]
    public class KlijentDTO
    {
        private int klijentId;
        private string pib;
        private string nazivFirme;
        private string imevlasnika;
        private string prezimevlasnika;
        private string adresa;
        private string email;
        private string telefon;
        private string drzava;

        public KlijentDTO()
        {

        }

        public KlijentDTO(int klijentId, string pib, string nazivFirme, string imeVlasnika, string prezimeVlasnika, string adresa,
            string email, string telefon, string drzava)
        {
            this.klijentId = klijentId;
            this.pib = pib;
            this.nazivFirme = nazivFirme;
            this.imevlasnika = imeVlasnika;
            this.prezimevlasnika = prezimeVlasnika;
            this.adresa = adresa;
            this.email = email;
            this.telefon = telefon;
            this.drzava = drzava;
        }

        [DataMember]
        public int KlijentId
        {
            get { return klijentId; }
            set { klijentId = value; }
        }

        [DataMember]
        public string Pib
        {
            get { return pib; }
            set { pib = value; }
        }

        [DataMember]
        public string NazivFirme
        {
            get { return nazivFirme; }
            set { nazivFirme = value; }
        }

        [DataMember]
        public string Imevlasnika
        {
            get { return imevlasnika; }
            set { imevlasnika = value; }
        }

        [DataMember]
        public string Prezimevlasnika
        {
            get { return prezimevlasnika; }
            set { prezimevlasnika = value; }
        }

        [DataMember]
        public string Adresa
        {
            get { return adresa; }
            set { adresa = value; }
        }

        [DataMember]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        public string Telefon
        {
            get { return telefon; }
            set { telefon = value; }
        }

        [DataMember]
        public string Drzava
        {
            get { return drzava; }
            set { drzava = value; }
        }
    }
}
