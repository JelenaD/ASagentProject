﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer
{
    [DataContract]
    public class TroskoviDTO
    {
        private int id;
        private string racun;
        private DateTime datumprijema;
        private DateTime datumplacanja;
        private double iznos;
        private bool placeno;

        public TroskoviDTO()
        {

        }

        public TroskoviDTO(int id, string racun, DateTime datumprijema, DateTime datumplacanja, double iznos, bool placeno)
        {
            this.id = id;
            this.racun = racun;
            this.datumprijema = datumprijema;
            this.datumplacanja = datumplacanja;
            this.iznos = iznos;
            this.placeno = placeno;
        }
        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string Racun
        {
            get { return racun; }
            set { racun = value; }
        }

        [DataMember]
        public DateTime Datumprijema
        {
            get { return datumprijema; }
            set { datumprijema = value; }
        }

        [DataMember]
        public DateTime Datumplacanja
        {
            get { return datumplacanja; }
            set { datumplacanja = value; }
        }

        [DataMember]
        public double Iznos
        {
            get { return iznos; }
            set { iznos = value; }
        }


        [DataMember]
        public bool Placeno
        {
            get { return placeno; }
            set { placeno = value; }
        }
    }
}
