﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer
{
    public class InvoiceMultipleDTO
    {
        //Ulazne, InoUlazne
        public List<InputInvoiceDTO> InputInvoices { get; set; }
        public List<ForeignInputInvoiceDTO> ForeignInputInvoices { get; set; }
        public List<OutgoingInvoiceDTO> OutInvoices { get; set; }
        public List<ForeignOutgoingInvoiceDTO> OutInoInvoices { get; set; }
    }
}
