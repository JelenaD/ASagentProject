﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class ClientDTO
    {
        private int clientId;
        private string pib;
        private string nameOfClientCompany;
        private string address;
        private string email;
        private string telephone;
        private string state;
        private string zipCode;
        private string city;

        public ClientDTO() { }

        public ClientDTO(int clientId, string pib, string nameOfClientCompany, string address, string email,
            string telephone, string state, string zipCode, string city)
        {
            this.clientId = clientId;
            this.pib = pib;
            this.nameOfClientCompany = nameOfClientCompany;
            this.address = address;
            this.email = email;
            this.telephone = telephone;
            this.state = state;
            this.zipCode = zipCode;
            this.city = city;
        }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int ClientId
        {
            get { return clientId; }
            set { clientId = value; }
        }

        [DataMember]
        [Display(Name = "PIB")]
        [Required(ErrorMessage = "Unesite pib!")]
        public string PIB
        {
            get { return pib; }
            set { pib = value; }
        }

        [DataMember]
        [Display(Name = "ZIP")]
        [Required(ErrorMessage = "Unesite zip code!")]
        public string ZipCode
        {
            get { return zipCode; }
            set { zipCode = value; }
        }

        [DataMember]
        [Display(Name = "Grad")]
        [Required(ErrorMessage = "Unesite grad!")]
        public string City
        {
            get { return city; }
            set { city = value; }
        }

        [DataMember]
        [Display(Name = "KOMPANIJA")]
        [Required(ErrorMessage = "Unesite naziv kompanije!")]
        public string NameOfClientCompany
        {
            get { return nameOfClientCompany; }
            set { nameOfClientCompany = value; }
        }

        [DataMember]
        [Display(Name = "ADRESA")]
        [Required(ErrorMessage = "Unesite adresu!")]
        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        [DataMember]
        [Display(Name = "EMAIL")]
        [Required(ErrorMessage = "Unesite email!")]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        [Display(Name = "TELEFON")]
        [Required(ErrorMessage = "Unesite telefon!")]
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        [DataMember]
        [Display(Name = "DRŽAVA")]
        [Required(ErrorMessage = "Unesite državu!")]
        public string State
        {
            get { return state; }
            set { state = value; }
        }
    }
}
