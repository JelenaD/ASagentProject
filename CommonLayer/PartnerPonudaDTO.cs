﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class PartnerPonudaDTO
    {
        private int id;
        private string nazivponude;
        private DateTime datum;
        private string opis;
        private double cena;
        private string nazivpartnera;
        private int partnerId;

        private  List<PartnerDTO> partneri;

        [Display(Name = "Partner")]
        public int SelectedPartnerId { get; set; }

        public PartnerPonudaDTO()
        {

        }

        public PartnerPonudaDTO(int id,string nazivponude, DateTime datumPonude, string opis, double cena, string nazivpartnera, int partnerId, List<PartnerDTO> partneri)
        {
            
            this.partnerId = partnerId;
            this.nazivponude = nazivponude;
            this.datum = datumPonude;
            this.opis = opis;
            this.cena = cena;
            this.nazivpartnera = nazivpartnera;
            this.partneri = partneri;
           
        }


        public IEnumerable<SelectListItem> PartnerItems
        {
            get { return new SelectList(partneri, "PartnerId", "Naziv"); }
           
        }
        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public int PartnerId
        {
            get { return partnerId; }
            set { partnerId = value; }
        }

        [DataMember]
        public string NazivPonude
        {
            get { return nazivponude; }
            set { nazivponude = value; }
        }

        [DataMember]
        public string Nazivpartnera
        {
            get { return nazivpartnera; }
            set { nazivpartnera = value; }
        }

        [DataMember]
        public string Opis
        {
            get { return opis; }
            set { opis = value; }
        }

        [DataMember]
        public double Cena
        {
            get { return cena; }
            set { cena = value; }
        }

        [DataMember]
        public DateTime Datum
        {
            get { return datum; }
            set { datum = value; }
        }

       
        
    }
}
