﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class AddressBookDTO
    {
        private int addressBookId;
        private string nameOfCompany;
        private string name;
        private string lastName;
        private string email;
        private string telephone;

        public AddressBookDTO() { }

        public AddressBookDTO(int addressBookId, string nameOfCompany, string name, string lastName,
            string email, string telephone)
        {
            this.addressBookId = addressBookId;
            this.nameOfCompany = nameOfCompany;
            this.name = name;
            this.lastName = lastName;
            this.email = email;
            this.telephone = telephone;
        }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int AddressBookId
        {
            get { return addressBookId; }
            set { addressBookId = value; }
        }

        [DataMember]
        [Display(Name = "KOMPANIJA")]
        [Required(ErrorMessage = "Unesite naziv kompanije!")]
        public string NameOfCompany
        {
            get { return nameOfCompany; }
            set { nameOfCompany = value; }
        }

        [DataMember]
        [Display(Name = "IME")]
        [Required(ErrorMessage = "Unesite ime!")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        [DataMember]
        [Display(Name = "PREZIME")]
        [Required(ErrorMessage = "Unesite prezime!")]
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        [DataMember]
        [Display(Name = "E-MAIL ADRESA")]
        [Required(ErrorMessage = "Unesite e-mail adresu!")]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        [Display(Name = "TELEFON")]
        [Required(ErrorMessage = "Unesite broj telefona!")]
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }
    }
}
