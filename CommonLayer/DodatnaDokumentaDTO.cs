﻿using System.Runtime.Serialization;

namespace CommonLayer
{
    [DataContract]
    public class DodatnaDokumentaDTO
    {
        private int id;
        private string naziv;
        private byte[] pdf;
        private string predmet;

        public DodatnaDokumentaDTO()
        {

        }

        public DodatnaDokumentaDTO(int id, string naziv, byte[] pdf, string predmet)
        {
            this.id = id;
            this.naziv = naziv;
            this.pdf = pdf;
            this.predmet = predmet;
        }

        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string Predmet
        {
            get { return predmet; }
            set { predmet = value; }
        }

        [DataMember]
        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; }
        }

        [DataMember]
        public byte[] Pdf
        {
            get { return pdf; }
            set { pdf = value; }
        }
    }
}
