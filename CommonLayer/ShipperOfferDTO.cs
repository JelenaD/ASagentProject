﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class ShipperOfferDTO
    {
        private int shipperOfferId;
        private string shipperOfferName;
        private DateTime date;
        private string description;
        private double price;
        private string shipperName;
        private int shipperId;
        private string clientOfferName;
        private string email;
        private string distance;
        private string telephone;
        private int clientOfferId;
        private List<ShipperDTO> shipperCollection;
        private List<ClientOffersDTO> clientOffersDTO;

        public ShipperOfferDTO() { }

        public ShipperOfferDTO(int shipperOfferId, string shipperOfferName, DateTime date, string description,
            double price, string shipperName, string clientOfferName, string telephone, string email, string distance)
        {
            this.shipperOfferId = shipperOfferId;
            this.shipperOfferName = shipperOfferName;
            this.date = date;
            this.description = description;
            this.price = price;
            this.email = email;
            this.telephone = telephone;
            this.distance = distance;
            this.clientOfferName = clientOfferName;
            this.shipperName = shipperName;
        }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int ShipperOfferId
        {
            get { return shipperOfferId; }
            set { shipperOfferId = value; }
        }

        [DataMember]
        [Display(Name = "PONUDA VOZARA")]
        [Required(ErrorMessage = "Unesite ponudu vozara!")]
        public string ShipperOfferName
        {
            get { return shipperOfferName; }
            set { shipperOfferName = value; }
        }

        [DataMember]
        [Display(Name = "RELACIJA")]
        public string Distance
        {
            get { return distance; }
            set { distance = value; }
        }

        [DataMember]
        [Display(Name = "E-MAIL")]   
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        [Display(Name = "TELEFON")]
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        [DataMember]
        [Display(Name = "KLIJENT PONUDA")]
        public string ClientOfferName
        {
            get { return clientOfferName; }
            set { clientOfferName = value; }
        }

        [DataMember]
        [Display(Name = "DATUM")]
        [Required(ErrorMessage = "Unesite datum!")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }


        [DataMember]
        [Display(Name = "OPIS")]
        [Required(ErrorMessage = "Unesite opis!")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [DataMember]
        [Display(Name = "CENA")]
        [Required(ErrorMessage = "Unesite cenu!")]
        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        [DataMember]
        [Display(Name = "VOZAR")]
        [Required(ErrorMessage = "Unesite vozara!")]
        public string ShipperName
        {
            get { return shipperName; }
            set { shipperName = value; }
        }

        [DataMember]
        public int ClientOfferId
        {
            get { return clientOfferId; }
            set { clientOfferId = value; }
        }

        [DataMember]
        public List<ClientOffersDTO> ClientOffersDTO
        {
            get { return clientOffersDTO; }
            set { clientOffersDTO = value; }
        }

        [DataMember]
        public int ShipperId
        {
            get { return shipperId; }
            set { shipperId = value; }
        }

        [DataMember]
        public List<ShipperDTO> ShipperCollection
        {
            get { return shipperCollection; }
            set { shipperCollection = value; }
        }
    }
}
