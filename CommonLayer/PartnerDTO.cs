﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer
{
    [DataContract]
    public class PartnerDTO
    {
        private int partnerId;
        private string pib;
        private string naziv;
        private string ime;
        private string prezime;
        private string adresa;
        private string email;
        private string telefon;
        private string drzava;
        private List<PartnerPonudaDTO> ponude;

        public PartnerDTO()
        {

        }

        public PartnerDTO(int partnerId, string pib, string naziv, string ime, string prezime, string adresa, 
            string telefon, string email, string drzava)
        {
            this.partnerId = partnerId;
            this.pib = pib;
            this.naziv = naziv;
            this.ime = ime;
            this.prezime = prezime;
            this.adresa = adresa;
            this.telefon = telefon;
            this.email = email;
            this.drzava = drzava;
        }
        [DataMember]
        public int PartnerId
        {
            get { return partnerId; }
            set { partnerId = value; }
        }

        [DataMember]
        public string Pib
        {
            get { return pib; }
            set { pib = value; }
        }

        [DataMember]
        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; }
        }

        [DataMember]
        public string Ime
        {
            get { return ime; }
            set { ime = value; }
        }

        [DataMember]
        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; }
        }

        [DataMember]
        public string Adresa
        {
            get { return adresa; }
            set { adresa = value; }
        }

        [DataMember]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        public string Telefon
        {
            get { return telefon; }
            set { telefon = value; }
        }

        [DataMember]
        public string Drzava
        {
            get { return drzava; }
            set { drzava = value; }
        }

        [DataMember]
        public List<PartnerPonudaDTO> Ponude
        {
            get { return ponude; }
            set { ponude = value; }
        }
    }
}
