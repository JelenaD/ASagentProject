﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class ASABookingNumberDTO
    {
        private int asaBookingNumberId;
        private string asaNumber;
        private string distance;
        private string container;

        public ASABookingNumberDTO() { }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int AsaBookingNumberId
        {
            get { return asaBookingNumberId; }
            set { asaBookingNumberId = value; }
        }

        [DataMember]
        [Display(Name = "ASA BROJ")]
        [Required(ErrorMessage = "Unesite asa broj!")]
        public string AsaNumber
        {
            get { return asaNumber; }
            set { asaNumber = value; }
        }

        [DataMember]
        [Display(Name = "RELACIJA")]
        [Required(ErrorMessage = "Unesite relaciju!")]
        public string Distance
        {
            get { return distance; }
            set { distance = value; }
        }

        [DataMember]
        [Display(Name = "KONTEJNER")]
        [Required(ErrorMessage = "Unesite kontejner!")]
        public string Container
        {
            get { return container; }
            set { container = value; }
        }
    }
}
