﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class StandardBookingNumberDTO
    {
        private int standardBookingNumberId;
        private string standardNumber;
        private string distance;
        private string container;

        public StandardBookingNumberDTO() { }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int StandardBookingNumberId
        {
            get { return standardBookingNumberId; }
            set { standardBookingNumberId = value; }
        }

        [DataMember]
        [Display(Name = "OBIČAN BROJ")]
        [Required(ErrorMessage = "Unesite običan broj!")]
        public string StandardNumber
        {
            get { return standardNumber; }
            set { standardNumber = value; }
        }

        [DataMember]
        [Display(Name = "RELACIJA")]
        [Required(ErrorMessage = "Unesite relaciju!")]
        public string Distance
        {
            get { return distance; }
            set { distance = value; }
        }

        [DataMember]
        [Display(Name = "KONTEJNER")]
        [Required(ErrorMessage = "Unesite kontejner!")]
        public string Container
        {
            get { return container; }
            set { container = value; }
        }
    }
}
