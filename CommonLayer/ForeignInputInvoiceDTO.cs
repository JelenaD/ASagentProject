﻿using CommonLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class ForeignInputInvoiceDTO
    {
        private int serialNumberOfForeignInputInvoice;
        private DateTime dateOfReceiptInvoice;
        private string numberOfForeignInputInvoice;
        private int shipperId;
        private string shipperName;
        private string shipperPlace;
        private double amountInEUR;
        private DateTime valueDate;
        private DateTime paymentDate;
        private bool? paid;
        private string comment;
        private int bookId;
        private string objectBook;
        private DateTime? dateFrom;
        private DateTime? dateTo;
        private List<BookDTO> bookCollection;
        private List<ShipperDTO> shipperCollection;

        public ForeignInputInvoiceDTO() { }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int SerialNumberOfForeignInputInvoice
        {
            get { return serialNumberOfForeignInputInvoice; }
            set { serialNumberOfForeignInputInvoice = value; }
        }

        [DataMember]
        [Display(Name = "DATUM PRIJEMA")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Unesite datum prijema!")]
        public DateTime DateOfReceiptInvoice
        {
            get { return dateOfReceiptInvoice; }
            set { dateOfReceiptInvoice = value; }
        }


        [DataMember]
        [Display(Name = "BROJ ULAZNE FAKTURE")]
        [Required(ErrorMessage = "Unesite broj ulazne fakture!")]
        public string NumberOfForeignInputInvoice
        {
            get { return numberOfForeignInputInvoice; }
            set { numberOfForeignInputInvoice = value; }
        }

        [DataMember]
        [Display(Name = "VOZAR")]
        public int ShipperId
        {
            get { return shipperId; }
            set { shipperId = value; }
        }


        [DataMember]
        [Display(Name = "VOZAR")]
        [Required(ErrorMessage = "Unesite vozara!")]
        public string ShipperName
        {
            get { return shipperName; }
            set { shipperName = value; }
        }

        [DataMember]
        [Display(Name = "MESTO VOZARA")]
        [Required(ErrorMessage = "Unesite mesto vozara!")]
        public string ShipperPlace
        {
            get { return shipperPlace; }
            set { shipperPlace = value; }
        }

        [DataMember]
        [Display(Name = "EUR/USD")]
        [Required(ErrorMessage = "Unesite iznos u EUR/USD!")]
        public double AmountInEUR
        {
            get { return amountInEUR; }
            set { amountInEUR = value; }
        }

        [DataMember]
        [Display(Name = "DATUM VALUTE")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Unesite datum valute!")]
        public DateTime ValueDate
        {
            get { return valueDate; }
            set { valueDate = value; }
        }

        [DataMember]
        [Display(Name = "DATUM PLAĆANJA")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Unesite datum plaćanja!")]
        public DateTime PaymentDate
        {
            get { return paymentDate; }
            set { paymentDate = value; }
        }

        [DataMember]
        [Display(Name = "PLAĆENO")]
        [Required(ErrorMessage = "Označite da li je plaćeno!")]
        public bool? Paid
        {
            get { return paid; }
            set { paid = value; }
        }

        [DataMember]
        [Display(Name = "KOMENTAR")]
        [Required(ErrorMessage = "Unesite komentar!")]
        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        [DataMember]
        [Display(Name = "KNJIGA NALOGA")]
        public int BookId
        {
            get { return bookId; }
            set { bookId = value; }
        }

        [DataMember]
        [Display(Name = "PREDMET")]
        [Required(ErrorMessage = "Unesite predmet!")]
        public string ObjectBook
        {
            get { return objectBook; }
            set { objectBook = value; }
        }

        [DataMember]
        [Display(Name = "KLIJENT")]
        public List<ShipperDTO> ShipperCollection
        {
            get { return shipperCollection; }
            set { shipperCollection = value; }
        }

        [DataMember]
        [Display(Name = "KNJIGA NALOGA")]
        public List<BookDTO> BookCollection
        {
            get { return bookCollection; }
            set { bookCollection = value; }
        }

        [DataMember]
        public DateTime? DateTo
        {
            get { return dateTo; }
            set { dateTo = value; }
        }

        [DataMember]
        public DateTime? DateFrom
        {
            get { return dateFrom; }
            set { dateFrom = value; }
        }
    }
}
