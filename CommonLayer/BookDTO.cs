﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class BookDTO
    {
        private int bookId;
        private string objectBook;
        private string placeOfLoading;

        private DateTime dateOfLoading;
        private string placeOfUnloading;
        private string goods;
        private string customer;
        private string quantityOfGoods;
        private string containerTruck;
        private double? difference;
        private string bookingNumber;
        private string clientName;
        private string inputInvoice;
        private double? priceOfInputInvoice;
        private double? asaPrice;
        private string outgoingInvoice;
        private int standardBookingNumberId;
        private int asaBookingNumberId;
        private List<ASABookingNumberDTO> asaCollection;
        private List<StandardBookingNumberDTO> standardCollection;


        public BookDTO() { }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int BookId
        {
            get { return bookId; }
            set { bookId = value; }
        }

        [DataMember]
        [Display(Name = "PREDMET")]
        [Required(ErrorMessage = "Unesite predmet!")]
        public string ObjectBook
        {
            get { return objectBook; }
            set { objectBook = value; }
        }

        [DataMember]
        [Display(Name = "MESTO UTOVARA")]
        public string PlaceOfLoading
        {
            get { return placeOfLoading; }
            set { placeOfLoading = value; }
        }

        [DataMember]
        [Display(Name = "DATUM UTOVARA")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateOfLoading
        {
            get { return dateOfLoading; }
            set { dateOfLoading = value; }
        }

        [DataMember]
        [Display(Name = "MESTO ISTOVARA")]
        public string PlaceOfUnloading
        {
            get { return placeOfUnloading; }
            set { placeOfUnloading = value; }
        }

        [DataMember]
        [Display(Name = "ROBA")]
        public string Goods
        {
            get { return goods; }
            set { goods = value; }
        }

        [DataMember]
        [Display(Name = "KOMITENT")]
        public string Customer
        {
            get { return customer; }
            set { customer = value; }
        } 

        [DataMember]
        [Display(Name = "KOLIČINA")]
        public string QuantityOfGoods
        {
            get { return quantityOfGoods; }
            set { quantityOfGoods = value; }
        }

        [DataMember]
        [Display(Name = "KONT/KAM")]
        public string ContainerTruck
        {
            get { return containerTruck; }
            set { containerTruck = value; }
        }

        [DataMember]
        [Display(Name = "RAZLIKA")]
        public double? Difference
        {
            get { return difference; }
            set { difference = value; }
        }


        [DataMember]
        [Display(Name = "BOOKING BROJ")]
        public string BookingNumber
        {
            get { return bookingNumber; }
            set { bookingNumber = value; }
        }

        [DataMember]
        [Display(Name = "KOMITENT")]
        public string ClientName
        {
            get { return clientName; }
            set { clientName = value; }
        }

        [DataMember]
        [Display(Name = "UF")]
        public string InputInvoice
        {
            get { return inputInvoice; }
            set { inputInvoice = value; }
        }

        [DataMember]
        [Display(Name = "CENA UF")]
        public double? PriceOfInputInvoice
        {
            get { return priceOfInputInvoice; }
            set { priceOfInputInvoice = value; }
        }

        [DataMember]
        [Display(Name = "ASA CENA")]
        public double? AsaPrice
        {
            get { return asaPrice; }
            set { asaPrice = value; }
        }

        [DataMember]
        [Display(Name = "IF")]
        public string OutgoingInvoice
        {
            get { return outgoingInvoice; }
            set { outgoingInvoice = value; }
        }

        [DataMember]
        public int StandardBookingNumberId
        {
            get { return standardBookingNumberId; }
            set { standardBookingNumberId = value; }
        }

        [DataMember]
        public int AsaBookingNumberId
        {
            get { return asaBookingNumberId; }
            set { asaBookingNumberId = value; }
        }

        [DataMember]
        public List<ASABookingNumberDTO> AsaCollection
        {
            get { return asaCollection; }
            set { asaCollection = value; }
        }
        [DataMember]
        public List<StandardBookingNumberDTO> StandardCollection
        {
            get { return standardCollection; }
            set { standardCollection = value; }
        }
    }
}
