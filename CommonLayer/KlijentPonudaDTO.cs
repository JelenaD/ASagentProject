﻿using System;
using System.Runtime.Serialization;

namespace CommonLayer
{
    [DataContract]
    public class KlijentPonudaDTO
    {
        private int id;
        private string nazivponude;
        private string opis;
        private byte[] pdf;
        private double asa;
        private bool prihvaceno;
        private DateTime datum;
        private string nazivponudepartnera;
        private string nazivklijenta;
        private string predmet;

        public KlijentPonudaDTO()
        {

        }

        public KlijentPonudaDTO(int id, string nazivponude, string opis, byte[] pdf, double asa, bool prihvaceno,DateTime datum,string nazivponudepartnera,
            string nazivklijenta, string predmet)
        {
            this.id = id;
            this.nazivponude = nazivponude;
            this.opis = opis;
            this.pdf = pdf;
            this.asa = asa;
            this.prihvaceno = prihvaceno;
            this.datum = datum;
            this.nazivponudepartnera = nazivponudepartnera;
            this.nazivklijenta = nazivklijenta;
            this.predmet = predmet;
        }

        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        [DataMember]
        public DateTime Datum
        {
            get { return datum; }
            set { datum = value; }
        }

        [DataMember]
        public string Nazivponude
        {
            get { return nazivponude; }
            set { nazivponude = value; }
        }

        [DataMember]
        public string Opis
        {
            get { return opis; }
            set { opis = value; }
        }

        [DataMember]
        public byte[] Pdf
        {
            get { return pdf; }
            set { pdf = value; }
        }

        [DataMember]
        public double ASA
        {
            get { return asa; }
            set { asa = value; }
        }

        [DataMember]
        public bool Prihvaceno
        {
            get { return prihvaceno; }
            set { prihvaceno = value; }
        }

        [DataMember]
        public string Nazivponudepartnera
        {
            get { return nazivponudepartnera; }
            set { nazivponudepartnera = value; }
        }

        [DataMember]
        public string Nazivklijenta
        {
            get { return nazivklijenta; }
            set { nazivklijenta = value; }
        }

        [DataMember]
        public string Predmet
        {
            get { return predmet; }
            set { predmet = value; }
        }

    }
}
