﻿using DataLayer;
using System;
using System.Runtime.Serialization;

namespace CommonLayer
{
    [DataContract]
    public class IzlaznaFakturaDTO
    {
        private int id;
        private string nazivFakture;
        private DateTime datumfakturisanja;
        private DateTime datumprometa;
        private double iznos;
        private Valuta valutaplacanja;
        private byte[] pdf;
        private string brojzakljucka;
        private string roba;
        private string relacija;
        private int valuta;
        private bool placeno;
        private string predmet;
        private string nazivklijenta;

        public IzlaznaFakturaDTO()
        {

        }

        public IzlaznaFakturaDTO(int id, string nazivFakture, DateTime datumFakturisanja, DateTime datumPrometa, double iznos, Valuta valutaPlacanja, byte[] pdf,
            string brojZakljucka, string roba, string relacija, int valuta, bool placeno, string predmet, string nazivKlijenta)
        {
            this.id = id;
            this.nazivFakture = nazivFakture;
            this.datumfakturisanja = datumfakturisanja;
            this.datumprometa = datumprometa;
            this.iznos = iznos;
            this.valutaplacanja = valutaPlacanja;
            this.pdf = pdf;
            this.brojzakljucka = brojzakljucka;
            this.roba = roba;
            this.relacija = relacija;
            this.valuta = valuta;
            this.placeno = placeno;
            this.predmet = predmet;
            this.nazivklijenta = nazivklijenta;
        }
        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        [DataMember]
        public string NazivFakture
        {
            get { return nazivFakture; }
            set { nazivFakture = value; }
        }

        [DataMember]
        public DateTime Datumfakturisanja
        {
            get { return datumfakturisanja; }
            set { datumfakturisanja = value; }
        }

        [DataMember]
        public DateTime Datumprometa
        {
            get { return datumprometa; }
            set { datumprometa = value; }
        }

        [DataMember]
        public double Iznos
        {
            get { return iznos; }
            set { iznos = value; }
        }

        [DataMember]
        public Valuta Valutaplacanja
        {
            get { return valutaplacanja; }
            set { valutaplacanja = value; }
        }

        [DataMember]
        public byte[] Pdf
        {
            get { return pdf; }
            set { pdf = value; }
        }

        [DataMember]
        public string Brojzakljucka
        {
            get { return brojzakljucka; }
            set { brojzakljucka = value; }
        }

        [DataMember]
        public string Roba
        {
            get { return roba; }
            set { roba = value; }
        }

        [DataMember]
        public string Relacija
        {
            get { return relacija; }
            set { relacija = value; }
        }

        [DataMember]
        public int Valuta
        {
            get { return valuta; }
            set { valuta = value; }
        }

        [DataMember]
        public bool Placeno
        {
            get { return placeno; }
            set { placeno = value; }
        }

        [DataMember]
        public string Predmet
        {
            get { return predmet; }
            set { predmet = value; }
        }

        [DataMember]
        public string Nazivklijenta
        {
            get { return nazivklijenta; }
            set { nazivklijenta = value; }
        }
    }
}
