﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer
{
    [DataContract]
    public class KnjigaNalogaDTO
    {
        private int id;
        private string predmet;
        private string mestoutovara;
        private DateTime datumutovara;
        private string mestoistovara;
        private string vrstarobe;
        private string kolicinarobe;
        private string kontejnerkamion;
        private double razlika;
        private int bookingnumber;
        private string komitent;
        private string prevoznikIme;
        private string prevoznikPrezime;
        private string ulaznaFaktura;
        private double cenaFakture;
        private double asaCena;
        private string izlaznaFaktura;


        public KnjigaNalogaDTO()
        {

        }

        public KnjigaNalogaDTO(string predmet, string kolicinarobe, string vrstarobe, string mestoutovara, DateTime datumutovara, string mestoistovara,
            string kontejnerkamion, double razlika, int bookingnumber)
        {
            this.predmet = predmet;
            this.mestoutovara = mestoutovara;
            this.datumutovara = datumutovara;
            this.mestoistovara = mestoistovara;
            this.kontejnerkamion = kontejnerkamion;
            this.razlika = razlika;
            this.kolicinarobe = kolicinarobe;
            this.vrstarobe = vrstarobe;
            this.bookingnumber = bookingnumber;
        }

        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Predmet
        {
            get { return predmet; }
            set { predmet = value; }
        }

        public string Vrstarobe
        {
            get { return vrstarobe; }
            set { vrstarobe = value; }
        }

        public string Kolicinarobe
        {
            get { return kolicinarobe; }
            set { kolicinarobe = value; }
        }

        public string Mestoutovara
        {
            get { return mestoutovara; }
            set { mestoutovara = value; }
        }

        public DateTime Datumutovara
        {
            get { return datumutovara; }
            set { datumutovara = value; }
        }

        public string Mestoistovara
        {
            get { return mestoistovara; }
            set { mestoistovara = value; }
        }
        public string Kontejnerkamion
        {
            get { return kontejnerkamion; }
            set { kontejnerkamion = value; }
        }

        public double Razlika
        {
            get { return razlika; }
            set { razlika = value; }
        }

        public int Bookingnumber
        {
            get { return bookingnumber; }
            set { bookingnumber = value; }
        }


    }
}
