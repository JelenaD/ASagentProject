﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class AdditionalDocumentationDTO
    {
        private int additionalDocumentationId;
        private string name;
        private string fileName;
        private string contentType;
        private byte[] content;
        private string objectBook;
        private int bookId;
        private List<BookDTO> bookCollection;

        public AdditionalDocumentationDTO() { }

        public AdditionalDocumentationDTO(int additionalDocumentationId, string name, string objectBook, byte[] content)
        {
            this.additionalDocumentationId = additionalDocumentationId;
            this.name = name;
            this.objectBook = objectBook;
            this.content = content;
        }

        public AdditionalDocumentationDTO(string name, string objectBook, int bookId,
            List<BookDTO> bookCollection)
        {
            this.name = name;
            this.objectBook = objectBook;
            this.bookId = bookId;
            this.bookCollection = bookCollection;
        }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int AdditionalDocumentationId
        {
            get { return additionalDocumentationId; }
            set { additionalDocumentationId = value; }
        }

        [DataMember]
        public List<BookDTO> BookCollection
        {
            get { return bookCollection; }
            set { bookCollection = value; }
        }

        [DataMember]
        public int BookId
        {
            get { return bookId; }
            set { bookId = value; }
        }



        [DataMember]
        [Display(Name = "PREDMET")]
        [Required(ErrorMessage ="Unesite predmet.")]
        public string ObjectBook
        {
            get { return objectBook; }
            set { objectBook = value; }
        }

        [DataMember]
        [Display(Name = "NAZIV DOKUMENTA")]
        [Required(ErrorMessage = "Unesite naziv dokumenta.")]

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [DataMember]
        public string ContentType
        {
            get { return contentType; }
            set { contentType = value; }
        }

        [DataMember]
        [Display(Name = "FILE NAME")]
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        [DataMember]
        public byte[] Content
        {
            get { return content; }
            set { content = value; }
        }
    }
}
