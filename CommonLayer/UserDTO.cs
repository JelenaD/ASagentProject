﻿using DataLayer;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class UserDTO
    {
        private int userId;
        private string name;
        private string lastName;
        private string address;
        private string email;
        private string telephone;
        private string username;
        private string password;
        private UserRole userRole;

        public UserDTO() { }

        public UserDTO(int userId, string name, string lastName, string address, string email, string telephone,
            string username, string password, UserRole userRole)
        {
            this.userId = userId;
            this.name = name;
            this.lastName = lastName;
            this.address = address;
            this.email = email;
            this.telephone = telephone;
            this.username = username;
            this.password = password;
            this.userRole = userRole;
        }


        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }

        [DataMember]
        [Display(Name = "IME")]
        [Required(ErrorMessage = "Unesite ime.")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [DataMember]
        [Display(Name = "PREZIME")]
        [Required(ErrorMessage = "Unesite prezime.")]
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        [DataMember]
        [Display(Name = "ADRESA")]
        [Required(ErrorMessage = "Unesite adresu.")]
        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        [DataMember]
        [Display(Name = "EMAIL")]
        [Required(ErrorMessage = "Unesite email.")]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        [Display(Name = "TELEFON")]
        [Required(ErrorMessage = "Unesite broj telefona.")]
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        [DataMember]
        [Display(Name = "KORISNIČKO IME")]
        [Required(ErrorMessage = "Unesite korisničko ime.")]
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        [DataMember]
        [Display(Name = "LOZINKA")]
        [Required(ErrorMessage = "Unesite lozinku.")]
        [DataType(DataType.Password)]
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        [DataMember]
        [Display(Name = "ULOGA")]
        public UserRole UserRole
        {
            get { return userRole; }
            set { userRole = value; }
        }

        public string LoginErrorMessage { get; set; }
    }
}
