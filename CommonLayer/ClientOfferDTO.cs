﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace CommonLayer
{
    [DataContract]
    public class ClientOffersDTO
    {
        private int clientOfferId;
        private string offersName;
        private string description;
        private bool accepted;
        private DateTime date;
        private double asa;
        private string shipperOfferName;
        private string clientName;
        private string objectBook;
        private int bookId;
        private int shipperOfferId;
        private int clientId;
        private List<ClientDTO> clientCollection;
        private List<BookDTO> bookCollection;
        private List<ShipperOfferDTO> shipperOfferCollection;

        public ClientOffersDTO() { }

        [DataMember]
        [HiddenInput(DisplayValue = false)]
        public int ClientOfferId
        {
            get { return clientOfferId; }
            set { clientOfferId = value; }
        }

        [DataMember]
        [Display(Name = "PONUDA")]
        [Required(ErrorMessage = "Unesite naziv ponude!")]
        public string OffersName
        {
            get { return offersName; }
            set { offersName = value; }
        }

        [DataMember]
        [Display(Name = "OPIS")]
        [Required(ErrorMessage = "Unesite opis!")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [DataMember]
        [Display(Name = "PRIHVAĆENO")]
        [Required(ErrorMessage = "Popunite polje!")]
        public bool Accepted
        {
            get { return accepted; }
            set { accepted = value; }
        }

        [DataMember]
        [Display(Name = "DATUM")]
        [Required(ErrorMessage = "Unesite datum!")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        [DataMember]
        [Display(Name = "ASA")]
        [Required(ErrorMessage = "Unesite asa cenu!")]
        public double ASA
        {
            get { return asa; }
            set { asa = value; }
        }

        [DataMember]
        [Display(Name = "PONUDA VOZARA")]
       
        public string ShipperOfferName
        {
            get { return shipperOfferName; }
            set { shipperOfferName = value; }
        }

        [DataMember]
        [Display(Name = "KLIJENT")]
        [Required(ErrorMessage = "Unesite klijenta!")]
        public string ClientName
        {
            get { return clientName; }
            set { clientName = value; }
        }

        [DataMember]
        [Display(Name = "PREDMET")]
        [Required(ErrorMessage = "Unesite predmet!")]
        public string ObjectBook
        {
            get { return objectBook; }
            set { objectBook = value; }
        }

        [DataMember]
        [Display(Name = "KNJIGA NALOGA")]
        public int BookId
        {
            get { return bookId; }
            set { bookId = value; }
        }

        [DataMember]
        [Display(Name = "PONUDA VOZARA")]
        public int ShipperOfferId
        {
            get { return shipperOfferId; }
            set { shipperOfferId = value; }
        }

        [DataMember]
        [Display(Name = "KLIJENT")]
        public int ClientId
        {
            get { return clientId; }
            set { clientId = value; }
        }

        [DataMember]
        [Display(Name = "KLIJENTI")]
        public List<ClientDTO> ClientCollection
        {
            get { return clientCollection; }
            set { clientCollection = value; }
        }

        [DataMember]
        [Display(Name = "KNJIGA NALOGA")]
        public List<BookDTO> BookCollection
        {
            get { return bookCollection; }
            set { bookCollection = value; }
        }

        [DataMember]
        [Display(Name = "PONUDA VOZARA")]
        public List<ShipperOfferDTO> ShipperOfferCollection
        {
            get { return shipperOfferCollection; }
            set { shipperOfferCollection = value; }
        }

    }
}
