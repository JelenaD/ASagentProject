﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer
{
    [DataContract]
    public class KorisnikDTO
    {
        private int korisnikId;
        private string ime;
        private string prezime;
        private string adresa;
        private string email;
        private string telefon;
        private string korisnickoime;
        private string lozinka;
        private UlogaKorisnika uloga;

        public KorisnikDTO()
        {

        }

        public KorisnikDTO(int korisnikId, string ime, string prezime, string adresa, string email, string telefon,
            string korisnickoime, string lozinka, UlogaKorisnika uloga)
        {
            this.korisnikId = korisnikId;
            this.ime = ime;
            this.prezime = prezime;
            this.adresa = adresa;
            this.email = email;
            this.telefon = telefon;
            this.korisnickoime = korisnickoime;
            this.lozinka = lozinka;
            this.uloga = uloga;
        }


        [DataMember]
        [Required(ErrorMessage = "Unesite ime")]
        public string Ime
        {
            get { return ime; }
            set { ime = value; }
        }

        [DataMember]
        public int KorisnikId
        {
            get { return korisnikId; }
            set { korisnikId = value; }
        }

        [DataMember]
        [Required(ErrorMessage ="Unesite prezime")]
        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; }
        }

        [DataMember]
        [Required(ErrorMessage ="Unesite adresu")]
        public string Adresa
        {
            get { return adresa; }
            set { adresa = value; }
        }

        [DataMember]
        [Display(Name ="Email adresa")]
        [EmailAddress(ErrorMessage ="Nije validna email adresa")]
        [Required(ErrorMessage = "Unesite email adresu")]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        [Required(ErrorMessage ="Unesite telefon")]
        public string Telefon
        {
            get { return telefon; }
            set { telefon = value; }
        }

        [DataMember]
        [Required(ErrorMessage ="Unesite korisnicko ime")]
        [Display(Name ="Korisnicko ime")]
        public string Korisnickoime
        {
            get { return korisnickoime; }
            set { korisnickoime = value; }
        }

        [DataMember]
        [Required(ErrorMessage ="Unesite lozinku")]
        [DataType(DataType.Password)]
        public string Lozinka
        {
            get { return lozinka; }
            set { lozinka = value; }
        }

        [DataMember]
        [Required(ErrorMessage ="Odaberite ulogu")]
        public UlogaKorisnika Uloga
        {
            get { return uloga; }
            set { uloga = value; }
        }

        public string LoginErrorMessage { get; set; }

    }

   
}
