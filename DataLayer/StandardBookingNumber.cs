﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class StandardBookingNumber
    {
        [Key]
        public int StandardBookingNumberId { get; set; }
        public int StandardNumber { get; set; }
        public string Distance { get; set; }
        public string Container { get; set; }
    }
}
