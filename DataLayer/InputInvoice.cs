﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class InputInvoice
    {
        [Key]
        public int SerialNumberOfInputInvoice { get; set; }
        public DateTime DateOfReceiptInvoice { get; set; }

        //dodaj u dto
        [Required]
        public int ShipperId { get; set; }
        [ForeignKey("ShipperId")]
        public Shipper Shipper { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        [Index("Unique_username", IsUnique = true)]
        public string NumberOfInputInvoice { get; set; }
        public double? AmountInDIN { get; set; }
        public double? AmountInEUR { get; set; }
        public DateTime ValueDate { get; set; }
        public DateTime PaymentDate { get; set; }
        public bool? Paid { get; set; }
        public string Comment { get; set; }

        [Required]
        public int BookId { get; set; }
        [ForeignKey("BookId")]
        public Book Book { get; set; }
    }
}
