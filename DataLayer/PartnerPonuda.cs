﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class PartnerPonuda
    {
        [Key]
        public int Id { get; set; }

        public string NazivPonude { get; set; }

        public DateTime Datum { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(80)]
        public string Opis { get; set; }

        public double Cena { get; set; }

        [Required]
        public int PartnerId { get; set; }
        [ForeignKey("PartnerId")]
        public Partner Partner { get; set; }
    }
}
