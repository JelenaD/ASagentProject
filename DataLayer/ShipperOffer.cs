﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class ShipperOffer
    {
        [Key]
        public int ShipperOfferId { get; set; }

        public string ShipperOfferName { get; set; }

        public DateTime Date { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(80)]
        public string Description { get; set; }

        public string Distance { get; set; }

        public string Telephone { get; set; }

        public string Email { get; set; }

        public double Price { get; set; }

        [Required]
        public int ShipperId { get; set; }
        [ForeignKey("ShipperId")]
        public Shipper Shipper { get; set; }

        [Required]
        public int ClientOffId{ get; set; }
        [ForeignKey("ClientOffId")]
        public ClientOffer ClientOffer { get; set; }
    }
}
