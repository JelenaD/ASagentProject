﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class ForeignOutgoingInvoice
    {
        [Key]
        //RedniBrojInoFakture  DatumPrijema
        public int ForeignOutgoingInvoiceId { get; set; }
        public DateTime DateOfReceipt { get; set; }

        [Required]
        //KlijentId  Klijent
        public int ClientId { get; set; }
        [ForeignKey("ClientId")]
        public Client Client { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Index("Unique_username", 1, IsUnique = true)]
        //EURUSD  BrojUlazneFakture
        public string NumberOfForeignOutgoingInvoice { get; set; }
        public double? AmountInEUR { get; set; }


        public DateTime ValueDate { get; set; }
        public DateTime PaymentDate { get; set; }
        public bool Paid { get; set; }
        public string Comment { get; set; }

        [Required]
        public int BookId { get; set; }
        [ForeignKey("BookId")]
        public Book Book { get; set; }
    }
}
