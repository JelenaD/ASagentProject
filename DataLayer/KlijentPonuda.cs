﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class KlijentPonuda
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string NazivPonude { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string Opis { get; set; }

        public byte[] Pdf { get; set; }

        public double ASA { get; set; }
        public bool Prihvaceno { get; set; }

        public DateTime Datum;

        [Required]
        public int PonudaPartneraId { get; set; }
        [ForeignKey("PonudaPartneraId")]
        public PartnerPonuda PartnerPonude { get; set; }

        [Required]
        public int KlijentId { get; set; }
        [ForeignKey("KlijentId")]
        public Klijent Klijent { get; set; }

        [Required]
        public int KnjigaNalogaId { get; set; }
        [ForeignKey("KnjigaNalogaId")]
        public KnjigaNaloga KnjigaNaloga { get; set; }
    }
}
