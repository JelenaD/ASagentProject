﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class ClientOffer
    {
        [Key]
        //Id
        public int ClientOfferId { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        //NazivPonude
        public string OffersName { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        //Opis
        public string Descritpion { get; set; }

        public string FileName { get; set; }

        public string ContetnType { get; set; }

        public byte[] Content { get; set; }

        public double ASA { get; set; }
        //Prihvaceno
        public bool Accepted { get; set; }
        //Datum
        public DateTime Date;

       

        [Required]
        //KlijentId
        public int ClientId { get; set; }
        [ForeignKey("ClientId")]
        //Klijent
        public Client Client { get; set; }

        [Required]
        public int BookId { get; set; }
        [ForeignKey("BookId")]
        public Book Book { get; set; }
    }
}
