﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class Cost
    {
        [Key]
        //Id
        public int CostId { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        //Racun
        public string AccountNumber { get; set; }
        //DatumPrijema,DatumPlacanja,Iznos,Placeno
        public DateTime DateOfReceipt { get; set; }
        public DateTime PaymentDate { get; set; }
        public double Amount { get; set; }

        public bool Paid { get; set; }
    }
}
