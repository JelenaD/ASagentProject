﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class Shipper
    {
        [Key]
        public int ShipperId { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        [Index("Unique_password", IsUnique = true)]
        public string PIB { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string CompanyName { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string Address { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string ZipCode { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string City { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string Email { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Telephone { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string State { get; set; }
    }
}
