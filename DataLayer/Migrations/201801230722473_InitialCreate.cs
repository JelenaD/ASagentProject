namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DodatnaDokumentas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naziv = c.String(maxLength: 30, unicode: false),
                        Pdf = c.Binary(),
                        KnjigaNalogaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.KnjigaNalogas", t => t.KnjigaNalogaId, cascadeDelete: true)
                .Index(t => t.KnjigaNalogaId);
            
            CreateTable(
                "dbo.KnjigaNalogas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Predmet = c.String(maxLength: 15, unicode: false),
                        MestoUtovara = c.String(maxLength: 30, unicode: false),
                        DatumUtovara = c.DateTime(nullable: false),
                        VrstaRobe = c.String(maxLength: 70, unicode: false),
                        KolicinaRobe = c.String(maxLength: 50, unicode: false),
                        MestoIstovara = c.String(maxLength: 30, unicode: false),
                        KontejnerKamion = c.String(maxLength: 40, unicode: false),
                        Razlika = c.Double(nullable: false),
                        BookingNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IzlaznaFakturas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NazivFakture = c.String(maxLength: 30, unicode: false),
                        DatumFakturisanja = c.DateTime(nullable: false),
                        DatumPrometa = c.DateTime(nullable: false),
                        Iznos = c.Double(nullable: false),
                        ValutaPlacanja = c.Int(nullable: false),
                        Pdf = c.Binary(),
                        BrojZakljucka = c.String(maxLength: 20, unicode: false),
                        Roba = c.String(maxLength: 30, unicode: false),
                        Relacija = c.String(maxLength: 40, unicode: false),
                        DatumPreuzimanjaURJK = c.DateTime(nullable: false),
                        Valuta = c.Int(nullable: false),
                        Placeno = c.Boolean(nullable: false),
                        KnjigaNalogaId = c.Int(nullable: false),
                        KlijentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Klijents", t => t.KlijentId, cascadeDelete: true)
                .ForeignKey("dbo.KnjigaNalogas", t => t.KnjigaNalogaId, cascadeDelete: true)
                .Index(t => t.NazivFakture, unique: true, name: "Unique_username")
                .Index(t => t.KnjigaNalogaId)
                .Index(t => t.KlijentId);
            
            CreateTable(
                "dbo.Klijents",
                c => new
                    {
                        KlijentId = c.Int(nullable: false, identity: true),
                        Pib = c.String(maxLength: 30, unicode: false),
                        NazivFirme = c.String(maxLength: 30, unicode: false),
                        ImeVlasnika = c.String(maxLength: 20, unicode: false),
                        PrezimeVlasnika = c.String(maxLength: 20, unicode: false),
                        Adresa = c.String(maxLength: 40, unicode: false),
                        Email = c.String(maxLength: 30, unicode: false),
                        Telefon = c.String(maxLength: 20, unicode: false),
                        Drzava = c.String(maxLength: 20, unicode: false),
                    })
                .PrimaryKey(t => t.KlijentId)
                .Index(t => t.Pib, unique: true, name: "Unique_password");
            
            CreateTable(
                "dbo.KlijentPonudas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NazivPonude = c.String(maxLength: 40, unicode: false),
                        Opis = c.String(maxLength: 50, unicode: false),
                        Pdf = c.Binary(),
                        ASA = c.Double(nullable: false),
                        Prihvaceno = c.Boolean(nullable: false),
                        PonudaPartneraId = c.Int(nullable: false),
                        KlijentId = c.Int(nullable: false),
                        KnjigaNalogaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Klijents", t => t.KlijentId, cascadeDelete: true)
                .ForeignKey("dbo.KnjigaNalogas", t => t.KnjigaNalogaId, cascadeDelete: true)
                .ForeignKey("dbo.PartnerPonudas", t => t.PonudaPartneraId, cascadeDelete: true)
                .Index(t => t.PonudaPartneraId)
                .Index(t => t.KlijentId)
                .Index(t => t.KnjigaNalogaId);
            
            CreateTable(
                "dbo.PartnerPonudas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NazivPonude = c.String(),
                        Datum = c.DateTime(nullable: false),
                        Opis = c.String(maxLength: 80, unicode: false),
                        Cena = c.Double(nullable: false),
                        PartnerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Partners", t => t.PartnerId, cascadeDelete: true)
                .Index(t => t.PartnerId);
            
            CreateTable(
                "dbo.Partners",
                c => new
                    {
                        PartnerId = c.Int(nullable: false, identity: true),
                        PIB = c.String(maxLength: 30, unicode: false),
                        NazivFirme = c.String(maxLength: 30, unicode: false),
                        Ime = c.String(maxLength: 20, unicode: false),
                        Prezime = c.String(maxLength: 20, unicode: false),
                        Adresa = c.String(maxLength: 40, unicode: false),
                        Email = c.String(maxLength: 30, unicode: false),
                        Telefon = c.String(maxLength: 20, unicode: false),
                        Drzava = c.String(maxLength: 20, unicode: false),
                    })
                .PrimaryKey(t => t.PartnerId)
                .Index(t => t.PIB, unique: true, name: "Unique_password");
            
            CreateTable(
                "dbo.Korisniks",
                c => new
                    {
                        KorisnikId = c.Int(nullable: false, identity: true),
                        Ime = c.String(maxLength: 20, unicode: false),
                        Prezime = c.String(maxLength: 20, unicode: false),
                        Adresa = c.String(maxLength: 40, unicode: false),
                        Email = c.String(maxLength: 20, unicode: false),
                        Telefon = c.String(maxLength: 20, unicode: false),
                        KorisnickoIme = c.String(maxLength: 13, unicode: false),
                        Lozinka = c.String(maxLength: 13, unicode: false),
                        Uloga = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KorisnikId)
                .Index(t => t.KorisnickoIme, unique: true, name: "Unique_username")
                .Index(t => t.Lozinka, unique: true, name: "Unique_password");
            
            CreateTable(
                "dbo.Troskovis",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Racun = c.String(maxLength: 40, unicode: false),
                        DatumPrijema = c.DateTime(nullable: false),
                        DatumPlacanja = c.DateTime(nullable: false),
                        Iznos = c.Double(nullable: false),
                        Placeno = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UlaznaFakturas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NazivFakture = c.String(maxLength: 40, unicode: false),
                        ImeVozara = c.String(maxLength: 20, unicode: false),
                        PrezimeVozara = c.String(maxLength: 20, unicode: false),
                        Opis = c.String(maxLength: 50, unicode: false),
                        DatumPrijema = c.DateTime(nullable: false),
                        DatumPlacanja = c.DateTime(nullable: false),
                        Iznos = c.Double(nullable: false),
                        Placeno = c.Boolean(nullable: false),
                        PartnerId = c.Int(nullable: false),
                        KnjigaNalogaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.KnjigaNalogas", t => t.KnjigaNalogaId, cascadeDelete: true)
                .ForeignKey("dbo.Partners", t => t.PartnerId, cascadeDelete: true)
                .Index(t => t.NazivFakture, unique: true, name: "Unique_username")
                .Index(t => t.PartnerId)
                .Index(t => t.KnjigaNalogaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UlaznaFakturas", "PartnerId", "dbo.Partners");
            DropForeignKey("dbo.UlaznaFakturas", "KnjigaNalogaId", "dbo.KnjigaNalogas");
            DropForeignKey("dbo.KlijentPonudas", "PonudaPartneraId", "dbo.PartnerPonudas");
            DropForeignKey("dbo.PartnerPonudas", "PartnerId", "dbo.Partners");
            DropForeignKey("dbo.KlijentPonudas", "KnjigaNalogaId", "dbo.KnjigaNalogas");
            DropForeignKey("dbo.KlijentPonudas", "KlijentId", "dbo.Klijents");
            DropForeignKey("dbo.IzlaznaFakturas", "KnjigaNalogaId", "dbo.KnjigaNalogas");
            DropForeignKey("dbo.IzlaznaFakturas", "KlijentId", "dbo.Klijents");
            DropForeignKey("dbo.DodatnaDokumentas", "KnjigaNalogaId", "dbo.KnjigaNalogas");
            DropIndex("dbo.UlaznaFakturas", new[] { "KnjigaNalogaId" });
            DropIndex("dbo.UlaznaFakturas", new[] { "PartnerId" });
            DropIndex("dbo.UlaznaFakturas", "Unique_username");
            DropIndex("dbo.Korisniks", "Unique_password");
            DropIndex("dbo.Korisniks", "Unique_username");
            DropIndex("dbo.Partners", "Unique_password");
            DropIndex("dbo.PartnerPonudas", new[] { "PartnerId" });
            DropIndex("dbo.KlijentPonudas", new[] { "KnjigaNalogaId" });
            DropIndex("dbo.KlijentPonudas", new[] { "KlijentId" });
            DropIndex("dbo.KlijentPonudas", new[] { "PonudaPartneraId" });
            DropIndex("dbo.Klijents", "Unique_password");
            DropIndex("dbo.IzlaznaFakturas", new[] { "KlijentId" });
            DropIndex("dbo.IzlaznaFakturas", new[] { "KnjigaNalogaId" });
            DropIndex("dbo.IzlaznaFakturas", "Unique_username");
            DropIndex("dbo.DodatnaDokumentas", new[] { "KnjigaNalogaId" });
            DropTable("dbo.UlaznaFakturas");
            DropTable("dbo.Troskovis");
            DropTable("dbo.Korisniks");
            DropTable("dbo.Partners");
            DropTable("dbo.PartnerPonudas");
            DropTable("dbo.KlijentPonudas");
            DropTable("dbo.Klijents");
            DropTable("dbo.IzlaznaFakturas");
            DropTable("dbo.KnjigaNalogas");
            DropTable("dbo.DodatnaDokumentas");
        }
    }
}
