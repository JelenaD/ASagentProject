namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialcreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdditionalDocumentations",
                c => new
                    {
                        AdditionalDocumentationId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 30, unicode: false),
                        FileName = c.String(),
                        ContentType = c.String(),
                        Content = c.Binary(),
                        BookId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AdditionalDocumentationId)
                .ForeignKey("dbo.Books", t => t.BookId, cascadeDelete: true)
                .Index(t => t.BookId);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        BookId = c.Int(nullable: false, identity: true),
                        BookObject = c.String(maxLength: 15, unicode: false),
                        PlaceOfLoading = c.String(maxLength: 30, unicode: false),
                        DateOfLoading = c.DateTime(nullable: false),
                        Goods = c.String(maxLength: 70, unicode: false),
                        Customer = c.String(maxLength: 70, unicode: false),
                        QuantityOfGoods = c.String(maxLength: 50, unicode: false),
                        PlaceOfUnloading = c.String(maxLength: 30, unicode: false),
                        ContainerTruck = c.String(maxLength: 40, unicode: false),
                        BookingNumber = c.String(),
                        AsaBookingNumberId = c.Int(),
                        StandardBookingNumberId = c.Int(),
                    })
                .PrimaryKey(t => t.BookId)
                .ForeignKey("dbo.AsaBookingNumbers", t => t.AsaBookingNumberId)
                .ForeignKey("dbo.StandardBookingNumbers", t => t.StandardBookingNumberId)
                .Index(t => t.AsaBookingNumberId)
                .Index(t => t.StandardBookingNumberId);
            
            CreateTable(
                "dbo.AsaBookingNumbers",
                c => new
                    {
                        AsaBookingNumberId = c.Int(nullable: false, identity: true),
                        AsaNumber = c.Int(nullable: false),
                        Distance = c.String(),
                        Container = c.String(),
                    })
                .PrimaryKey(t => t.AsaBookingNumberId);
            
            CreateTable(
                "dbo.StandardBookingNumbers",
                c => new
                    {
                        StandardBookingNumberId = c.Int(nullable: false, identity: true),
                        StandardNumber = c.Int(nullable: false),
                        Distance = c.String(),
                        Container = c.String(),
                    })
                .PrimaryKey(t => t.StandardBookingNumberId);
            
            CreateTable(
                "dbo.AddressBooks",
                c => new
                    {
                        AddressBookId = c.Int(nullable: false, identity: true),
                        Company = c.String(maxLength: 30, unicode: false),
                        Name = c.String(maxLength: 30, unicode: false),
                        LastName = c.String(maxLength: 30, unicode: false),
                        Email = c.String(maxLength: 40, unicode: false),
                        Telephone = c.String(maxLength: 30, unicode: false),
                    })
                .PrimaryKey(t => t.AddressBookId);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ClientId = c.Int(nullable: false, identity: true),
                        PIB = c.String(maxLength: 30, unicode: false),
                        NameOfClientCompany = c.String(maxLength: 30, unicode: false),
                        Address = c.String(maxLength: 40, unicode: false),
                        ZipCode = c.String(maxLength: 40, unicode: false),
                        City = c.String(maxLength: 40, unicode: false),
                        Email = c.String(maxLength: 30, unicode: false),
                        Telephone = c.String(maxLength: 20, unicode: false),
                        State = c.String(maxLength: 20, unicode: false),
                    })
                .PrimaryKey(t => t.ClientId)
                .Index(t => t.PIB, unique: true, name: "Unique_password");
            
            CreateTable(
                "dbo.ClientOffers",
                c => new
                    {
                        ClientOfferId = c.Int(nullable: false, identity: true),
                        OffersName = c.String(maxLength: 40, unicode: false),
                        Descritpion = c.String(maxLength: 50, unicode: false),
                        FileName = c.String(),
                        ContetnType = c.String(),
                        Content = c.Binary(),
                        ASA = c.Double(nullable: false),
                        Accepted = c.Boolean(nullable: false),
                        ClientId = c.Int(nullable: false),
                        BookId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClientOfferId)
                .ForeignKey("dbo.Books", t => t.BookId, cascadeDelete: true)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.BookId);
            
            CreateTable(
                "dbo.Costs",
                c => new
                    {
                        CostId = c.Int(nullable: false, identity: true),
                        AccountNumber = c.String(maxLength: 40, unicode: false),
                        DateOfReceipt = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Amount = c.Double(nullable: false),
                        Paid = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CostId);
            
            CreateTable(
                "dbo.DistanceTransports",
                c => new
                    {
                        DTId = c.Int(nullable: false, identity: true),
                        DistanceName = c.String(),
                        TransportName = c.String(),
                        ShipperId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DTId)
                .ForeignKey("dbo.Shippers", t => t.ShipperId, cascadeDelete: true)
                .Index(t => t.ShipperId);
            
            CreateTable(
                "dbo.Shippers",
                c => new
                    {
                        ShipperId = c.Int(nullable: false, identity: true),
                        PIB = c.String(maxLength: 30, unicode: false),
                        CompanyName = c.String(maxLength: 30, unicode: false),
                        Address = c.String(maxLength: 40, unicode: false),
                        ZipCode = c.String(maxLength: 40, unicode: false),
                        City = c.String(maxLength: 40, unicode: false),
                        Email = c.String(maxLength: 30, unicode: false),
                        Telephone = c.String(maxLength: 20, unicode: false),
                        State = c.String(maxLength: 20, unicode: false),
                    })
                .PrimaryKey(t => t.ShipperId)
                .Index(t => t.PIB, unique: true, name: "Unique_password");
            
            CreateTable(
                "dbo.ForeignInputInvoices",
                c => new
                    {
                        SerialNumberOfForeignInputInvoice = c.Int(nullable: false, identity: true),
                        DateOfReceiptInvoice = c.DateTime(nullable: false),
                        ShipperId = c.Int(nullable: false),
                        NumberOfForeignInputInvoice = c.String(maxLength: 40, unicode: false),
                        AmountInEUR = c.Double(nullable: false),
                        ValueDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Paid = c.Boolean(),
                        Comment = c.String(),
                        BookId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SerialNumberOfForeignInputInvoice)
                .ForeignKey("dbo.Books", t => t.BookId, cascadeDelete: true)
                .ForeignKey("dbo.Shippers", t => t.ShipperId, cascadeDelete: true)
                .Index(t => t.ShipperId)
                .Index(t => t.NumberOfForeignInputInvoice, unique: true, name: "Unique_username")
                .Index(t => t.BookId);
            
            CreateTable(
                "dbo.ForeignOutgoingInvoices",
                c => new
                    {
                        ForeignOutgoingInvoiceId = c.Int(nullable: false, identity: true),
                        DateOfReceipt = c.DateTime(nullable: false),
                        ClientId = c.Int(nullable: false),
                        NumberOfForeignOutgoingInvoice = c.String(maxLength: 50, unicode: false),
                        AmountInEUR = c.Double(),
                        ValueDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Paid = c.Boolean(nullable: false),
                        Comment = c.String(),
                        BookId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ForeignOutgoingInvoiceId)
                .ForeignKey("dbo.Books", t => t.BookId, cascadeDelete: true)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.NumberOfForeignOutgoingInvoice, unique: true, name: "Unique_username")
                .Index(t => t.BookId);
            
            CreateTable(
                "dbo.InputInvoices",
                c => new
                    {
                        SerialNumberOfInputInvoice = c.Int(nullable: false, identity: true),
                        DateOfReceiptInvoice = c.DateTime(nullable: false),
                        ShipperId = c.Int(nullable: false),
                        NumberOfInputInvoice = c.String(maxLength: 40, unicode: false),
                        AmountInDIN = c.Double(),
                        AmountInEUR = c.Double(),
                        ValueDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Paid = c.Boolean(),
                        Comment = c.String(),
                        BookId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SerialNumberOfInputInvoice)
                .ForeignKey("dbo.Books", t => t.BookId, cascadeDelete: true)
                .ForeignKey("dbo.Shippers", t => t.ShipperId, cascadeDelete: true)
                .Index(t => t.ShipperId)
                .Index(t => t.NumberOfInputInvoice, unique: true, name: "Unique_username")
                .Index(t => t.BookId);
            
            CreateTable(
                "dbo.OutgoingInvoices",
                c => new
                    {
                        OutgoingInvoiceId = c.Int(nullable: false, identity: true),
                        DateOfReceipt = c.DateTime(nullable: false),
                        ClientId = c.Int(nullable: false),
                        NumberOfOutgoingInvoice = c.String(maxLength: 50, unicode: false),
                        AmountInEUR = c.Double(),
                        AmountInDIN = c.Double(),
                        ValueDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Paid = c.Boolean(nullable: false),
                        Comment = c.String(),
                        BookId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OutgoingInvoiceId)
                .ForeignKey("dbo.Books", t => t.BookId, cascadeDelete: true)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.NumberOfOutgoingInvoice, unique: true, name: "Unique_username")
                .Index(t => t.BookId);
            
            CreateTable(
                "dbo.ShipperOffers",
                c => new
                    {
                        ShipperOfferId = c.Int(nullable: false, identity: true),
                        ShipperOfferName = c.String(),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(maxLength: 80, unicode: false),
                        Distance = c.String(),
                        Telephone = c.String(),
                        Email = c.String(),
                        Price = c.Double(nullable: false),
                        ShipperId = c.Int(nullable: false),
                        ClientOffId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ShipperOfferId)
                .ForeignKey("dbo.ClientOffers", t => t.ClientOffId, cascadeDelete: true)
                .ForeignKey("dbo.Shippers", t => t.ShipperId, cascadeDelete: true)
                .Index(t => t.ShipperId)
                .Index(t => t.ClientOffId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 20, unicode: false),
                        LastName = c.String(maxLength: 20, unicode: false),
                        Address = c.String(maxLength: 40, unicode: false),
                        Email = c.String(maxLength: 50, unicode: false),
                        Telephone = c.String(maxLength: 20, unicode: false),
                        Username = c.String(maxLength: 13, unicode: false),
                        Password = c.String(maxLength: 13, unicode: false),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .Index(t => t.Username, unique: true, name: "Unique_username")
                .Index(t => t.Password, unique: true, name: "Unique_password");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShipperOffers", "ShipperId", "dbo.Shippers");
            DropForeignKey("dbo.ShipperOffers", "ClientOffId", "dbo.ClientOffers");
            DropForeignKey("dbo.OutgoingInvoices", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.OutgoingInvoices", "BookId", "dbo.Books");
            DropForeignKey("dbo.InputInvoices", "ShipperId", "dbo.Shippers");
            DropForeignKey("dbo.InputInvoices", "BookId", "dbo.Books");
            DropForeignKey("dbo.ForeignOutgoingInvoices", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.ForeignOutgoingInvoices", "BookId", "dbo.Books");
            DropForeignKey("dbo.ForeignInputInvoices", "ShipperId", "dbo.Shippers");
            DropForeignKey("dbo.ForeignInputInvoices", "BookId", "dbo.Books");
            DropForeignKey("dbo.DistanceTransports", "ShipperId", "dbo.Shippers");
            DropForeignKey("dbo.ClientOffers", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.ClientOffers", "BookId", "dbo.Books");
            DropForeignKey("dbo.AdditionalDocumentations", "BookId", "dbo.Books");
            DropForeignKey("dbo.Books", "StandardBookingNumberId", "dbo.StandardBookingNumbers");
            DropForeignKey("dbo.Books", "AsaBookingNumberId", "dbo.AsaBookingNumbers");
            DropIndex("dbo.Users", "Unique_password");
            DropIndex("dbo.Users", "Unique_username");
            DropIndex("dbo.ShipperOffers", new[] { "ClientOffId" });
            DropIndex("dbo.ShipperOffers", new[] { "ShipperId" });
            DropIndex("dbo.OutgoingInvoices", new[] { "BookId" });
            DropIndex("dbo.OutgoingInvoices", "Unique_username");
            DropIndex("dbo.OutgoingInvoices", new[] { "ClientId" });
            DropIndex("dbo.InputInvoices", new[] { "BookId" });
            DropIndex("dbo.InputInvoices", "Unique_username");
            DropIndex("dbo.InputInvoices", new[] { "ShipperId" });
            DropIndex("dbo.ForeignOutgoingInvoices", new[] { "BookId" });
            DropIndex("dbo.ForeignOutgoingInvoices", "Unique_username");
            DropIndex("dbo.ForeignOutgoingInvoices", new[] { "ClientId" });
            DropIndex("dbo.ForeignInputInvoices", new[] { "BookId" });
            DropIndex("dbo.ForeignInputInvoices", "Unique_username");
            DropIndex("dbo.ForeignInputInvoices", new[] { "ShipperId" });
            DropIndex("dbo.Shippers", "Unique_password");
            DropIndex("dbo.DistanceTransports", new[] { "ShipperId" });
            DropIndex("dbo.ClientOffers", new[] { "BookId" });
            DropIndex("dbo.ClientOffers", new[] { "ClientId" });
            DropIndex("dbo.Clients", "Unique_password");
            DropIndex("dbo.Books", new[] { "StandardBookingNumberId" });
            DropIndex("dbo.Books", new[] { "AsaBookingNumberId" });
            DropIndex("dbo.AdditionalDocumentations", new[] { "BookId" });
            DropTable("dbo.Users");
            DropTable("dbo.ShipperOffers");
            DropTable("dbo.OutgoingInvoices");
            DropTable("dbo.InputInvoices");
            DropTable("dbo.ForeignOutgoingInvoices");
            DropTable("dbo.ForeignInputInvoices");
            DropTable("dbo.Shippers");
            DropTable("dbo.DistanceTransports");
            DropTable("dbo.Costs");
            DropTable("dbo.ClientOffers");
            DropTable("dbo.Clients");
            DropTable("dbo.AddressBooks");
            DropTable("dbo.StandardBookingNumbers");
            DropTable("dbo.AsaBookingNumbers");
            DropTable("dbo.Books");
            DropTable("dbo.AdditionalDocumentations");
        }
    }
}
