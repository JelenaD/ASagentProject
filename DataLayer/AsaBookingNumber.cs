﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class AsaBookingNumber
    {
        [Key]
        public int AsaBookingNumberId { get; set; }
        public int AsaNumber { get; set; }
        public string Distance { get; set; }
        public string Container { get; set; }
    }
}
