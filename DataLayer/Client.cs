﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class Client
    {
        [Key]
        //KlijentId
        public int ClientId { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        [Index("Unique_password", IsUnique = true)]
        public string PIB { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        //NazivFirme
        public string NameOfClientCompany { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        //Adresa
        public string Address { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        //ZipCode
        public string ZipCode { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        //Grad
        public string City { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string Email { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        //Telefon
        public string Telephone { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        //Drzava
        public string State { get; set; }
    }
}
