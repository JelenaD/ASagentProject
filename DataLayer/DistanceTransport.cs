﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class DistanceTransport
    {
        [Key]
        public int DTId { get; set; }
        public string DistanceName { get; set; }
        public string TransportName { get; set; }

        [Required]
        public int ShipperId { get; set; }
        [ForeignKey("ShipperId")]
        public Shipper Shipper { get; set; }
    }
}
