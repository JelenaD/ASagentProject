﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class Klijent
    {
        [Key]
        public int KlijentId { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        [Index("Unique_password", IsUnique = true)]
        public string Pib { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string NazivFirme { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string ImeVlasnika { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string PrezimeVlasnika { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string Adresa { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string Email { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Telefon { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Drzava { get; set; }
    }
}
