﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class Troskovi
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string Racun { get; set; }

        public DateTime DatumPrijema { get; set; }
        public DateTime DatumPlacanja { get; set; }
        public double Iznos { get; set; }

        public bool Placeno { get; set; }
    }
}
