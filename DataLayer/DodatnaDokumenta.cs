﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class DodatnaDokumenta
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string Naziv { get; set; }

        public byte[] Pdf { get; set; }

        [Required]
        public int KnjigaNalogaId { get; set; }
        [ForeignKey("KnjigaNalogaId")]
        public KnjigaNaloga KnjigaNaloga { get; set; }
    }
}
