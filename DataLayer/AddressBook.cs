﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class AddressBook
    {
        [Key]
        public int AddressBookId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string Company { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string Name { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string LastName { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string Email { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string Telephone { get; set; }
    }
}
