﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class OutgoingInvoice
    {

        [Key]
        //Redni broj
        public int OutgoingInvoiceId { get; set; }
        //DatumPrijema
        public DateTime DateOfReceipt { get; set; }

        [Required]
        //KlijentId Klijent
        public int ClientId { get; set; }
        [ForeignKey("ClientId")]
        public Client Client { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Index("Unique_username", 1, IsUnique = true)]
        //BrojUlazneFakture
        public string NumberOfOutgoingInvoice { get; set; }
        //IznosUEUR, IznosDin  Placena  Komentar
        public double? AmountInEUR { get; set; }
        public double? AmountInDIN { get; set; }
        //DatumValute  DatumPlacanja 
        public DateTime ValueDate { get; set; }
        public DateTime PaymentDate { get; set; }
        public bool Paid { get; set; }
        public string Comment { get; set; }

        [Required]
        public int BookId { get; set; }
        [ForeignKey("BookId")]
        public Book Book { get; set; }
    }

    
}

