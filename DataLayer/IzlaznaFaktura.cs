﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class IzlaznaFaktura
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        [Index("Unique_username", 1, IsUnique = true)]
        public string NazivFakture { get; set; }

        public DateTime DatumFakturisanja { get; set; }
        public DateTime DatumPrometa { get; set; }
        public double Iznos { get; set; }

        public Valuta ValutaPlacanja { get; set; }

        public byte[] Pdf { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string BrojZakljucka { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string Roba { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string Relacija { get; set; }

        public DateTime DatumPreuzimanjaURJK { get; set; }
        public int Valuta { get; set; }
        public bool Placeno { get; set; }

        [Required]
        public int KnjigaNalogaId { get; set; }
        [ForeignKey("KnjigaNalogaId")]
        public KnjigaNaloga KnjigaNaloga { get; set; }

        [Required]
        public int KlijentId { get; set; }
        [ForeignKey("KlijentId")]
        public Klijent Klijent { get; set; }
    }

    public enum Valuta
    {
        Domaca = 1,
        Strana = 2
    }
}

