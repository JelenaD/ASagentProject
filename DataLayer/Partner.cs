﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class Partner
    {
        [Key]
        public int PartnerId { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        [Index("Unique_password", IsUnique = true)]
        public string PIB { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string NazivFirme { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Ime { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Prezime { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string Adresa { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string Email { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Telefon { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Drzava { get; set; }
    }
}
