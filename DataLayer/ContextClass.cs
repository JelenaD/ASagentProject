﻿using System;
using System.Data.Entity;

namespace DataLayer
{
    public class ContextClass : DbContext
    {
        public ContextClass() : base("proj")
        {

        }

        public DbSet<AdditionalDocumentation> AdditionalDocumentation { get; set; }
        public DbSet<AddressBook> AddressBook { get; set; }
        public DbSet<OutgoingInvoice> OutgoingInvoice { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<ClientOffer> ClientOffer { get; set; }
        public DbSet<DistanceTransport> DistanceTransport { get; set; }
        public DbSet<Book> Book { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Shipper> Shipper { get; set; }
        public DbSet<ShipperOffer> ShipperOffer { get; set; }
        public DbSet<Cost> Cost { get; set; }
        public DbSet<InputInvoice> InputInvoice { get; set; }
        public DbSet<AsaBookingNumber> AsaBookingNumber { get; set; }
        public DbSet<ForeignInputInvoice> ForeignInputInvoice { get; set; }
        public DbSet<ForeignOutgoingInvoice> ForeignOutgoingInvoice { get; set; }
        public DbSet<StandardBookingNumber> StandardBookingNumber { get; set; }
        
    }
}
