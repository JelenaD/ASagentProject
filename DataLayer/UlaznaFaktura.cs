﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class UlaznaFaktura
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        [Index("Unique_username", IsUnique = true)]
        public string NazivFakture { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string ImeVozara { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string PrezimeVozara { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string Opis { get; set; }

        public DateTime DatumPrijema { get; set; }
        public DateTime DatumPlacanja { get; set; }
        public double Iznos { get; set; }

        public bool Placeno { get; set; }

        [Required]
        public int PartnerId { get; set; }
        [ForeignKey("PartnerId")]
        public Partner Partner { get; set; }

        [Required]
        public int KnjigaNalogaId { get; set; }
        [ForeignKey("KnjigaNalogaId")]
        public KnjigaNaloga KnjigaNaloga { get; set; }
    }
}
