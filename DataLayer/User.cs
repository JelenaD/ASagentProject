﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Name { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string LastName { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string Address { get; set; }



        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string Email { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Telephone { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(13)]
        [Index("Unique_username", 1, IsUnique = true)]
        public string Username { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(13)]
        [Index("Unique_password", 2, IsUnique = true)]
        public string Password { get; set; }

        public UserRole Role { get; set; }
    }

    public enum UserRole
    {
        Administrator = 1,
        Operater = 2,
        Knjigovodja = 3
    }
}

