﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Book
    {
        [Key]
        public int BookId { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(15)]
        //Predmet
        public string BookObject { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        //MestoUtovara
        public string PlaceOfLoading { get; set; }
        //DatumUtovara
        public DateTime DateOfLoading { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(70)]
        //VrstaRobe
        public string Goods { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(70)]
        //VrstaRobe
        public string Customer { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        //KolicinaRobe
        public string QuantityOfGoods { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        //MestoIstovara
        public string PlaceOfUnloading { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        //KontejnerKamion
        public string ContainerTruck { get; set; }
        public string BookingNumber{ get; set; }

        public int? AsaBookingNumberId { get; set; }
        [ForeignKey("AsaBookingNumberId")]
        public virtual AsaBookingNumber ASABookingNumber { get; set; }


        public int? StandardBookingNumberId { get; set; }
        [ForeignKey("StandardBookingNumberId")]
        public virtual StandardBookingNumber StandardBookingNumber { get; set; }
    }
}
