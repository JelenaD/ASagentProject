﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class ForeignInputInvoice
    {
        [Key]
        public int SerialNumberOfForeignInputInvoice { get; set; }
        public DateTime DateOfReceiptInvoice { get; set; }

        [Required]
        public int ShipperId { get; set; }
        [ForeignKey("ShipperId")]
        public Shipper Shipper { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        [Index("Unique_username", IsUnique = true)]
        public string NumberOfForeignInputInvoice { get; set; }
        public double AmountInEUR { get; set; }
        public DateTime ValueDate { get; set; }
        public DateTime PaymentDate { get; set; }
        public bool? Paid { get; set; }
        public string Comment { get; set; }

        [Required]
        public int BookId { get; set; }
        [ForeignKey("BookId")]
        public Book Book { get; set; }
    }
}
