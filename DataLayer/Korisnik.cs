﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class Korisnik
    {
        [Key]
        public int KorisnikId { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Ime { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Prezime { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string Adresa { get; set; }

        

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Email { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string Telefon { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(13)]
        [Index("Unique_username", 1, IsUnique = true)]
        public string KorisnickoIme { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(13)]
        [Index("Unique_password", 2, IsUnique = true)]
        public string Lozinka { get; set; }

        public UlogaKorisnika Uloga { get; set; }
    }

    public enum UlogaKorisnika
    {
        Administrator = 1,
        Operater = 2,
        Knjigovodja = 3
    }
}

