﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer
{
    public class KnjigaNaloga
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(15)]
        public string Predmet { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string MestoUtovara { get; set; }

        public DateTime DatumUtovara { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(70)]
        public string VrstaRobe { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string KolicinaRobe { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(30)]
        public string MestoIstovara { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string KontejnerKamion { get; set; }

        public double Razlika { get; set; }

        public int BookingNumber { get; set; }
    }
}
